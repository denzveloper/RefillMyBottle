<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shopping extends CI_Controller {

    public function __construct(){
        parent::__construct();
        //Must to Load!
        //load model placem
        $this->load->model('shopm');
    }

    public function index(){
        $id = $this->input->get("id", TRUE);
        $tkn = $this->input->get("token", TRUE);
        $data['sh'] = $this->shopm->info($id, $tkn);
        if(!empty($data['sh'])) $this->load->view("shop", $data);
        else echo "<h2>Not Found</h2><p style='color: #fff'>TWFkZSBCeTogREVOWlZFTE9QRVIgRHpFTi9EekVOIOOAjERFTkRZIE9DVEFWSUFO44CN</p>";
    }
}