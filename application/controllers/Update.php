<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Update extends CI_Controller {

    public function __construct(){
        parent::__construct();
        //Must to Load!
        $this->load->library('session');
        //load model loginm
        $this->load->model('loginm');
    }

    public function index(){
        redirect('dashboard');
    }

    public function addpoin(){
        if($this->loginm->chksess() && $this->loginm->placeid() != null){
            if($_SESSION['lv'] == 1){
                //load model waterm
                $this->load->model('waterm');

                //Send Poin to user
                $this->form_validation->set_rules('user', 'User', 'required|valid_email');
                $this->form_validation->set_rules('poin', 'Multiplication point', 'required|is_natural_no_zero');

                if ($this->form_validation->run() == TRUE){
                    //add poin user
                    $usr = $this->input->post("user", TRUE);
                    $mtp = $this->input->post("poin", TRUE);
                    $pnt = $this->waterm->getail('tempatisi', array('id' => $this->loginm->placeid()))->poin;

                    //okok
                    $jumlah = $pnt * $mtp;
                    $cek = $this->loginm->addpnt($usr, $jumlah);
                    $this->loginm->addcom($usr, $mtp);

                    if($cek != FALSE){

                        $errmsg[] = array('ico' => 'ti-check', 'txt' => "<b>OK: </b><br><i>Point Added $jumlah to $usr!</i>",
                            'typ' => 'success');
                    }else{
                        $errmsg[] = array('ico' => 'ti-close', 'txt' => "<b>Error: </b><br><i>Add Point to $usr Failed!</i>",
                            'typ' => 'danger');
                    }
                }else{
                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Warning: </b><br><i>'.preg_replace("/(\n)+/m", '<br>', strip_tags(strip_tags(validation_errors()))).'</i>',
                        'typ' => 'warning');
                }
                $this->session->set_flashdata('info', $errmsg);
                redirect("dashboard");
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
            redirect("login");
        }
    }

    public function updplc(){
        if($this->loginm->chksess() && $this->loginm->placeid() != null){
            if($_SESSION['lv'] == 1){
                $errmsg = array();
                //Load Model waterm
                $this->load->model('waterm');

                $this->form_validation->set_rules('nama', 'Nama Tempat', 'required');
                $this->form_validation->set_rules('alamat', 'Nama Tempat', 'required');
                $this->form_validation->set_rules('deskripsi', 'Deskripsi Tempat', 'required');
                #$this->form_validation->set_rules('foto', 'Photo', 'required');
                $this->form_validation->set_rules('poin', 'Poin Yang diberikan', 'required');
                $this->form_validation->set_rules('premium', 'Premium Tempat', 'required');
                $this->form_validation->set_rules('jam_b', 'Jam Buka', 'required');
                $this->form_validation->set_rules('jam_t', 'Jam Tutup', 'required');
                $this->form_validation->set_rules('harga', 'Harga Rata-rata Produk', 'required');
                $this->form_validation->set_rules('lat', 'Latitude', 'required');
                $this->form_validation->set_rules('lon', 'Longitude', 'required');
                $this->form_validation->set_rules('type[]', 'Tipe Tempat', 'required');
                $this->form_validation->set_rules('pass', 'Password', 'required|min_length[4]|max_length[64]');

                if ($this->form_validation->run() == TRUE){
                    $nam = $this->input->post("nama", TRUE);
                    $ala = $this->input->post("alamat", TRUE);
                    $des = $this->input->post("deskripsi", TRUE);
                    $pnt = $this->input->post("poin", TRUE);
                    $pre = $this->input->post("premium", TRUE);
                    $jab = date("H:i:s", strtotime($this->input->post("jam_b", TRUE)));
                    $jat = date("H:i:s", strtotime($this->input->post("jam_t", TRUE)));
                    $har = $this->input->post("harga", TRUE);
                    $lat = $this->waterm->duocimal($this->input->post("lat", TRUE), 8);
                    $lon = $this->waterm->duocimal($this->input->post("lon", TRUE), 8);
                    $typ = $this->input->post("type", TRUE);
                    $id = $this->loginm->placeid();
                    $pas = $this->input->post("pass", TRUE);

                    if($pas == $this->loginm->getailalt('user', array('surel' => $_SESSION['mail']), 'sandi')){

                        $data = array('nama' => $nam, 'alamat' => $ala, 'detail' => $des, 'poin' => $pnt, 'premium' => $pre,
                        'jam_buka' => $jab, 'jam_tutup' => $jat, 'harga' => $har, 'latitude' => $lat, 'longitude' => $lon);
                        
                        $cek = $this->loginm->updplc($data);
                        $cek1 = $this->loginm->updtyp($typ);
                        //Configuration Updoad Photos
                        //$prna = time().$mail.$_FILES["profil"]['name']; //Change name of image
                        $config['upload_path'] = './asset/plpic/'; //On "plpic" upload
                        $config['allowed_types'] = 'jfif|jpg|jpeg|png|gif'; //Pitcure Only
                        $config['max_size'] = '2048'; //2MB
                        $config['encrypt_name'] = TRUE;
                        //$config['file_name'] = $prna; //set new Name
                        $this->load->library('upload', $config); //load library upload

                        $cek2 = FALSE;

                        //Photos is detected OK
                        if ($this->upload->do_upload('foto')){
                            //Get Filename
                            $tmp = $this->waterm->getail('tempatisi', array('id' => $this->loginm->placeid()))->foto;
                            $fnm = $this->upload->data('file_name');
                            chmod('./asset/plpic/'.$fnm, 0777);

                            if($tmp != 'default.png'){
                                rename("./asset/plpic/$tmp", "./asset/papic/$tmp");
                                $this->loginm->addpict(array('id_tempat' => $this->loginm->placeid(), 'image' => $tmp));
                            }

                            //Update Photo Place
                            $cek2 = $this->loginm->updplc(array('foto' => $fnm));
                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => "<b>Warning: </b><br><i>".$this->upload->display_errors()."</i>",
                                'typ' => 'warning');
                        }
                        if($cek != FALSE){
                            $errmsg[] = array('ico' => 'ti-check', 'txt' => "<b>OK: </b><br><i>Information about Place Updated!</i>",
                                'typ' => 'success');
                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => "<b>Warning: </b><br><i>Information Place not Updated!</i>",
                                'typ' => 'warning');
                        }
                        if($cek1 != FALSE){
                            $errmsg[] = array('ico' => 'ti-check', 'txt' => "<b>OK: </b><br><i>Place Type Updated!</i>",
                                'typ' => 'success');
                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => "<b>Warning: </b><br><i>Place Type is Not Update</i>",
                                'typ' => 'warning');
                        }
                        if($cek2 != FALSE){
                            $errmsg[] = array('ico' => 'ti-check', 'txt' => "<b>OK: </b><br><i>Photo Updated!</i>",
                                'typ' => 'success');
                        }
                    }else{
                        $errmsg[] = array('ico' => 'ti-close', 'txt' => "<b>Error: </b><br><i>Wrong Password to Update!</i>",
                            'typ' => 'danger');
                    }
                }else{
                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Warning: </b><br><i>'.preg_replace("/(\n)+/m", '<br>', strip_tags(strip_tags(validation_errors()))).'</i>',
                        'typ' => 'warning');
                }
                $this->session->set_flashdata('info', $errmsg);
                redirect("dashboard/place");
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
            redirect("login");
        }
    }

    public function acph(){
        $this->load->library("safe");

        if($this->loginm->chksess()){
            if($_SESSION['lv'] == 1){
                $mail = $_SESSION['mail'];
                $id = $this->input->get("id", TRUE);
                $key = $this->input->get("key", TRUE);
                $todo = $this->input->get("todo", TRUE);
                
                if(!empty($id) && !empty($key) && !empty($todo)){
                    $ids = $this->loginm->placeid();
                    $file = $this->safe->dencrypt($key, $ids);
                    $fname = $this->loginm->getailalt('img_tempat', array('id' => $id), 'image');

                    if($todo == "primary"){
                        if($fname == $file){
                            $file1 = $this->loginm->getailalt('tempatisi', array('id' => $ids), 'foto');

                            $cek1 = $this->loginm->updplc(array('foto' => $file));
                            $this->loginm->addpict(array('id_tempat' => $this->loginm->placeid(), 'image' => $file1));

                            $cek = $this->loginm->delete(array('id' => $id), 'img_tempat');

                            rename("./asset/papic/$file", "./asset/plpic/$file");
                            rename("./asset/plpic/$file1", "./asset/papic/$file1");

                            if($cek != FALSE && $cek1 != FALSE){
                                $errmsg[] = array('ico' => 'ti-check', 'txt' => "<b>OK: </b><br><i>Image Set as Primary!</i><br><sub>$file as Primary!</sub>",
                                    'typ' => 'success');

                            }else{
                                $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Cant made image as primary.</i>',
                                    'typ' => 'warning');

                            }

                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Invalid Key</sub></i>',
                                'typ' => 'danger');
                        }
                    }elseif($todo == "delete"){
                        if($fname == $file){
                            $cek = $this->loginm->delete(array('id' => $id), 'img_tempat');
                            unlink("./asset/papic/$file");
                            if($cek != FALSE && !file_exists("./asset/papic/$file")){
                                $errmsg[] = array('ico' => 'ti-check', 'txt' => "<b>OK: </b><br><i>Image Deleted!</i><br><sub>$file is Deleted!</sub>",
                                    'typ' => 'success');
                            }else{
                                $errmsg[] = array('ico' => 'ti-check', 'txt' => "<b>Warining: </b><br><i>Image Can't deleted</i>",
                                    'typ' => 'warning');
                            }
                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Invalid Key</sub></i>',
                                'typ' => 'danger');
                        }

                    }else{
                        $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Unknown Action</sub></i>',
                            'typ' => 'danger');
                    }

                }else{
                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Uncomplete Address</sub></i>',
                        'typ' => 'danger');
                }

                $this->session->set_flashdata('info', $errmsg);
                redirect('dashboard/place');

            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
            redirect("login");
        }
    }

    public function updpro(){
        if($this->loginm->chksess()){
            if($_SESSION['lv'] < 2){
                $errmsg = array();
                $mail = $_SESSION['mail'];
                //load model waterm
                $this->load->model('waterm');

                $this->form_validation->set_rules('nd', 'First Name', 'required');
                $this->form_validation->set_rules('nb', 'Last Name', 'required');
                $this->form_validation->set_rules('email', 'E-Mail', 'required|valid_email|trim|min_length[6]|max_length[64]');
                $this->form_validation->set_rules('pass', 'Password', 'required|min_length[4]|max_length[64]');
                if($this->form_validation->run() == TRUE){
                    $nad = $this->input->post("nd", TRUE);
                    $nab = $this->input->post("nb", TRUE);
                    $ema = $this->input->post("email", TRUE);
                    $pas = $this->input->post("pass", TRUE);

                    $nad = ucwords(strtolower($nad), '\',. ');
                    $nab = ucwords(strtolower($nab), '\',. ');

                    if($pas == $this->loginm->getailalt('user', array('surel' => $_SESSION['mail']), 'sandi')){
                    
                        $data = array('namadepan' => $nad, 'namabelakang' => $nab, 'surel' => $ema);

                        $cek = $this->loginm->updpro(array('sandi' => $pas), $data);

                        //Configuration Updoad Photos
                        //$prna = time().$mail.$_FILES["profil"]['name']; //Change name of image
                        $config['upload_path'] = './asset/uspic/'; //On "uspic" upload
                        $config['allowed_types'] = 'jfif|jpg|jpeg|png|gif'; //Pitcure Only
                        $config['max_size'] = '2048'; //2MB
                        $config['encrypt_name'] = TRUE;
                        //$config['file_name'] = $prna; //set new Name
                        $this->load->library('upload', $config); //load library upload

                        $cek2 = FALSE;

                        //Photos is detected OK
                        if ($this->upload->do_upload('foto')){
                            //Get Filename
                            $tmp = $this->waterm->getail('user', array('surel' => $mail))->photo;
                            $fnm = $this->upload->data('file_name');
                            chmod('./asset/uspic/'.$fnm, 0777);

                            if($tmp != 'default.png'){
                                unlink('./asset/uspic/'.$tmp);
                            }

                            //Update Photo profil
                            $cek2 = $this->loginm->updpro(array('sandi' => $pas), array('photo' => $fnm));
                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Info: </b><br><i>'.$this->upload->display_errors().'</i>',
                                'typ' => 'warning');
                        }
                        if($cek != FALSE){
                            $errmsg[] = array('ico' => 'ti-check', 'txt' => '<b>OK: </b><br><i>Info Updated</i>',
                                'typ' => 'success');
                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>info: </b><br><i>No Info where Updated</i>',
                                'typ' => 'warning');
                            }
                        if($cek2 != FALSE){
                            $errmsg[] = array('ico' => 'ti-check', 'txt' => '<b>OK: </b><br><i>Image Updated</i>',
                                'typ' => 'success');
                        }
                    }else{
                        $errmsg[] = array('ico' => 'ti-close', 'txt' => "<b>Error: </b><br><i>Wrong Password!</i>",
                            'typ' => 'danger');
                    }
                }else{
                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Warning: </b><br><i>'.preg_replace("/(\n)+/m", '<br>', strip_tags(strip_tags(validation_errors()))).'</i>',
                        'typ' => 'warning');
                }
                $this->session->set_flashdata('info', $errmsg);
                redirect('dashboard/profil');
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
            redirect("login");
        }
    }

    public function updsan(){
        if($this->loginm->chksess()){
            if($_SESSION['lv'] < 2){
                $errmsg = array();
                $mail = $_SESSION['mail'];
                $this->form_validation->set_rules('pass', 'Last Password', 'required|min_length[4]|max_length[64]');
                $this->form_validation->set_rules('pabs', 'New Password', 'required|min_length[4]|max_length[64]');
                $this->form_validation->set_rules('pabd', 'Confirmation Password', 'required|min_length[4]|max_length[64]');
                if($this->form_validation->run() == TRUE){
                    $pal = $this->input->post("pass", TRUE);
                    $pas = $this->input->post("pabs", TRUE);
                    $pad = $this->input->post("pabd", TRUE);
                    if($pad == $pas){
                        if($pal != $pad){
                            $data = array('sandi' => $pad);

                            $cek = $this->loginm->updpro(array('sandi' => $pal), $data);

                            if($cek != FALSE){
                                $errmsg[] = array('ico' => 'ti-check', 'txt' => '<b>OK: </b><br><i>'.preg_replace("/(\n)+/m", '<br>', strip_tags(strip_tags(validation_errors())).'</b>'),
                                    'typ' => 'success');
                            }else{
                                $errmsg[] = array('ico' => 'ti-close', 'txt' => "<b>Error: </b><br><i>Can't Update Password</i>",
                                    'typ' => 'danger');
                            }
                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Warning: <b><br><i>Password not update, it same before</i>',
                                'typ' => 'warning');
                        }
                    }else{
                        $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Warning: </b><br><i>New Password with Confirmation Password different</i>',
                            'typ' => 'warning');
                    }
                }else{
                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Warning: </b><br><i>'.preg_replace("/(\n)+/m", '<br>', strip_tags(strip_tags(validation_errors()))).'</i>',
                        'typ' => 'warning');
                }
                $this->session->set_flashdata('info', $errmsg);
                redirect('dashboard/passwd');
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
            redirect("login");
        }
    }

    public function usrtodo(){
        if($this->loginm->chksess()){
            $this->load->library("safe");
            if($_SESSION['lv'] < 1){
                $errmsg = array();
                $mail = $_SESSION['mail'];
                $tod = $this->input->get("todo", TRUE);
                $usr = $this->input->get("usr", TRUE);

                if(isset($tod) && isset($usr)){
                    $tus = $this->safe->dencrypt($usr, $mail);
                    if($tod == "block"){
                        $cek = $this->loginm->blockus($tus);
                        if($cek != FALSE){
                            $dis = $this->loginm->getailalt('user', array('surel' => $tus), 'disable');
                            if($dis){
                                $errmsg[] = array('ico' => 'ti-check', 'txt' => '<b>OK: </b><br><i>'.$tus.' blocked!</b>',
                                    'typ' => 'success');
                            }else{
                                $errmsg[] = array('ico' => 'ti-check', 'txt' => '<b>OK: </b><br><i>'.$tus.' Unblocked!</b>',
                                    'typ' => 'success');
                            }
                        }else{
                            $dis = $this->loginm->getailalt('user', array('surel' => $tus), 'disable');
                            if($dis){
                                $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Cant Unblock '.$tus.'!</i>',
                                    'typ' => 'danger');
                            }else{
                                $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Cant Block '.$tus.'!</i>',
                                    'typ' => 'danger');
                            }
                        }
                    }elseif($tod == "delete"){
                        $dis = $this->loginm->getailalt('user', array('surel' => $tus), 'disable');

                        if($dis){
                            $pho = $this->loginm->getailalt('user', array('surel' => $tus), 'photo');
                            if($pho != "default.png"){
                                unlink('./asset/uspic/'.$pho);
                            }
                            $cek = $this->loginm->delete('user', array('surel' => $tus));
                            if($cek != FALSE){
                                $errmsg[] = array('ico' => 'ti-check', 'txt' => '<b>OK: </b><br><i>'.$tus.' deleted!</b>',
                                    'typ' => 'success');
                            }else{
                                $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Cant delete '.$tus.'!</i>',
                                    'typ' => 'danger');
                            }
                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Cant Delete '.$tus.'!<br><sub>This Features available if you blocking first User.</sub></i>',
                                'typ' => 'warning');
                        }
                    }else{
                        $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Operation is Unknown Command</sub></i>',
                            'typ' => 'danger');
                    }
                }else{
                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request</i>',
                        'typ' => 'danger');
                }
                $this->session->set_flashdata('info', $errmsg);
                redirect('dashboard/mngusr');
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
            redirect("login");
        }
    }

    public function evtodo(){
        if($this->loginm->chksess()){
            $this->load->library("safe");
            if($_SESSION['lv'] == 1){
                $errmsg = array();
                $mail = $_SESSION['mail'];
                $tod = $this->input->get("todo", TRUE);
                if(!empty($tod)){
                    if($tod == "new"){
                        $this->form_validation->set_rules('enam', 'Event Name', 'required|min_length[5]|max_length[128]');
                        $this->form_validation->set_rules('alamat', 'Alamat Name', 'required|min_length[10]');
                        $this->form_validation->set_rules('info', 'Information Event', 'required|min_length[10]');
                        $this->form_validation->set_rules('date', 'Date Event', 'required');
                        $this->form_validation->set_rules('web', 'Offical Page Event', 'required|valid_url');

                        //Detecting Are Value is Valid?
                        if ($this->form_validation->run() == TRUE){
                            $nam = $this->input->post("enam", TRUE);
                            $alm = $this->input->post("alamat", TRUE);
                            $inf = $this->input->post("info", TRUE);
                            $dat = date('Y-m-d', strtotime($this->input->post("date", TRUE)));
                            $web = $this->input->post("web", TRUE);
                            $tus = $this->loginm->singkat($nam, 15);

                            $cek = $this->loginm->chkean('event', array('tgl' => $dat), array('by_user' => $mail));

                            if($cek != FALSE){
                                //Add to database
                                $cek1 = $this->loginm->addsm('event', array('nama' => $nam, 'alamat' => $alm,'keterangan' => $inf, 'tgl' => $dat, 'url'=>$web, 'by_user' => $mail));

                                if($cek1 != FALSE){
                                    $errmsg[] = array('ico' => 'ti-check', 'txt' => '<b>OK: Event </b><br><i>'.$tus.' added!</b>',
                                        'typ' => 'success');
                                    //Configuration Updoad Photos
                                    //$prna = time().$mail.$_FILES["profil"]['name']; //Change name of image
                                    $config['upload_path'] = './asset/evpic/'; //On "evpic" upload
                                    $config['allowed_types'] = 'jfif|jpg|jpeg|png|gif'; //Pitcure Only
                                    $config['max_size'] = '2048'; //2MB
                                    $config['encrypt_name'] = TRUE;
                                    //$config['file_name'] = $prna; //set new Name
                                    $this->load->library('upload', $config); //load library upload

                                    $cek2 = FALSE;

                                    //Photos is detected OK
                                    if ($this->upload->do_upload('foto')){
                                        //Get Filename
                                        $fnm = $this->upload->data('file_name');
                                        chmod('./asset/evpic/'.$fnm, 0777);
                                        //Update user table on "Photo" column where Mail as ID
                                        $this->loginm->upev(array('by_user' => $mail), array('tgl' => $dat), array('photo' => $fnm));

                                        $errmsg[] = array('ico' => 'ti-check', 'txt' => '<b>OK: Event Photo </b><br><i>'.$tus.' added!</b>',
                                            'typ' => 'success');
                                    }else{
                                        $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Info: </b><br><i>'.$this->upload->display_errors().'</b>',
                                            'typ' => 'warning');
                                    }
                                    
                                }else{
                                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Cant adding '.$tus.'!</i>',
                                        'typ' => 'danger');
                                }
                            }else{
                                $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Exists Event on this date!</i>',
                                    'typ' => 'danger');
                            }
                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Warning: </b><br><i>'.preg_replace("/(\n)+/m", '<br>', strip_tags(strip_tags(validation_errors()))).'</i>',
                            'typ' => 'warning');
                        }
                    }elseif($tod == "delete"){
                        $id = $this->input->get("id", TRUE);
                        $mail = $_SESSION['mail'];
                        $key = $this->input->get("key", TRUE);
                        if(!empty($id) && !empty($key)){
                            $name = $this->loginm->singkat($this->loginm->getailalt('event', array('id' => $id), 'nama'), 15);
                            $pho = $this->loginm->getailalt('jual', array('id' => $id), 'foto');                            
                            $key = $this->safe->dencrypt($key, $mail);

                            if($name == $key){
                                if($pho != "default.png"){
                                    unlink('./asset/evpic/'.$pho);
                                }
                                $cek = $this->loginm->delete('event', array('id' => $id));
                                if($cek != FALSE){
                                    $errmsg[] = array('ico' => 'ti-check', 'txt' => '<b>OK: </b><br><i>'.$key.' deleted!</b>',
                                        'typ' => 'success');
                                }else{
                                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Cant delete '.$key.'!</i>',
                                        'typ' => 'danger');
                                }
                            }else{
                                $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Address is Cant be Understood</sub></i>',
                                    'typ' => 'danger');
                            }

                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Incomplete address</sub></i>',
                                'typ' => 'danger');
                        }

                    }else{
                        $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Operation is Unknown Command</sub></i>',
                            'typ' => 'danger');
                    }

                }else{
                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Action Address Needed</sub></i>',
                        'typ' => 'danger');
                }
                $this->session->set_flashdata('info', $errmsg);
                redirect('dashboard/event');
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
            redirect("login");
        }
    }

    public function votodo(){
        if($this->loginm->chksess()){
            $this->load->library("safe");

            if($_SESSION['lv'] == 1){
                $errmsg = array();
                $mail = $_SESSION['mail'];
                $tod = $this->input->get("todo", TRUE);
                if(!empty($tod)){
                    if($tod == "new"){
                        $this->form_validation->set_rules('namvoc', 'Voucer Name', 'required|min_length[5]|max_length[128]');
                        $this->form_validation->set_rules('povoc', 'Point', 'required|is_natural_no_zero');
                        $this->form_validation->set_rules('date', 'Expired date', 'required');

                        //Detecting Are Value is Valid?
                        if ($this->form_validation->run() == TRUE){
                            $nam = $this->input->post("namvoc", TRUE);
                            $pnt = $this->input->post("povoc", TRUE);
                            $dat = date('Y-m-d', strtotime($this->input->post("date", TRUE)));
                            $tus = $this->loginm->singkat($nam, 15);
                            $value = rand(0,99);
                            $cde = str_pad($value, 2, '0', STR_PAD_LEFT);
                            $kode = strtoupper(substr($nam, 0, 2).date('ymd', strtotime($dat)).$cde.substr($mail, 1, 2));

                            $cek = $this->loginm->chkean('voucer', array('kode' => $kode), array('by_user' => $mail));

                            if($cek != FALSE){
                                //Add to database
                                $cek1 = $this->loginm->addsm('voucer', array('nama' => $nam, 'kode' => $kode, 'poin' => $pnt, 'expired' => $dat, 'by_user' => $mail));

                                if($cek1 != FALSE){
                                    $errmsg[] = array('ico' => 'ti-check', 'txt' => '<b>OK: Voucher </b><br><i>'.$tus.' added!</b>',
                                        'typ' => 'success');
                                    //Configuration Updoad Photos
                                    //$prna = time().$mail.$_FILES["profil"]['name']; //Change name of image
                                    $config['upload_path'] = './asset/vopic/'; //On "vopic" upload
                                    $config['allowed_types'] = 'jfif|jpg|jpeg|png|gif'; //Pitcure Only
                                    $config['max_size'] = '2048'; //2MB
                                    $config['encrypt_name'] = TRUE;
                                    //$config['file_name'] = $prna; //set new Name
                                    $this->load->library('upload', $config); //load library upload

                                    $cek2 = FALSE;

                                    //Photos is detected OK
                                    if ($this->upload->do_upload('foto')){
                                        //Get Filename
                                        $file = $this->upload->data('file_name');
                                        chmod('./asset/vopic/'.$file, 0777);
                                        //Update user table on "Photo" column where Mail as ID
                                        $this->loginm->upvo(array('kode' => $kode), array('foto' => $file));
                                        
                                        $errmsg[] = array('ico' => 'ti-check', 'txt' => '<b>OK: Voucher Photo </b><br><i>'.$tus.' added!</b>',
                                            'typ' => 'success');
                                    }else{
                                        $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Info: </b><br><i>'.$this->upload->display_errors().'</b>',
                                            'typ' => 'warning');
                                    }
                                    
                                }else{
                                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Cant adding '.$tus.'!</i>',
                                        'typ' => 'danger');
                                }
                            }else{
                                $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Exists Voucher!</i>',
                                    'typ' => 'danger');
                            }
                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Warning: </b><br><i>'.preg_replace("/(\n)+/m", '<br>', strip_tags(strip_tags(validation_errors()))).'</i>',
                            'typ' => 'warning');
                        }
                    }elseif($tod == "delete"){
                        $id = $this->input->get("id", TRUE);
                        $mail = $_SESSION['mail'];
                        $key = $this->input->get("key", TRUE);
                        if(!empty($id) && !empty($key)){
                            $name = $this->loginm->getailalt('voucer', array('id' => $id), 'kode');
                            $pho = $this->loginm->getailalt('voucer', array('id' => $id), 'foto');
                            $key = $this->safe->dencrypt($key, $mail);

                            if($name == $key){
                                if($pho != "default.png"){
                                    unlink('./asset/vopic/'.$pho);
                                }
                                $cek = $this->loginm->delete('voucer', array('id' => $id));
                                if($cek != FALSE){
                                    $errmsg[] = array('ico' => 'ti-check', 'txt' => '<b>OK: </b><br><i>'.$key.' deleted!</b>',
                                        'typ' => 'success');
                                }else{
                                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Cant delete '.$key.'!</i>',
                                        'typ' => 'danger');
                                }
                            }else{
                                $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Address is Cant be Understood</sub></i>',
                                    'typ' => 'danger');
                            }

                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Incomplete address</sub></i>',
                                'typ' => 'danger');
                        }

                    }else{
                        $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Operation is Unknown Command</sub></i>',
                            'typ' => 'danger');
                    }

                }else{
                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Action Address Needed</sub></i>',
                        'typ' => 'danger');
                }
                $this->session->set_flashdata('info', $errmsg);
                redirect('dashboard/voucer');
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
            redirect("login");
        }
    }
    
    public function sotodo(){
        if($this->loginm->chksess()){
            $this->load->library("safe");

            if($_SESSION['lv'] == 1){
                $errmsg = array();
                $mail = $_SESSION['mail'];
                $tod = $this->input->get("todo", TRUE);
                if(!empty($tod)){
                    if($tod == "new"){
                        $this->form_validation->set_rules('nambar', 'Voucer Name', 'required|min_length[5]|max_length[128]');
                        $this->form_validation->set_rules('detail', 'Details', 'required|min_length[5]');
                        $this->form_validation->set_rules('snk', 'Terms and Conditions', 'required|min_length[5]');
                        $this->form_validation->set_rules('harga', 'Price', 'required|is_natural');
                        $this->form_validation->set_rules('poin', 'Point', 'required|is_natural_no_zero');
                        $this->form_validation->set_rules('hardis', 'Price after discount', 'required|is_natural');
                        $this->form_validation->set_rules('guna', 'Usage', 'required|min_length[5]');
                        $this->form_validation->set_rules('date', 'Expired', 'required');
                        $this->form_validation->set_rules('url', 'URL', 'required');

                        //Detecting Are Value is Valid?
                        if ($this->form_validation->run() == TRUE){
                            $nam = $this->input->post("nambar", TRUE);
                            $det = $this->input->post("detail", TRUE);
                            $snk = $this->input->post("snk", TRUE);
                            $pri = $this->input->post("harga", TRUE);
                            $pnt = $this->input->post("poin", TRUE);
                            $htu = $this->input->post("guna", TRUE);
                            $pap = $this->input->post("hardis", TRUE);
                            $url = $this->input->post("url", TRUE);
                            //$fpi = $this->input->post("file", TRUE);
                            $dat = date('Y-m-d', strtotime($this->input->post("date", TRUE)));
                            $tus = $this->loginm->singkat($nam, 15);

                            $cek = $this->loginm->chkean('jual', array('nama' => $nam), array('by_user' => $mail));

                            if($cek != FALSE){
                                //Add to database
                                $array = array('nama' => $nam, 'detail' => $det, 'harga' => $pri, 'harga_poin' => $pap, 'ambil_poin' => $pnt, 'cara_guna' => $htu, 'exp' => $dat, 'tnc' => $snk, 'by_user' => $mail, 'address' => $url);
                                $cek1 = $this->loginm->addsm('jual', $array);

                                if($cek1 != FALSE){
                                    $errmsg[] = array('ico' => 'ti-check', 'txt' => '<b>OK: Goods Shop </b><br><i>'.$tus.' added!</b>',
                                        'typ' => 'success');
                                    //Configuration Updoad Photos
                                    //$prna = time().$mail.$_FILES["profil"]['name']; //Change name of image
                                    $config['upload_path'] = './asset/sopic/'; //On "sopic" upload
                                    $config['allowed_types'] = 'jfif|jpg|jpeg|png|gif'; //Pitcure Only
                                    $config['max_size'] = '2048'; //2MB
                                    $config['encrypt_name'] = TRUE;
                                    //$config['file_name'] = $prna; //set new Name
                                    $this->load->library('upload', $config); //load library upload

                                    $id = $this->loginm->getshopid(array("nama" => $nam), array("exp" => $dat));

                                    //Photos is detected OK
                                    if ($this->upload->do_upload('foto')){
                                        //Get Filename
                                        $fnm = $this->upload->data('file_name');
                                        chmod('./asset/sopic/'.$fnm, 0777);
                                        //Update user table on "Photo" column where Mail as ID
                                        $cek = $this->loginm->upsh('jual', $array, array('foto' => $fnm));

                                        if($cek != FALSE){
                                            $errmsg[] = array('ico' => 'ti-check', 'txt' => '<b>OK: Shop Photo </b><br><i>'.$tus.' added!</b>',
                                                'typ' => 'success');
                                        }else{
                                            $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Info: Shop Photo Cant save to </b><br><i>'.$tus.' added!</b>',
                                                'typ' => 'warning');
                                        }
                                    }else{
                                        $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Info: </b><br><i>'.$this->upload->display_errors().'</b>',
                                                'typ' => 'warning');
                                    }

                                    if(!empty($_FILES['file'])){
                                        //Configuration Updoad Photos
                                        //$prna = time().$mail.$_FILES["profil"]['name']; //Change name of image
                                        $config2['upload_path'] = './asset/sdpic/'; //On "userpic" upload
                                        $config2['allowed_types'] = 'jpe|jpg|jpeg|png'; //Jpg/Jpeg Only
                                        $config2['max_size'] = '2048'; //2MB
                                        $config2['encrypt_name'] = TRUE;
                                        //$config['file_name'] = $prna; //set new Name
                                            
                                        $files = $_FILES;

                                        $cpt = count($_FILES['file']['name']);

                                        for($i=0; $i<$cpt; $i++){           
                                            $_FILES['userfile']['name']= $files['file']['name'][$i];
                                            $_FILES['userfile']['type']= $files['file']['type'][$i];
                                            $_FILES['userfile']['tmp_name']= $files['file']['tmp_name'][$i];
                                            $_FILES['userfile']['error']= $files['file']['error'][$i];
                                            $_FILES['userfile']['size']= $files['file']['size'][$i];    

                                            $this->upload->initialize($config2); //modify the existing loaded library

                                            //Photos is detected OK
                                            if ($this->upload->do_upload('userfile')){
                                                //Get Filename
                                                $fnm = $this->upload->data('file_name');
                                                chmod('./asset/sdpic/'.$fnm, 0777);
                                                //Update user table on "Photo" column where Mail as ID
                                                $idt = $this->loginm->getailalt('jual', $array, 'id');
                                                $arim = array('id_jual' => $idt, 'foto' => $fnm);
                                                $this->loginm->addsm('jual_img', $arim);
                                            }else{
                                                $errmsg[] = array('ico' => 'ti-info', 'txt' => "<b>Info: </b<i>Photo Details $x : ".$this->upload->display_errors()."</i>",
                                                    'typ' => 'warning');
                                            }
                                        }

                                    }else{
                                        $errmsg[] = array('ico' => 'ti-info', 'txt' => "<b>Info: </b<i>No Detail Pitcures Selected</i>",
                                            'typ' => 'warning');
                                    }

                                }else{
                                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Cant adding '.$tus.'!</i>',
                                        'typ' => 'danger');
                                }
                            }else{
                                $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Exists Goods!</i>',
                                    'typ' => 'danger');
                            }
                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Warning: </b><br><i>'.preg_replace("/(\n)+/m", '<br>', strip_tags(strip_tags(validation_errors()))).'</i>',
                            'typ' => 'warning');
                        }
                    }elseif($tod == "delete"){
                        $id = $this->input->get("id", TRUE);
                        $mail = $_SESSION['mail'];
                        $key = $this->input->get("key", TRUE);
                        if(!empty($id) && !empty($key)){
                            $name = $this->loginm->singkat($this->loginm->getailalt('jual', array('id' => $id), 'nama'),15);
                            $pho = $this->loginm->getailalt('jual', array('id' => $id), 'foto');
                            $key = $this->safe->dencrypt($key, $mail);

                            if($name == $key){
                                $get = $this->db->from('jual_img')->where('id_jual', $id)->get();
                                if($get !== FALSE && $get->num_rows() > 0){
                                    foreach($get->result() as $hit){
                                        $foto = $hit->foto;
                                        if($foto != "default.png"){
                                            unlink('./asset/sdpic/'.$foto);
                                        }
                                    }
                                }
                                if($pho != "default.png"){
                                    unlink('./asset/sopic/'.$pho);
                                }
                                $cek = $this->loginm->delete('jual', array('id' => $id));
                                if($cek != FALSE){
                                    $errmsg[] = array('ico' => 'ti-check', 'txt' => '<b>OK: </b><br><i>'.$key.' deleted!</b>',
                                        'typ' => 'success');
                                }else{
                                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Cant delete '.$key.'!</i>',
                                        'typ' => 'danger');
                                }
                            }else{
                                $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Address is Cant be Understood</sub></i>',
                                    'typ' => 'danger');
                            }

                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Incomplete address</sub></i>',
                                'typ' => 'danger');
                        }

                    }else{
                        $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Operation is Unknown Command</sub></i>',
                            'typ' => 'danger');
                    }

                }else{
                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Action Address Needed</sub></i>',
                        'typ' => 'danger');
                }
                $this->session->set_flashdata('info', $errmsg);
                redirect('dashboard/shop');
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
            redirect("login");
        }
    }

    public function volis(){
        if($_SESSION['lv'] == 1){
            $usm = $this->input->post("id",TRUE);
            $mail = $_SESSION['mail'];
            $list = $this->loginm->getean("voucer", array('by_user' => $mail));
            if(!empty($usm) && $list != FALSE){

                foreach($list as $voc){
                
                    $vol = $this->loginm->getean('user_voucer', array('id_voucer' => $voc['id']), array('mail_user' => $usm));
                
                    if($vol != FALSE){

                        foreach($vol as $hit){
                            $kode = $this->loginm->getailalt('voucer', array('id' => $hit['id_voucer']), 'kode');
                            $nama = $this->loginm->getailalt('voucer', array('id' => $hit['id_voucer']), 'nama');
                            echo "<option value=\"$hit[id]\">$kode - $nama</option>";
                        }
                    }
                }
            }
        }
    }

    public function redmvoc(){
        if($this->loginm->chksess() && $this->loginm->placeid() != null){
            if($_SESSION['lv'] == 1){
                //load model waterm
                $this->load->model('waterm');

                //get Voucher
                $this->form_validation->set_rules('uservoc', 'User Mail', 'required|valid_email');
                $this->form_validation->set_rules('voucer', 'Voucer Code', 'required');

                if ($this->form_validation->run() == TRUE){
                    //get voucher
                    echo $usr = $this->input->post("uservoc", TRUE);
                    $voc = $this->input->post("voucer", TRUE);
                    echo $idv = $this->loginm->getailalt('voucer', array('kode' => $voc), 'id');

                    //okok

                    $cek = $this->loginm->delete('user_voucer', array('id_voucer' => $voc, 'mail_user' => $usr), TRUE);
                    //$this->loginm->addcom($usr, $mtp);

                    if($cek != FALSE){

                        $errmsg[] = array('ico' => 'ti-check', 'txt' => "<b>OK: </b><br><i>Voucher is get!</i>",
                            'typ' => 'success');
                    }else{
                        $errmsg[] = array('ico' => 'ti-close', 'txt' => "<b>Error: </b><br><i>Get Voucher Failed!</i>",
                            'typ' => 'danger');
                    }
                }else{
                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Warning: </b><br><i>'.preg_replace("/(\n)+/m", '<br>', strip_tags(strip_tags(validation_errors()))).'</i>',
                        'typ' => 'warning');
                }
                $this->session->set_flashdata('info', $errmsg);
                redirect("dashboard");
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
            redirect("login");
        }
    }

    public function npltd(){
        if($this->loginm->chksess()){
            $this->load->library("safe");

            if($_SESSION['lv'] == 0){
                $errmsg = array();
                $mail = $_SESSION['mail'];
                $tod = $this->input->get("todo", TRUE);
                $key = $this->input->get("plac", TRUE);
                if(!empty($tod) && !empty($key)){
                    $plc = $this->safe->dencrypt($key, $mail);
                    $cek = $this->loginm->chkean('tempat_baru', array('id_tempat' => $plc));
                    if($cek == FALSE){
                        $name = $this->loginm->getailalt('tempat_baru', array('id_tempat' => $plc), 'nama');
                        if($tod == "ok"){
                            $stat = $this->loginm->getailalt('tempat_baru', array('id_tempat' => $plc), 'relasi');

                            if($stat == "owner"){
                                $surel = $this->loginm->getailalt('tempat_baru', array('id_tempat' => $plc), 'pendaftar');
                                $nam = $this->loginm->getailalt('tempat_baru', array('id_tempat' => $plc), 'nama');
                                $tip = $this->loginm->getailalt('tempat_baru', array('id_tempat' => $plc), 'tipe');
                                $lat = $this->loginm->getailalt('tempat_baru', array('id_tempat' => $plc), 'latitude');
                                $lon = $this->loginm->getailalt('tempat_baru', array('id_tempat' => $plc), 'longitude');
                                $ala = $this->loginm->getailalt('tempat_baru', array('id_tempat' => $plc), 'alamat');
                                $img = $this->loginm->getimgf($plc);
                                
                                $this->loginm->upsh('user', array('surel' => $surel), array('level' => '1'));

                                $array = array('nama' => $nam, 'alamat' => $ala, 'foto' => $img, 'detail' => '-', 'poin' => 0, 'premium' => 'n',
                                    'jam_buka' => '00:00:00', 'jam_tutup' => '00:00:00', 'harga' => '0', 'latitude' => $lat, 'longitude' => $lon);
                                $cek1 = $this->loginm->addsm('tempatisi', $array);

                                if($img != "default.png"){
                                    rename("./asset/nppic/$img", "./asset/plpic/$img");
                                }

                                $ids = $this->loginm->getailalt('tempatisi', array('nama' => $nam, 'latitude' => $lat, 'longitude' => $lon), 'id');
                                
                                $ima = $this->loginm->getimnli($img, $ids);
                                if($ima != FALSE){foreach($ima as $hit){
                                    $this->loginm->addsm('img_tempat', array('id_tempat' => $ids, 'image' => $hit['image']));
                                    
                                    if($hit['image'] != "default.png"){
                                    rename("./asset/nppic/$hit[image]", "./asset/papic/$hit[image]");
                                    }
                                }}

                                $this->loginm->addsm('kepemilikan', array('surel' => $surel, 'id_tempat' => $ids));

                                if($cek1 != FALSE){
                                    $this->loginm->delete('tempat_baru', array('id_tempat' => $plc));
                                    $this->loginm->delete('pic_tmpt_baru', array('id_tempat' => $plc));

                                    $errmsg[] = array('ico' => 'ti-check', 'txt' => "<b>OK: </b><br><i>Place $name Added!</i>",
                                        'typ' => 'success');
                                }else{
                                    $errmsg[] = array('ico' => 'ti-close', 'txt' => "<b>Error: </b><br><i>Place \"$name\" Added Failed!</i>",
                                        'typ' => 'danger');
                                }
                            }else{
                                redirect("dashboard/addusr?aduspl=$key");
                            }
                        }elseif($tod == "delete"){
                            $get = $this->loginm->getean('pic_tmpt_baru', array('id_tempat' => $plc));
                            if($get != FALSE){
                                foreach($get as $hit){
                                    unlink("./asset/nppic/$hit[image]");
                                }
                                $this->loginm->delete('pic_tmpt_baru', array('id_tempat' => $plc));
                            }
                            $cek2 = $this->loginm->delete('tempat_baru', array('id_tempat' => $plc));
                            if($cek2 != FALSE){
                                $errmsg[] = array('ico' => 'ti-check', 'txt' => "<b>OK: </b><br><i>Place $name Deleted!</i>",
                                    'typ' => 'success');
                            }else{
                                $errmsg[] = array('ico' => 'ti-close', 'txt' => "<b>Error: </b><br><i>Delete $name Failed!</i>",
                                    'typ' => 'danger');
                            }
                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Cant be Understood Address!</sub></i>',
                                'typ' => 'warning');
                        }
                    }else{
                        $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>No Place!<br><sub>Place is doesnt exists.</sub></i>',
                            'typ' => 'warning');
                    }
                }else{
                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>Invalid Address Request.<br><sub>Action Address Needed!</sub></i>',
                        'typ' => 'danger');
                }
                $this->session->set_flashdata('info', $errmsg);
                redirect('dashboard/npu');
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
        //If Doesnt Login go to login
        redirect("login");
        }
    }

    public function mngplcz(){
        if($this->loginm->chksess()){
            $this->load->library("safe");

            if($_SESSION['lv'] == 0){
                $errmsg = array();
                $mail = $_SESSION['mail'];
                $key = $this->input->get("plac", TRUE);

                $plc = $this->safe->dencrypt($key, $mail);
                $cek = $this->loginm->infoex('tempatisi', array('id' => $plc));
                if($cek != FALSE){
                    $img = $this->loginm->getailalt('tempatisi', array('id' => $plc), 'foto');
                    if($img != "default.png"){
                        unlink("./asset/plpic/$img");
                    }
                    $get = $this->loginm->getean('img_tempat', array('id_tempat' => $plc));
                    if($get != FALSE){
                        foreach($get as $hit){
                            unlink("./asset/papic/$hit[image]");
                        }
                    }

                    $user = $this->loginm->getailalt('kepemilikan', array('id_tempat' => $plc), 'surel');
                    $this->loginm->delete('kepemilikan', array('surel' => $user), TRUE);
                    $this->loginm->delete('user', array('surel' => $user), TRUE);

                    $this->loginm->delete('img_tempat', array('id_tempat' => $plc));
                    $cek2 = $this->loginm->delete('tempatisi', array('id' => $plc));
                    if($cek2 != FALSE){
                        $errmsg[] = array('ico' => 'ti-check', 'txt' => "<b>OK: </b><br><i>Place $name Deleted!</i>",
                            'typ' => 'success');
                    }else{
                        $errmsg[] = array('ico' => 'ti-close', 'txt' => "<b>Error: </b><br><i>Delete $name Failed!</i>",
                            'typ' => 'danger');
                    }
                }else{
                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Error: </b><br><i>No Place!<br><sub>Place is doesnt exists.</sub></i>',
                        'typ' => 'warning');
                }

                $this->session->set_flashdata('info', $errmsg);
                redirect('dashboard/mngplc');
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
        //If Doesnt Login go to login
        redirect("login");
        }
    }

    public function addpln(){
        if($this->loginm->chksess()){
            if($_SESSION['lv'] == 0){
                $this->load->library("safe");
                $mail = $_SESSION['mail'];
                $errmsg = array();

                $this->form_validation->set_rules('nama', 'Name Place', 'required');
                $this->form_validation->set_rules('alamat', 'Address', 'required');
                $this->form_validation->set_rules('lon', 'Longitude', 'required');
                $this->form_validation->set_rules('lat', 'Latitude', 'required');
                if ($this->form_validation->run() == TRUE){
                    $nam = $this->input->post("nama", TRUE);
                    $add = $this->input->post("alamat", TRUE);
                    $lon = $this->input->post("lon", TRUE);
                    $lat = $this->input->post("lat", TRUE);

                    $cek = $this->loginm->chkean('tempat_baru', array('longitude' => $lon, 'latitude' => $lat, 'nama' => $nam), TRUE);
                    $cek1 = $this->loginm->chkean('tempatisi', array('longitude' => $lon, 'latitude' => $lat, 'nama' => $nam), TRUE);

                    if($cek == TRUE && $cek1 == TRUE){
                        $cek2 = $this->loginm->addsm('tempat_baru', array('nama' => $nam, 'relasi' => 'customer', 'tipe' => 'other', 'longitude' => $lon, 'latitude' => $lat, 'alamat' => $add, 'pendaftar' => $mail));
                        $ids = $this->loginm->getailalt('tempat_baru', array('nama' => $nam, 'relasi' => 'customer', 'tipe' => 'other', 'longitude' => $lon, 'latitude' => $lat, 'alamat' => $add, 'pendaftar' => $mail), 'id_tempat');

                        if($cek2 != FALSE){
                            $errmsg[] = array('ico' => 'ti-check', 'txt' => "<b>OK: </b><br><i>Place $nam Added!</i>",
                                'typ' => 'success');
                            
                            $key = $this->safe->encrypt($ids, $_SESSION['mail']);
                            redirect("dashboard/addusr?aduspl=$key");                            
                        }else{
                            $errmsg[] = array('ico' => 'ti-close', 'txt' => "<b>Error: </b><br><i>Place $nam Failed to Add!</i>",
                                'typ' => 'danger');
                        }

                    }else{
                        $errmsg[] = array('ico' => 'ti-info', 'txt' => "<b>Warning: </b><br><i>Place Already Exists</i>",
                        'typ' => 'warning');
                    }

                }else{
                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Warning: </b><br><i>'.preg_replace("/(\n)+/m", '<br>', strip_tags(strip_tags(validation_errors()))).'</i>',
                        'typ' => 'warning');
                }

                $this->session->set_flashdata('info', $errmsg);
                redirect('dashboard/adpl');
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
            redirect("login");
        }
    }

    public function addus(){
        if($this->loginm->chksess()){
            if($_SESSION['lv'] == 0){
                $mail = $_SESSION['mail'];
                $errmsg = array();
                
                if(!empty($_GET['aduspl'])){
                    $key = $this->input->get("aduspl", TRUE);

                    $this->load->library("safe");

                    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
                    $this->form_validation->set_rules('nd', 'First Name', 'required');
                    $this->form_validation->set_rules('nb', 'Last Name', 'required');
                    $this->form_validation->set_rules('pass', 'Password', 'required|min_length[4]');

                    if ($this->form_validation->run() == TRUE){
                        $sur = $this->input->post("email", TRUE);
                        $nad = $this->input->post("nd", TRUE);
                        $nab = $this->input->post("nb", TRUE);
                        $pas = $this->input->post("pass", TRUE);

                        $plc = $this->safe->dencrypt($key, $mail);

                        //$surel = $this->loginm->getailalt('tempat_baru', array('id_tempat' => $plc), 'pendaftar');
                        $nam = $this->loginm->getailalt('tempat_baru', array('id_tempat' => $plc), 'nama');
                        $tip = $this->loginm->getailalt('tempat_baru', array('id_tempat' => $plc), 'tipe');
                        $lat = $this->loginm->getailalt('tempat_baru', array('id_tempat' => $plc), 'latitude');
                        $lon = $this->loginm->getailalt('tempat_baru', array('id_tempat' => $plc), 'longitude');
                        $ala = $this->loginm->getailalt('tempat_baru', array('id_tempat' => $plc), 'alamat');
                        $img = $this->loginm->getimgf($plc);
                        
                        $cek = $this->loginm->chkean('user', array('surel' => $sur), TRUE);

                        if($cek == TRUE){
                            $cek1 = $this->loginm->addsm('user', array('surel'=> $sur, 'namadepan' => $nad, 'namabelakang' => $nab, 'sandi' => $pas, 'lahir' => date("Y-m-d"), 'negara' => '-',
                                'prov' => '-', 'kota' => '-', 'jalan' => '-', 'poin' => 0, 'disable' => 0, 'progress' => 0, 'level' => '1'));
                            //$this->loginm->upsh('user', array('surel' => $surel), array('level' => '1'));

                            $array = array('nama' => $nam, 'alamat' => $ala, 'foto' => $img, 'detail' => '-', 'poin' => 0, 'premium' => 'n',
                                'jam_buka' => '00:00:00', 'jam_tutup' => '00:00:00', 'harga' => '0', 'latitude' => $lat, 'longitude' => $lon);
                            $cek1 = $this->loginm->addsm('tempatisi', $array);

                            if($img != "default.png"){
                                rename("./asset/nppic/$img", "./asset/plpic/$img");
                            }

                            $ids = $this->loginm->getailalt('tempatisi', array('nama' => $nam, 'latitude' => $lat, 'longitude' => $lon), 'id');

                            $ima = $this->loginm->getimnli($img, $ids);
                            if($ima != FALSE){foreach($ima as $hit){
                                $this->loginm->addsm('img_tempat', array('id_tempat' => $ids, 'image' => $hit['image']));

                                if($hit['image'] != "default.png"){
                                    rename("./asset/nppic/$hit[image]", "./asset/papic/$hit[image]");
                                }
                            }}

                            $this->loginm->addsm('kepemilikan', array('surel' => $sur, 'id_tempat' => $ids));

                            if($cek1 != FALSE){
                                $this->loginm->delete('tempat_baru', array('id_tempat' => $plc));
                                $this->loginm->delete('pic_tmpt_baru', array('id_tempat' => $plc));

                                $errmsg[] = array('ico' => 'ti-check', 'txt' => "<b>OK: </b><br><i>Place \"$name\" and User \"$nad\" Added!</i>",
                                    'typ' => 'success');
                            }else{
                                $errmsg[] = array('ico' => 'ti-close', 'txt' => "<b>Error: </b><br><i>Place \"$name\" and User \"$nad\" Failed to Add!</i>",
                                    'typ' => 'danger');
                            }
                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Warning: </b><br><i>Email Address Is Registered with other user!</i>',
                                'typ' => 'warning');

                            $this->session->set_flashdata('info', $errmsg);
                            redirect("dashboard/addusr?aduspl=$key");
                        }

                    }else{
                        $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Warning: </b><br><i>'.preg_replace("/(\n)+/m", '<br>', strip_tags(strip_tags(validation_errors()))).'</i>',
                            'typ' => 'warning');

                        $this->session->set_flashdata('info', $errmsg);
                        redirect("dashboard/addusr?aduspl=$key");
                    }

                }else{
                    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
                    $this->form_validation->set_rules('nd', 'First Name', 'required');
                    $this->form_validation->set_rules('nb', 'Last Name', 'required');
                    $this->form_validation->set_rules('pass', 'Password', 'required|min_length[4]');;
                    $this->form_validation->set_rules('level', 'User Level', 'required|regex_match[/0-2/]');

                    if ($this->form_validation->run() == TRUE){
                        $sur = $this->input->post("email", TRUE);
                        $nad = $this->input->post("nd", TRUE);
                        $nab = $this->input->post("nb", TRUE);
                        $pas = $this->input->post("pass", TRUE);
                        $lvl = $this->input->post("level", TRUE);

                        $cek = $this->loginm->chkean('user', array('mail' => $sur) , TRUE);

                        if($cek != FALSE){
                            $cek1 = $this->loginm->addsm('user', array('surel'=> $sur, 'namadepan' => $nad, 'namabelakang' => $nab, 'sandi' => $pas, 'lahir' => date("Y-m-d"), 'negara' => '-',
                            'prov' => '-', 'kota' => '-', 'jalan' => '-', 'poin' => 0, 'disable' => 0, 'progress' => 0, 'level' => $lvl));

                            if($cek1 != FALSE){
                                $errmsg[] = array('ico' => 'ti-check', 'txt' => "<b>OK: </b><br><i>User $nad Added!</i>",
                                    'typ' => 'success');
                            }else{
                                $errmsg[] = array('ico' => 'ti-close', 'txt' => "<b>Error: </b><br><i>User $nad Failed Add!</i>",
                                    'typ' => 'danger');
                            }
                        }else{
                            $errmsg[] = array('ico' => 'ti-info', 'txt' => "<b>Warning: </b><br><i>User Mail Already Exists</i>",
                                'typ' => 'warning');
                        }

                    }else{
                        $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Warning: </b><br><i>'.preg_replace("/(\n)+/m", '<br>', strip_tags(strip_tags(validation_errors()))).'</i>',
                            'typ' => 'warning');
                    }

                }

                $this->session->set_flashdata('info', $errmsg);
                redirect('dashboard');
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
            redirect("login");
        }
    }

}