<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Water extends CI_Controller {

    public function __construct(){
        parent::__construct();
        //load model and library
        $this->load->model('waterm');
        $this->load->model('forgot');
        $this->load->library('safe');
        $this->load->library('user_agent');
    }

    public function index(){
    #if ($this->agent->is_mobile()){
      header('Content-Type: application/json; charset=UTF-8;');
      //Getting get value what todo
      $stat = array('status' => '404', 'msg' => "Error : Not Found!");
      $go = $this->input->get("do", TRUE);
      switch ($go){
        case 'register'://Create New Account
          //Values requipment here
          $this->form_validation->set_rules('namadpn', 'First Name', 'required');
          $this->form_validation->set_rules('namablk', 'Last Name', 'required');
          $this->form_validation->set_rules('mail', 'Email', 'required|valid_email|trim|min_length[6]|max_length[64]');
          $this->form_validation->set_rules('mail1', 'Email Validation', 'required|valid_email|trim|min_length[6]|max_length[64]');
          $this->form_validation->set_rules('pass', 'Password', 'required|min_length[4]|max_length[64]');
          $this->form_validation->set_rules('pass1', 'Password Validation', 'required|min_length[4]|max_length[64]');
          $this->form_validation->set_rules('tgl', 'Birth of Date', 'required');
          $this->form_validation->set_rules('ngr', 'Country', 'required');
          $this->form_validation->set_rules('prov', 'State/Province', 'required');
          $this->form_validation->set_rules('kot', 'City', 'required');
          $this->form_validation->set_rules('jln', 'Street', 'required');

          //Detecting Are Value is Valid?
          if ($this->form_validation->run() == TRUE){
            $mail = $this->input->post("mail",TRUE);
            $mail1 = $this->input->post("mail1",TRUE);
            $pass = $this->input->post("pass",TRUE);
            $pass1 = $this->input->post("pass1",TRUE);

            //Detecting if Different Values On Email and Password beetween they verification
            if($mail != $mail1){
              if($pass != $pass1){
                //If Password And Mail Not Match/Different
                $stat = array('status' => '400','msg' => 'New Email and New Password is not Match with they Confirmation!');
              }else{
                //Error Email Different
                $stat = array('status' => '400','msg' => 'Email is different!');
              }
            }elseif ($pass != $pass1){
              //Error Password Different
              $stat = array('status' => '400','msg' => 'Password is different!');
            }else{

              //Get data From POST method
              $ndp = $this->input->post("namadpn", TRUE);
              $nbl = $this->input->post("namablk", TRUE);
              $tgl = $this->input->post("tgl", TRUE);
              $ngr = $this->input->post("ngr", TRUE);
              $prov = $this->input->post("prov", TRUE);
              $kot = $this->input->post("kot", TRUE);
              $jln = $this->input->post("jln", TRUE);

              $ndp = ucwords(strtolower($ndp), '\',. ');
              $nbl = ucwords(strtolower($nbl), '\',. ');

              //Check Email avaiable?
              $cek = $this->waterm->chkreg("user", array('surel' => $mail));
              //if Email was usage before. Here!
              if($cek == FALSE){
                $stat = array('status' => '400','msg' => 'Email was Registered before!');
              }else{
                //Save to database
                $cek1 = $this->waterm->reg(array('surel' => $mail, 'namadepan' => $ndp,
                  'namabelakang' => $nbl, 'sandi' => $pass, 'lahir' => $tgl, 'negara' => $ngr,
                  'prov' => $prov, 'kota' => $kot, 'jalan' => $jln, 'poin' => 0, 'disable' => 0, 'progress' => 0, 'level' => '2'));

                if ($cek1 != FALSE){
                  $tmp = tempnam('./tmp', 'imgprotmp_'); // might not work on some systems, specify your temp path if system temp dir is not writeable
                  file_put_contents($tmp, base64_decode(str_replace(' ', '+',$this->input->post("img",TRUE))));
                  $imginf = getimagesize($tmp);
                  chmod($tmp, 0777);
                  $_FILES['userfile'] = array(
                      'name' => uniqid().'.'.preg_replace('!\w+/!', '', $imginf['mime']),
                      'tmp_name' => $tmp,
                      'size'  => filesize($tmp),
                      'error' => UPLOAD_ERR_OK,
                      'type'  => $imginf['mime'],
                  );
                  //Configuration Updoad Photos
                  //$prna = time().$mail.$_FILES["profil"]['name']; //Change name of image
                  $config['upload_path'] = './asset/uspic/'; //On "userpic" upload
                  $config['allowed_types'] = 'jfif|jpg|jpeg'; //Jpg and Png Only
                  $config['max_size'] = '2048'; //2MB
                  $config['encrypt_name'] = TRUE;
                  //$config['file_name'] = $prna; //set new Name
                  $this->load->library('upload', $config); //load library upload
                  $fnm = "default.png";

                  //Photos is detected OK
                  if ($this->upload->do_upload('userfile', true)){
                    //Get Filename
                    $fnm = $this->upload->data('file_name');
                    if($fnm == "default.png"){
                    chmod('./asset/uspic/'.$fnm, 0777);
                    //Update user table on "Photo" column where Mail as ID
                    $this->waterm->uptabusr(array('surel' => $mail), array('photo' => $fnm));
                    }
                  }
                  unlink($tmp);
                  //JSON send OK!
                  $fnm = base_url()."asset/uspic/".$fnm;
                  $stat = array('msg' => "Thank You for Join Us!",
                    'user' => array('email' => $mail, 'photo' => $fnm, 'namadpn' => $ndp, 'namablk' => $nbl),
                    'status' => '200');
                }else{
                  $stat = array('status' => '400','msg' => 'Error Register!');
                }
              }
            }
          }else{
            //Fail Validation Because illegal
            $stat = array('status' => '400','msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'login': //Login Application Code
          //Validation Mail and Password Required
          $this->form_validation->set_rules('mail', 'Email', 'required|valid_email|trim|min_length[6]|max_length[64]');
          $this->form_validation->set_rules('pass', 'Password', 'required|min_length[4]|max_length[64]');

          //Here if Validation Passed
          if ($this->form_validation->run() == TRUE){
            //get input from android with POST method
            $mail = $this->input->post("mail",TRUE);
            $pass = $this->input->post("pass",TRUE);
            //checking is user legal to login?
            $cek = $this->waterm->login(array('surel' => $mail), array('sandi' => $pass));
            

            //Checked is legal login?
            if ($cek != FALSE){

              //Creating Token Access
              $q = $this->waterm->gtok($mail);

              if($q != FALSE){
                //Getting data require send to android
                foreach($q as $q){
                  $tkn = $q['data'];
                  $dat = $q['date'];
                }
                foreach($cek as $cek){
                  $pho = base_url()."asset/uspic/".$cek->photo;
                  $ndp = $cek->namadepan;
                  $nbl = $cek->namabelakang;
                  $tgl = $cek->lahir;
                  $ngr = $cek->negara;
                  $pro = $cek->prov;
                  $kot = $cek->kota;
                  $jln = $cek->jalan;
                  $nbl = $cek->namabelakang;
                  $pnt = $cek->poin;
                  $pro = $cek->progress;
                  $dis = $cek->disable;
                }

                if($dis == 0){
                  //JSON data to android. Login is Successfull
                  $stat = array('msg' => "Welcome $ndp!",
                    'user' => array('token' => $tkn,'email' => $mail, 'photo' => $pho, 'namadpn' => $ndp, 'namablk' => $nbl,
                      'tgl' => $tgl, 'negara' => $ngr, 'prov' => $pro, 'kota' => $kot, 'jln' => $jln,
                      'poin' => $pnt, 'progress' => $pro, 'date' => $dat),
                    'status' => '200');
                }else{
                  //Error JSON fail Login
                  $stat = array('status' => '400', 'msg' => "Account Has been disabled! Please contacts Support Center.");
                }
              }else{
                //Error JSON fail Login
                $stat = array('status' => '400', 'msg' => "Can't create session with $mail!");
              }
            }else{
              //Error JSON fail Login
              $stat = array('status' => '400', 'msg' => "Failed Login As $mail");
            }

          }else{
            //Error JSON validation is fail
            $stat = array('status' => '400','msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'loginfb': //Login Facebook
          $this->form_validation->set_rules('nama', 'Name', 'required');
          $this->form_validation->set_rules('mail', 'Email', 'required|valid_email|trim|min_length[6]|max_length[64]');
          $this->form_validation->set_rules('tgl', 'Birth of Date', 'required');

          if ($this->form_validation->run() == TRUE){
            $mail = $this->input->post("mail",TRUE);
            //Check Email avaiable?
            $cek = $this->waterm->chkreg("user", array('surel' => $mail));
            //if Email was usage before. Here!
            if($cek == FALSE){
              $stat = array('status' => '200', 'msg' => 'User is registered. Now Log In!');

              //Checked is legal login?
              if ($cek == FALSE){

                //Creating Token Access
                $q = $this->waterm->gtok($mail);

                if($q != FALSE){
                  //Getting data require send to android
                  foreach($q as $q){
                    $tkn = $q['data'];
                    $dat = $q['date'];
                  }
                  $pss = $this->waterm->getail('user', array('surel' => $mail))->sandi;
                  $cek = $this->waterm->login(array('surel' => $mail), array('sandi' => $pss));
                  foreach($cek as $cek){
                    $pho = base_url()."asset/uspic/".$cek->photo;
                    $ndp = $cek->namadepan;
                    $nbl = $cek->namabelakang;
                    $tgl = $cek->lahir;
                    $ngr = $cek->negara;
                    $pro = $cek->prov;
                    $kot = $cek->kota;
                    $jln = $cek->jalan;
                    $nbl = $cek->namabelakang;
                    $pnt = $cek->poin;
                    $pro = $cek->progress;
                    $dis = $cek->disable;
                  }

                  if($dis == 0){
                    //JSON data to android. Login is Successfull
                    $stat = array('msg' => "Welcome $ndp!",
                      'user' => array('token' => $tkn,'email' => $mail, 'photo' => $pho, 'namadpn' => $ndp, 'namablk' => $nbl,
                        'tgl' => $tgl, 'negara' => $ngr, 'prov' => $pro, 'kota' => $kot, 'jln' => $jln,
                        'poin' => $pnt, 'progress' => $pro, 'date' => $dat),
                      'status' => '200');
                  }else{
                    //Error JSON fail Login
                    $stat = array('status' => '400', 'msg' => "Account Has been disabled! Please contacts Support Center.");
                  }
                }else{
                  //Error JSON fail Login
                  $stat = array('status' => '400', 'msg' => "Can't create session with $mail!");
                }
              }else{
                //Error JSON fail Login
                $stat = array('status' => '400', 'msg' => "Failed Login As $mail");
              }
            }else{
              //Get data From POST method
              $nam = $this->input->post("nama", TRUE);

              $array = $this->waterm->splitname(ucwords(strtolower($nam), '\',. '));
              $ndp = $array[0];
              $nbl = $array[1];
              $tgl = $this->input->post("tgl", TRUE);

              $ndp = ucwords(strtolower($ndp), '\',. ');
              $nbl = ucwords(strtolower($nbl), '\',. ');
              $pas = md5(uniqid(rand(), true));

              $pss = substr($pas, 0, 6);

              //Save to database
              $cek1 = $this->waterm->reg(array('surel' => $mail, 'namadepan' => $ndp,
                  'namabelakang' => $nbl, 'sandi' => $pss, 'lahir' => $tgl, 'negara' => '-',
                  'prov' => '-', 'kota' => '-', 'jalan' => '-', 'poin' => 0, 'disable' => 0, 'progress' => 0, 'level' => '2'));

              $cek = $this->waterm->login(array('surel' => $mail), array('sandi' => $pss));
              if($cek1 != FALSE){
                //Creating Token Access
                $q = $this->waterm->gtok($mail);

                if($q != FALSE){
                  //Getting data require send to android
                  foreach($q as $q){
                    $tkn = $q['data'];
                    $dat = $q['date'];
                  }
                  foreach($cek as $cek){
                    $pho = base_url()."asset/uspic/".$cek->photo;
                    $ndp = $cek->namadepan;
                    $nbl = $cek->namabelakang;
                    $tgl = $cek->lahir;
                    $ngr = $cek->negara;
                    $pro = $cek->prov;
                    $kot = $cek->kota;
                    $jln = $cek->jalan;
                    $nbl = $cek->namabelakang;
                    $pnt = $cek->poin;
                    $pro = $cek->progress;
                    $dis = $cek->disable;
                  }

                  if($dis == 0){
                    //JSON data to android. Login is Successfull
                    $stat = array('msg' => "Welcome $ndp! You Can login account with password \"$pss\"",
                      'user' => array('token' => $tkn,'email' => $mail, 'photo' => $pho, 'namadpn' => $ndp, 'namablk' => $nbl,
                        'tgl' => $tgl, 'negara' => $ngr, 'prov' => $pro, 'kota' => $kot, 'jln' => $jln,
                        'poin' => $pnt, 'progress' => $pro, 'date' => $dat),
                      'status' => '200');
                  }else{
                    //Error JSON fail Login
                    $stat = array('status' => '400', 'msg' => "Account Has been disabled! Please contacts Support Center.");
                  }
                }else{
                  //Error JSON fail Login
                  $stat = array('status' => '400', 'msg' => "Can't create session with $mail!");
                }

              }else{
                //For Register only
                $stat = array('status' => '400', 'msg' => 'Register Failed!');
              }
            }
          }else{
            //Error JSON validation is fail
            $stat = array('status' => '400','msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'updatepro': //Update Profil
          //Validasi form yang diketikkan user
          $this->form_validation->set_rules('nama', 'Full Name', 'required');
          $this->form_validation->set_rules('token', 'data login', 'required');
          $this->form_validation->set_rules('tgl', 'Birth Of Date', 'required');
          $this->form_validation->set_rules('ngr', 'Country', 'required');
          $this->form_validation->set_rules('prov', 'State', 'required');
          $this->form_validation->set_rules('kot', 'City', 'required');
          $this->form_validation->set_rules('jln', 'Street', 'required');

          if ($this->form_validation->run() == TRUE){
            //ambil token
            $tkn = $this->input->post("token",TRUE);
            //ngecek password dan email itu benar
            $cek = $this->waterm->ctok(array('token' => $tkn));

            //Jika autentifikasi benar kesini
            if($cek != FALSE && $this->agent->agent_string() == $cek->useragent){
              $mail = $cek->user_mail;
              $pass = $this->waterm->getail('user',array('surel' => $cek->user_mail))->sandi;
              //Ambil data yang dikirimkan!
              $nam = $this->input->post("nama",TRUE);
              $tgl = $this->input->post("tgl",TRUE);
              $ngr = $this->input->post("ngr",TRUE);
              $pro = $this->input->post("prov",TRUE);
              $kot = $this->input->post("kot",TRUE);
              $jln = $this->input->post("jln",TRUE);

              //Split variable into array
              $array = $this->waterm->splitname(ucwords(strtolower($nam), '\',. '));
              $ndp = $array[0];
              $nbl = $array[1];
              
              //Upload
              $tmp = tempnam('./tmp', 'imgprotmp_'); // might not work on some systems, specify your temp path if system temp dir is not writeable
              file_put_contents($tmp, base64_decode(str_replace(' ', '+',$this->input->post("img",TRUE))));
              $imginf = getimagesize($tmp);
              chmod($tmp, 0777);
              $_FILES['userfile'] = array(
                'name' => uniqid().'.'.preg_replace('!\w+/!', '', $imginf['mime']),
                'tmp_name' => $tmp,
                'size'  => filesize($tmp),
                'error' => UPLOAD_ERR_OK,
                'type'  => $imginf['mime'],
              );
              chmod($tmp, 0777);
              //Configuration to Updoad Photos
              $config['upload_path'] = './asset/uspic/'; //On "userpic" upload
              $config['allowed_types'] = 'jfif|jpg|jpeg'; //Jpg and Png Only
              $config['max_size'] = '2048'; //2MB
              //$config['max_width']  = '2048'; //max width images
              //$config['max_height']  = '2048'; //max height images
              $config['encrypt_name'] = TRUE;
              //$config['file_name'] = $prna; //set new Name
              $this->load->library('upload', $config); //load library upload
              //Set Null and True First
              $pho = null;
              $del = TRUE;

              //Get Name file
              $get = $this->waterm->login(array('surel' => $mail), array('sandi' => $pass));
              //Getting data
              foreach($get as $get){
                  $pho = $get->photo;
              }
              if ($pho === null || $pho == 'default.png'){
                $del = FALSE;
              }

              //Photos is detected OK
              if ($this->upload->do_upload('userfile', true)){
                //Deleting Image Profil File
                if ($del == TRUE){
                  //Delete Photos before
                  chmod('./asset/uspic/'.$pho, 0777);
                  unlink("./asset/uspic/$pho");
                }

                //Get Filename Image
                $pho = $this->upload->data('file_name');
                //Update Data With Email as that ID Include Image
                $cek1 = $this->waterm->uptabusr(array('surel' => $mail), array('namadepan' => $ndp,
                        'namabelakang' => $nbl, 'lahir' => $tgl, 'negara' => $ngr,
                        'prov' => $pro, 'kota' => $kot, 'jalan' => $jln, 'photo' => $pho));

              }else{
                //Update data with email as id Exclude Image
                $cek1 = $this->waterm->uptabusr(array('surel' => $mail), array('namadepan' => $ndp,
                        'namabelakang' => $nbl, 'lahir' => $tgl, 'negara' => $ngr,
                        'prov' => $pro, 'kota' => $kot, 'jalan' => $jln));
              }

              //delete file temporiary
              unlink($tmp);

              if ($cek1 =! FALSE){
              $pho = base_url()."asset/uspic/".$pho;
              //OK JSON Update Success
              $stat = array('msg' => "Update Profil Successfull!",
                'user' => array('email' => $mail, 'photo' => $pho, 'namadpn' => $ndp, 'namablk' => $nbl,
                'profil' => array('tgl' => $tgl, 'negara' => $ngr, 'prov' => $pro, 'kota' => $kot, 'jln' => $jln)),
                'status' => '200');
              }else{
              //Error JSON fail updating profil
              $stat = array('status' => '400', 'msg' => "Can't Update Profil!");
              }
            }else{
              //Error JSON fail Wrong Password
              $stat = array('status' => '400', 'msg' => "Auth Failed!");
            }
          }else {
            //Fail Validation Because illegal input
            $stat = array('status' => '400', 'msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'editmail'://Edit Email
          //Validation Email(New+Confirm) and password for change email
          $this->form_validation->set_rules('mailb', 'mailb', 'required|valid_email|trim|min_length[6]|max_length[64]');
          $this->form_validation->set_rules('mail', 'mail', 'required|valid_email|trim|min_length[6]|max_length[64]');
          $this->form_validation->set_rules('mail1', 'mail1', 'required|valid_email|trim|min_length[6]|max_length[64]');
          $this->form_validation->set_rules('pass', 'pass', 'required|min_length[4]|max_length[64]');

          if ($this->form_validation->run() == TRUE){
            //ambil passwor dan email
            $mailb = $this->input->post("mailb",TRUE);
            $mail = $this->input->post("mail",TRUE);
            $mail1 = $this->input->post("mail1",TRUE);
            $pass = $this->input->post("pass",TRUE);

            //Check Email avaiable?
            $cek = $this->waterm->chkreg("user",array('surel' => $mail));
            if ($cek == FALSE) {
              $stat = array('status' => '400', 'msg' => 'Email Registered with other account!');
            }else{
              //OK Email Not registered with other account
              if ($mail == $mail1){
                //ngecek password dan email itu benar
                $cek1 = $this->waterm->login(array('surel' => $mailb), array('sandi' => $pass));

                //Jika autentifikasi benar kesini
                if ($cek1 != FALSE){
                  //Updating data user
                  $cek2 = $this->waterm->uptabusr(array('surel' => $mailb),array('surel' => $mail));

                  if ($cek2 != FALSE) {
                    $stat = array('msg' => "Update Mail to: $mail Successfull!",
                      'user' => array('email' => $mail), 'status' => '200');
                  }else {
                    //Error JSON fail updating email
                    $stat = array('status' => '400', 'msg' => "Something went wrong!");
                  }
                }else{
                  //Error JSON fail Wrong Password
                  $stat = array('status' => '400', 'msg' => "Failed Update! Bad Password!");
                }
              }else{
                //Error Email Different(With New and verification)
                $stat = array('status' => '400', 'msg' => 'Email is different!');
              }
            }
          }else{
            //Fail Validation Because illegal input
            $stat = array('status' => '400','msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'edpass'://Edit Password
          //Validation Email and password(Old+New+Confirm) for change Password
          $this->form_validation->set_rules('token', 'data login', 'required');
          $this->form_validation->set_rules('pass', 'pass', 'required|min_length[4]|max_length[64]');
          $this->form_validation->set_rules('psn', 'pass new', 'required|min_length[4]|max_length[64]');
          $this->form_validation->set_rules('psn1', 'pass com', 'required|min_length[4]|max_length[64]');

          if ($this->form_validation->run() == TRUE){
            //get input from android with POST method
            $pass = $this->input->post("pass", TRUE);
            $passn = $this->input->post("psn", TRUE);
            $passn1 = $this->input->post("psn1", TRUE);
            //checking is user legal to login?
            $tkn = $this->input->post("token", TRUE);
            $cek = $this->waterm->ctok(array('token' => $tkn));

            if($cek != FALSE && $this->agent->agent_string() == $cek->useragent){
              //Jika autentifikasi benar kesini
              $mail = $cek->user_mail;
              $pass = $this->waterm->getail('user', array('surel' => $cek->user_mail))->sandi;
              $cek1 = $this->waterm->login(array('surel' => $mail), array('sandi' => $pass));

              //Checking Is User Allowable to Change Password
              if ($cek1 != FALSE) {
                foreach($cek1 as $cek){
                  $namadpn = $cek->namadepan;
                }
                if ($passn != $passn1){
                  //Error New Password and Confirmation Password Different
                  $stat = array('status' => '400', 'msg' => 'New Password and Confirm Password is different!');
                }else{
                  $cek2 = $this->waterm->uptabusr(array('surel' => $mail),array('sandi' => $passn));
                  if ($cek2 != FALSE) {
                    $stat = array('msg' => "$namadpn, Update Password Successfull!", 'status' => '200');
                  }else {
                    //Error JSON fail updating Password
                    $stat = array('status' => '400', 'msg' => "Server Maintenance!");
                  }
                }
              }else{
                //Error JSON fail Wrong Password
                $stat = array('status' => '400', 'msg' => "Current Password Wrong!");
              }
            }else{
              //ERROR UN AUTH
              $stat = array('status' => '400', 'msg' => "No Auth! Please Login First");
            }
          }else {
            //Fail Validation Because illegal input
            $stat = array('status' => '400', 'msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'poin'://Transaksi Poin dan cek poin
          //Apa yang dibutuhkan
          $this->form_validation->set_rules('todo', 'Ngapain', 'required');
          $this->form_validation->set_rules('pts', 'Poin', 'required');
          $this->form_validation->set_rules('token', 'data login', 'required');

          if ($this->form_validation->run() == TRUE){
            $todo = $this->input->post("todo",TRUE);
            $pts = $this->input->post("pts",TRUE);
            //checking is user legal to login?
            $tkn = $this->input->post("token",TRUE);
            $cek = $this->waterm->ctok(array('token' => $tkn));

            if ($cek != FALSE && $this->agent->agent_string() == $cek->useragent){
              //If User ok
              $mail = $cek->user_mail;

              //What-todo
              if ($todo == 1){
                $stat = array('status' => '400', 'msg' => 'No Avaiable!');
                //Blocked
                /*
                //OK, Point No to be add
                $cek1 = $this->waterm->pntud("+$pts", array('surel' => $mail));
                if($cek1 != FALSE){
                  $stat = array('status' => '200','msg' => 'OK');
                }else{
                  $stat = array('status' => '400','msg' => 'Error! Cant Addition Your point!');
                }
                */
              }elseif ($todo == 2){
                //code for min
                /*
                if ($pts - $cek->poin >= 0){
                  //Fail Action Because point min
                  $stat = array('status' => '400', 'msg' => 'Sorry Point is limit!');
                }else{
                  //OK, Point No to be Minus
                  $cek1 = $this->waterm->pntud("-$pts", array('surel' => $mail));
                  if ($cek1 != FALSE){
                  $stat = array('status' => '200', 'msg' => 'OK');                    
                  }else{
                    $stat = array('status' => '400', 'msg' => 'Error! Cant Subtraction Your point!');
                  }
                }
                */
              }else{
                //Fail Action Because is unknown
                $hit = $this->waterm->getail("user", array('surel' => $mail))->poin;
                $stat = array('status' => '200', 'data' => array('poin' => $hit), 'msg' => 'OK');
              }
            }else{
              //Fail Account unknown
              $stat = array('status' => '400', 'msg' => 'No Logged in!');
            }
          }else{
            //Fail Validation Because illegal input
            $stat = array('status' => '400', 'msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'refill'://Transaction Bottle refill On Mobile app
          //what is nedeed
          $this->form_validation->set_rules('token', 'data login', 'required');
          $this->form_validation->set_rules('id', 'NO ID DATA', 'required');

          if($this->form_validation->run() == TRUE){
            //sucess validation
            $idp = $this->input->post("id", TRUE);
            //checking is user legal to login?
            $tkn = $this->input->post("token", TRUE);

            //Checking token
            $cek = $this->waterm->ctok(array('token' => $tkn));

            if($cek != FALSE && $this->agent->agent_string() == $cek->useragent){
              //If User OK
              $mail = $cek->user_mail;

              $cek1 = $this->waterm->chkreg('tempatisi', array('id' => $idp));
                if($cek1 == FALSE){
                    $ida = array('id' => $idp);
                    $pnt = $this->waterm->getail('tempatisi', $ida)->poin;
                    $nam = $this->waterm->getail('tempatisi', $ida)->nama;

                    $cek2 = $this->waterm->addpnt($mail, $pnt, $idp);
                    $this->waterm->addcom($mail, 1, $idp);

                    if($cek2 != FALSE){
                      $com = "FALSE";
                      //TRUE
                      if($this->waterm->chkreg('review_tempat', array('surel' => $mail, 'id_tempat' => $idp))) $com = "TRUE";

                      //Fail
                      $stat = array('status' => '200', 'msg' => "Refill on $nam Success!", 'pts' => $pnt, "review" => $com);
                    }else{
                      //Fail
                      $stat = array('status' => '400', 'msg' => "Unknown Reason, Refill on $nam is Failed!");
                    }

                }else{
                  //Fail Place is unknown
                  $stat = array('status' => '400', 'msg' => 'Unidenfied Place!');
                }
              
            }else{
              //Fail Account unknown
              $stat = array('status' => '400', 'msg' => 'No Logged in!');
            }
          }else{
            //Fail Validation Because illegal input
            $stat = array('status' => '400', 'msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'newplace'://Create New Place map
          //Values requipment here
          $this->form_validation->set_rules('nama', 'Name Place', 'required');
          $this->form_validation->set_rules('alamat', 'Address', 'required');
          $this->form_validation->set_rules('relasi', 'Relation', 'required');
          $this->form_validation->set_rules('tipe', 'Type', 'required');
          $this->form_validation->set_rules('lon', 'Longitude', 'required');
          $this->form_validation->set_rules('lat', 'Latitude', 'required');
          $this->form_validation->set_rules('token', 'Data Login', 'required');
          $this->form_validation->set_rules('img[]', 'Image', 'required');

          if ($this->form_validation->run() == TRUE){
            //Validation Success
            $nam = ucwords($this->input->post("nama",TRUE));
            $add = $this->input->post("alamat",TRUE);
            $rel = $this->input->post("relasi",TRUE);
            $typ = $this->input->post("tipe",TRUE);
            $lon = $this->input->post("lon",TRUE);
            $lat = $this->input->post("lat",TRUE);
            $tkn = $this->input->post("token",TRUE);
            $img = $this->input->post("img",TRUE);

            //Validation user Is OK
            $cek = $this->waterm->ctok(array('token' => $tkn));

            //Check user is allowed?
            if($cek != FALSE && $this->agent->agent_string() == $cek->useragent){
              //Get mail user form token
              $mail = $cek->user_mail;
              //check this place maked before?
              $cek1 = $this->waterm->chsm('tempat_baru', array('longitude' => $lon, 'latitude' => $lat), array('nama' => $nam));
              $cek2 = $this->waterm->chsm('tempatisi', array('longitude' => $lon, 'latitude' => $lat), array('nama' => $nam));
              
              if($cek1 == FALSE && $cek2 == FALSE){
                //cek the tag of place
                $cek3 = $this->waterm->chkreg('interest_list', array('nama' => $typ));
                
                if($cek3 == FALSE){
                  //add place to database
                  $cek4 = $this->waterm->addsm('tempat_baru', array('nama' => $nam, 'relasi' => $rel, 'tipe' => $typ, 'longitude' => $lon, 'latitude' => $lat, 'alamat' => $add, 'pendaftar' => $mail));
                  //Get Id
                  $id = $this->waterm->getail('tempat_baru', array('nama' => $nam, 'relasi' => $rel, 'tipe' => $typ, 'longitude' => $lon, 'latitude' => $lat, 'pendaftar' => $mail))->id_tempat;
                  //upload image and save name to 'pic_tmpt_baru'
                  $x = 0;
                  $img = $this->input->post("img",TRUE);
                  while(isset($img[$x])){
                    //This For Upload Image
                    $tmp = tempnam('./tmp', 'imgmaptmp_'); // might not work on some systems, specify your temp path if system temp dir is not writeable
                    file_put_contents($tmp, base64_decode(str_replace(' ', '+',$img[$x])));
                    $imginf = getimagesize($tmp);
                    $_FILES['userfile'] = array(
                        'name' => uniqid().'.'.preg_replace('!\w+/!', '', $imginf['mime']),
                        'tmp_name' => $tmp,
                        'size'  => filesize($tmp),
                        'error' => UPLOAD_ERR_OK,
                        'type'  => $imginf['mime'],
                    );
                    chmod($tmp, 0777);
                    //Configuration Updoad Photos
                    //$prna = time().$mail.$_FILES["profil"]['name']; //Change name of image
                    $config['upload_path'] = './asset/nppic/'; //On "userpic" upload
                    $config['allowed_types'] = 'jpe|jpg|jpeg'; //Jpg/Jpeg Only
                    $config['max_size'] = '2048'; //2MB
                    $config['encrypt_name'] = TRUE;
                    //$config['file_name'] = $prna; //set new Name
                    $this->load->library('upload', $config); //load library upload
                    $fnm = null;

                    //Photos is detected OK
                    if ($this->upload->do_upload('userfile', true)){
                      //Get Filename
                      $fnm = $this->upload->data('file_name');
                      chmod('./asset/nppic/'.$fnm, 0777);

                      //Update user table on "Photo" column where Mail as ID
                      $this->waterm->addsm('pic_tmpt_baru', array('id_tempat' => $id, 'image' => $fnm));
                    }
                    $x++;

                    //Remove file temporiary
                    unlink($tmp);
                  }
                  if($cek4 != FALSE){
                    //Ok
                    $stat = array('status' => '200','msg' => "$nam Success added! This can take few days to show point map.");
                  }else{
                    //Error Adding
                    $stat = array('status' => '400','msg' => 'Internal Server Error!');
                  }
                }else{
                  $stat = array('status' => '400','msg' => 'Unknown Type refill station!');
                }
              }else{
                $stat = array('status' => '400','msg' => 'Place already exists!');
              }
            }else{
              //Fail Because Not logged in
              $stat = array('status' => '400','msg' => 'Not Allowed!');
            }
          }else{
            //Fail Validation Because illegal input
            $stat = array('status' => '400','msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'refresh'://refreshing data about user profile
          //Validation add review with account auth.
          $this->form_validation->set_rules('token', 'Data Login', 'required');
          
          //Detecting Are Value is Valid
          if ($this->form_validation->run() == TRUE){
            //Check User
            $tkn = $this->input->post("token", TRUE);

            $cek1 = $this->waterm->ctok(array('token' => $tkn));

            if($cek1 != FALSE  && $this->agent->agent_string() == $cek1->useragent){
              //This account review before?
              $mail = $cek1->user_mail;
              $detal = $this->waterm->getail("user", array('surel' => $mail));
              $pho = base_url()."asset/uspic/".$this->waterm->getail("user", array('surel' => $mail))->photo;
              $ndp = $this->waterm->getail("user", array('surel' => $mail))->namadepan;
              $nbl = $this->waterm->getail("user", array('surel' => $mail))->namabelakang;
              $tgl = $this->waterm->getail("user", array('surel' => $mail))->lahir;
              $ngr = $this->waterm->getail("user", array('surel' => $mail))->negara;
              $prv = $this->waterm->getail("user", array('surel' => $mail))->prov;
              $kot = $this->waterm->getail("user", array('surel' => $mail))->kota;
              $jln = $this->waterm->getail("user", array('surel' => $mail))->jalan;
              $nbl = $this->waterm->getail("user", array('surel' => $mail))->namabelakang;
              $pnt = $this->waterm->getail("user", array('surel' => $mail))->poin;
              $pro = $this->waterm->getail("user", array('surel' => $mail))->progress;
              
              //JSON
              $stat = array('msg' => "OK",
                  'user' => array('email' => $mail, 'photo' => $pho, 'namadpn' => $ndp, 'namablk' => $nbl,
                    'tgl' => $tgl, 'negara' => $ngr, 'prov' => $prv, 'kota' => $kot, 'jln' => $jln,
                    'poin' => $pnt, 'progress' => $pro),
                    'status' => '200');
            }else{
              //Error JSON fail Login
              $stat = array('status' => '400', 'msg' => "Not Logged in!");
            }
          }else{
            //Require Data incomplete
            $stat = array('status' => '400','msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'review': // Add Review
          //Validation add review with account auth.
          $this->form_validation->set_rules('id', 'id map', 'required');
          $this->form_validation->set_rules('token', 'Data Login', 'required');
          $this->form_validation->set_rules('star', 'Ngapain', 'required');
          $this->form_validation->set_rules('komentar', 'Komentar', 'required');

          //Detecting Are Value is Valid
          if ($this->form_validation->run() == TRUE){
            $tkn = $this->input->post("token", TRUE);
            $idm = $this->input->post("id", TRUE);
            $bin = $this->input->post("star", TRUE);
            $kom = $this->input->post("komentar", TRUE);

            //check maps is true?
            $cek = $this->waterm->chkreg("tempatisi", array("id" => $idm));

            if($cek == FALSE){
              //Check User
              $cek1 = $this->waterm->ctok(array('token' => $tkn));

              if($cek1 != FALSE  && $this->agent->agent_string() == $cek1->useragent){
                //This account review before?
                $mail = $cek1->user_mail;
                $cek2 = $this->waterm->chsm("review_tempat", array('id_tempat' => $idm), array('surel' => $mail));
                
                if($cek2 == FALSE){
                  //No, Its New Review
                  $cek3 = $this->waterm->addsm("review_tempat", array('surel' => $mail, 'id_tempat' => $idm , 'bintang' => $bin, 'komentar' => $kom));
                  
                  if($cek3 != FALSE){
                    //Review add successfully
                    $stat = array('status' => '200','msg' => 'Review Added!');
                  }else{
                    //adding review error
                    $stat = array('status' => '400','msg' => 'Falied to add Review!');
                  }
                }else{
                  //yes, Before review
                  $cek3 = $this->waterm->uprev("review_tempat", array('id_tempat' => $idm), array('surel' => $mail), array('bintang' => $bin, 'komentar' => $kom));
                  
                  if($cek3 != FALSE){
                    //Ok review renew
                    $stat = array('status' => '200','msg' => 'Review Renewed!');
                  }else{
                    //renew error
                    $stat = array('status' => '400','msg' => 'Falied to Renew Review!');
                  }
                }
              }else{
                //Not Login User
                $stat = array('status' => '400','msg' => 'Not Logged in!');
              }
            }else{
              //No map Point found
              $stat = array('status' => '400','msg' => 'Map Point not Found!');
            }
          }else{
            //Require Data incomplete
            $stat = array('status' => '400','msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'showevent'://Showing Event
          $stat = array('status' => '200', 'data' => $this->waterm->fetchev("event"), 'msg' => 'OK' );
        break;

        case 'shotd'://Show on today event
          $this->form_validation->set_rules('tgl', 'DATE', 'required');
          $this->form_validation->set_rules('token', 'ID Login', 'required');

          //Detecting Are Value is Valid?
          if ($this->form_validation->run() == TRUE){
            //get a post
            $tkn = $this->input->post("token",TRUE);
            $tgl = $this->input->post("tgl",TRUE);
            //Cek benenaran User
            $cek1 = $this->waterm->ctok(array('token' => $tkn));
            if ($cek1 != FALSE && $this->agent->agent_string() == $cek1->useragent){
              //If Ok account is true is here
              $data = $this->waterm->ddtail($tgl);
              if($data != FALSE){
                $stat = array('status' => '200', 'data' => $data, 'msg' => 'OK' );
              }else{
                $stat = array('status' => '400', 'data' => array(), 'msg' => 'NO DATA' );
              }
            }else{
              //Fail Account unknown

              $stat = array('status' => '400','msg' => 'No Logged in!');
            }
          }else{
            //Fail Validation Because illegal input
            $stat = array('status' => '400','msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'etail'://Detail Event
          $this->form_validation->set_rules('id', 'id event', 'required');
          $this->form_validation->set_rules('token', 'ID Login', 'required');

          //Detecting Are Value is Valid?
          if ($this->form_validation->run() == TRUE){
            //get a post
            $tkn = $this->input->post("token",TRUE);
            $id = $this->input->post("id",TRUE);

            $cek = $this->waterm->chkev(array('id' => $id));
            if ($cek != FALSE){
              //Cek benenaran User
              $cek1 = $this->waterm->ctok(array('token' => $tkn));
              if ($cek1 != FALSE && $this->agent->agent_string() == $cek1->useragent){
                //If Ok account is true is here
              
                $stat = array('status' => '200', 'data' => $this->waterm->etail('id', $id), 'msg' => 'OK' );
              }else{
                //Fail Account unknown
                $stat = array('status' => '400','msg' => 'No Logged in!');
              }
            }else{
              //Fail Validation Because no event
              $stat = array('status' => '400','msg' => 'No event!');
            }
          }else{
            //Fail Validation Because illegal input
            $stat = array('status' => '400','msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'event-act': // Sahring Event Add Counting
          //Validation add share count. Check Account and id validation
          $this->form_validation->set_rules('id', 'Id Share', 'required');
          $this->form_validation->set_rules('todo', 'Todo', 'required');
          $this->form_validation->set_rules('token', 'ID Login', 'required');

          //Detecting Are Value is Valid?
          if ($this->form_validation->run() == TRUE){
            //get a post
            $tkn = $this->input->post("token",TRUE);
            $id = $this->input->post("id",TRUE);
            $to = $this->input->post("todo",TRUE);

            //Cek kebenaran event
            $cek = $this->waterm->chkev(array('id' => $id));
            if ($cek != FALSE){
              //Cek benenaran User
              $cek1 = $this->waterm->ctok(array('token' => $tkn));

              if ($cek1 != FALSE && $this->agent->agent_string() == $cek1->useragent){
                $mail = $cek1->user_mail;
                //If Ok account is true is here
                if ($to == 1) {
                  //check if already like?
                  $cek2 = $this->waterm->chsm("suka_event", array('id_event' => $id), array('surel' => $mail));

                  if($cek2 == FALSE){
                    //if no already exexute to like
                    $cek3 = $this->waterm->addsm("suka_event", array('id_event' => $id,'surel' => $mail));
                    if($cek3 != FALSE){
                      //Ok
                      $stat = array('status' => '200','msg' => 'Like');
                    }else{
                      //Error
                      $stat = array('status' => '400','msg' => 'INTERNAL SERVER ERROR!');
                    }
                  }else{
                    //if already exexute to unlike
                    $cek3 = $this->waterm->delsm("suka_event", array('id_event' => $id,'surel' => $mail));
                    if($cek3 != FALSE){
                      //Ok
                      $stat = array('status' => '200','msg' => 'Unlike');
                    }else{
                      //Error
                      $stat = array('status' => '400','msg' => 'INTERNAL SERVER ERROR!');
                    }
                  }
                }elseif ($to == 2) {
                  $cek2 = $this->waterm->chsm("share_event", array('id_event' => $id), array('surel' => $mail));
                  if($cek2 == FALSE){
                    //Share is goes here
                    $this->waterm->addsm("share_event", array('id_event' => $id,'surel' => $mail));
                    $stat = array('status' => '200','msg' => 'You Share This..');
                  }else{
                    $stat = array('status' => '200','msg' => 'You Re-Share This..');
                  }
                }else{
                  //Fail Action Because is unknown
                  $stat = array('status' => '400','msg' => 'Unknown Action!');
                }
              }else{
                //Fail Account unknown
                $stat = array('status' => '400','msg' => 'No Logged in!');
              }
            }else{
              //Fail Validation Because no event
              $stat = array('status' => '400','msg' => 'No event!');
            }
          }else{
            //Fail Validation Because illegal input
            $stat = array('status' => '400','msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'interest'://Add or Update Interest
          //Validation check
          $this->form_validation->set_rules('token', 'Data Login', 'required');

          if($this->form_validation->run() == TRUE){
            //Get from Post
            $tkn = $this->input->post("token",TRUE);
            $lst = $this->input->post("list",TRUE);
            $chk = $this->input->post("chk",TRUE);

            //Check Login
            $cek = $this->waterm->ctok(array('token' => $tkn));

            if($cek != FALSE && $this->agent->agent_string() == $cek->useragent){
              //Get mail address
              $mail = $cek->user_mail;

              if(isset($chk)){
                //just cek
                $get = $this->waterm->infetch('kesukaan_pengguna', array('surel' => $mail));
                $hit = array();
                foreach($get as $get){
                  $hit[] = $get->interest;
                }
                $it = array();
                $all = $this->waterm->infetch('interest_list');
                foreach($all as $all){
                  $it[] = $all->nama;
                }
                $stat = array('status' => '200', 'data' => array('all' => $it, 'user' => $hit) , 'msg' => 'OK' );
              }else{
                //User OK
                $this->waterm->delinter(array('surel' => $mail));
                $x = 0;
                while(isset($lst[$x])){
                  //code
                  $hit = $lst[$x];
                  $cek1 = $this->waterm->infetch('kesukaan_pengguna', array('surel' => $mail), array('interest' => $hit));
                  if($cek1 == FALSE){
                    $this->waterm->addinter(array('surel' => $mail, 'interest' => $hit));
                  }
                  $x++;
                }
                //Whatever Ok or no Is Saved
                $stat = array('status' => '400', 'msg' => "Done!");
              }
            }else{
              //User Unknown
              $stat = array('status' => '400', 'msg' => "You're Not Logged in!");
            }
          }else{
            //Requipment is min
            $stat = array('status' => '400','msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'geo'://get geo Country
          //code get
          $sta = $this->input->post("state",TRUE);
          $cty = $this->input->post("city",TRUE);

          if(isset($cty)){
            //search the city with filter of district
            $get = $this->waterm->scfetch(array('District' => $cty), 'Name');
            $hit = array();
            $x = null;
            //Check Is Avaiable?
            if($get != FALSE){
              //get data
              foreach($get as $get){
                $hit[] = array("city" => $get->Name);
              }
              //Sent data
              $stat = array('status' => '200', 'data' => $hit, 'msg' => 'OK' );
            }else{
              //No data will send error
              $data = array();
              $data[] = array('city' => '-');
              $stat = array('status' => '200', 'data' => $data, 'msg' => 'NO DATA' );
            }
          }elseif(isset($sta)){
            //search the city with filter of country
            $a = $this->waterm->getail('country', array('Name' => $sta));
            $get = $this->waterm->scfetch(array('CountryCode' => $a->Code), 'District');
            
            //Define Variable
            $hit = array();
            $x = null;

            //Check Is Avaiable?
            if($get != FALSE){
              //get data
              foreach($get as $get){
                //Selecting no same data sent
                if($x != $get->District){
                  $hit[] = array("state" => $get->District);
                  $x = $get->District;
                }
              }
              //Sent data
              $stat = array('status' => '200', 'data' => $hit, 'msg' => 'OK' );
            }else{
              //No data error
              $data = array();
              $data[] = array('state' => '-');
              $stat = array('status' => '400', 'data' => $data, 'msg' => 'NO DATA' );
            }
          }else{
            //Sent Country data
            $stat = array('status' => '200', 'data' => $this->waterm->cofetch(), 'msg' => 'OK' );
          }
        break;

        case 'topshop': //Penjualan Terpopuler
          //Get transaction list
          $get = $this->waterm->shtop();
          if($get != FALSE){
            //If Get have no FALSE result
            //Sorting array
            krsort($get);
            #foreach($get as $x => $x_value) {
            #  echo "Key=" . $x . ", Value=" . $x_value;
            #  echo "<br>";
            #}
            //fetching data form shop
            $stat = array('status' => '200', 'data' => $this->waterm->fetchshop(TRUE, $get), 'msg' => 'OK' );
          }else{
            //No data error
            $stat = array('status' => '400', 'msg' => 'No Data Avaiable');
          }
        break;

        case 'shop': //Get Shop who promo doesnt expired
          $stat = array('status' => '200', 'data' => $this->waterm->fetchshop(), 'msg' => 'OK' );
        break;

        case 'desho': //Get Shop details
          //value is allowable?
          $this->form_validation->set_rules('id', 'id shop', 'required');
          $this->form_validation->set_rules('token', 'Data Login', 'required');

          //Check validation
          if ($this->form_validation->run() == TRUE){

            $tkn = $this->input->post("token", TRUE);//Get Token
            $id = $this->input->post("id", TRUE);

            $cek = $this->waterm->desho($id, $tkn);

            if($cek != FALSE){
              $stat = array('status' => '200', 'data' => $cek, 'msg' => 'OK' );
            }else{
              $stat = array('status' => '400', 'msg' => 'Not Found' );
            }
          }else{
            //Wrong parameter if not identify
            $stat = array('status' => '400', 'msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'map': //Get Map
          //value is allowable?
          $this->form_validation->set_rules('token', 'Data Login', 'required');
          $this->form_validation->set_rules('lon', 'Longitude Point', 'required');
          $this->form_validation->set_rules('lat', 'Latitude Point', 'required');

          //Check validation
          if ($this->form_validation->run() == TRUE){
            //get a POST method
            
            $tkn = $this->input->post("token",TRUE);//Get Token
            
            $lon = $this->input->post("lon",TRUE);//Original
            $lat = $this->input->post("lat",TRUE);//Original

            //login check
            $cek = $this->waterm->ctok(array('token' => $tkn));
            if($cek != FALSE && $this->agent->agent_string() == $cek->useragent){
              //save Coordinates to session
              $this->waterm->savcoor($tkn, $lat, $lon);

              //get mail
              $mail = $cek->user_mail;

              //Get A Favorite place
              $cek2 = $this->waterm->fetchfaf(array('surel' => $mail));
              if($cek2 != FALSE){
                //function while data received(Favorite)
                $hit = $this->waterm->fmap($cek2);
              }else{
                //no data favorite
                $hit = FALSE;
              }
              //get place. if false all maps get, if hit exists filter maps get
              $hit = $this->waterm->fetchmap("tempatisi", /*FALSE*/$hit, $lon, $lat);

              //filter array
              $cek = array_filter($hit);
              if(!empty($cek)){
                //have nearby place
                $stat = array('status' => '200', 'data' => $hit, 'msg' => 'OK' );
              }else{
                //No Nearby place detected
                $stat = array('status' => '400', 'msg' => 'No Nearby Place' );
              }
            }else{
              //Wrong parameter if not identify
              $stat = array('status' => '400', 'msg' => 'Authentication Failed!');
            }
          }else{
            //Wrong parameter if not identify
            $stat = array('status' => '400', 'msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'mapimg'://Get image maps
          //value allowable?
          $this->form_validation->set_rules('id', 'Place pin', 'required');

          if ($this->form_validation->run() == TRUE){
            //Get Id
            $id = $this->input->post("id",TRUE);

            //selecting map
            //$hit = $this->waterm->imgmap($id);
            $hit = $this->waterm->revimgmap($id);
            if($hit != FALSE){
              $stat = array('status' => '200', "data" => $hit, 'msg' => "OK");
            }else{
              //Wrong parameter if havent image
              $stat = array('status' => '400', 'msg' => "NO IMAGE");
            }
          }else{
            //Wrong parameter if not identify
            $stat = array('status' => '400', 'msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'mapdet'://Get maps details
          //value allowable?
          $this->form_validation->set_rules('id', 'Place pin', 'required');

          if ($this->form_validation->run() == TRUE){
            //Get Id
            $id = $this->input->post("id",TRUE);

            //selecting map
            $hit = $this->waterm->detamap($id);
            if($hit != FALSE){
              $stat = array('status' => '200', "data" => $hit, 'msg' => "OK");
            }else{
              //Wrong parameter if havent image
              $stat = array('status' => '400', 'msg' => "No Maps Info");
            }
          }else{
            //Wrong parameter if not identify
            $stat = array('status' => '400', 'msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'maprev'://Get maps details
          //value allowable?
          $this->form_validation->set_rules('id', 'Place pin', 'required');

          if ($this->form_validation->run() == TRUE){
            //Get Id
            $id = $this->input->post("id",TRUE);

            //selecting map
            $hit = $this->waterm->revmap($id);
            if($hit != FALSE){
              $stat = array('status' => '200', "data" => $hit, 'msg' => "OK");
            }else{
              //Wrong parameter if havent image
              $stat = array('status' => '400', 'msg' => "No Maps Review");
            }
          }else{
            //Wrong parameter if not identify
            $stat = array('status' => '400', 'msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'voucer': //Get Voucer List Avaiable
          $this->form_validation->set_rules('token', 'Data Login', 'required');

          //Check validation
          if ($this->form_validation->run() == TRUE){
            //ok
            $tkn = $this->input->post("token",TRUE);
            $idv = $this->input->post("id",TRUE);

            //Cek Token
            $cek = $this->waterm->ctok(array('token' => $tkn));
            if($cek != FALSE && $this->agent->agent_string() == $cek->useragent){
              if(!isset($idv)){
                $stat = array('status' => '200', 'data' => $this->waterm->fetchdata("voucer"), 'msg' => 'OK' );
              }else{
                $mail = $cek->user_mail;
                $mpn = $this->waterm->getail("voucer", array('kode' => $idv))->poin;
                
                //Cek Is any data
                if(isset($mpn)){
                  $pnt = $this->waterm->getail("user", array('surel' => $mail))->poin;
                  if($pnt - $mpn >= 0){
                    //Point Enough
                    $cek1 = $this->waterm->addvoc($idv, $mail);
                    if($cek1 != FALSE){
                      //OK

                      $this->waterm->pntud("-$pnt", array('surel' => $mail));
                      $stat = array('status' => '400', 'msg' => 'Voucer Reddemed!');
                    }else{
                      //Fail
                      $stat = array('status' => '400', 'msg' => 'Redeem Voucer invalid!');
                    }
                  }else{
                      //Fail
                      $stat = array('status' => '400', 'msg' => 'Point Not Enough!');
                  }
                }else{
                  //Fail
                  $stat = array('status' => '400', 'msg' => 'Voucer Not Valid!');
                }

              }

            }else{
              //Wrong parameter if not identify
              $stat = array('status' => '400', 'msg' => 'Authentication Failed!');
            }
          }else{
            //Wrong parameter if not identify
            $stat = array('status' => '400', 'msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'myvoucerlist': //get my voucer list
          $this->form_validation->set_rules('token', 'Data Login', 'required');

          //Check validation
          if ($this->form_validation->run() == TRUE){
            //get a POST method

            $tkn = $this->input->post("token",TRUE);//Get Token

            //login check
            $cek = $this->waterm->ctok(array('token' => $tkn));
            if($cek != FALSE && $this->agent->agent_string() == $cek->useragent){
              //get mail
              $mail = $cek->user_mail;
              $hit = $this->waterm->myvoucerlist(array('mail_user' => $mail));
              if($hit != FALSE){
                $stat = array('status' => '200', 'data' => $hit, 'msg' => 'OK' );
              }else{
                $stat = array('status' => '400', 'msg' => 'You Havent Voucher right now!' );
              }
            }else{
              //Wrong parameter if not identify
              $stat = array('status' => '400', 'msg' => 'Authentication Failed!');
            }
          }else{
            //Wrong parameter if not identify
            $stat = array('status' => '400', 'msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'relation': //Get Voucer List Avaiable
          $stat = array('status' => '200', 'data' => $this->waterm->infetch("relasi"), 'msg' => 'OK' );
        break;

        case 'types': //Get Voucer List Avaiable
          $stat = array('status' => '200', 'data' => $this->waterm->infetch("interest_list"), 'msg' => 'OK' );
        break;

        case 'saved': //Get saved
          //get is value is allowable
          $this->form_validation->set_rules('token', 'Data Login', 'required');

          //Check validation
          if ($this->form_validation->run() == TRUE){
            //get a POST method
            $tkn = $this->input->post("token",TRUE);

            //login check
            $cek = $this->waterm->ctok(array('token' => $tkn));

            //Cek login is Ok
            if($cek != FALSE && $this->agent->agent_string() == $cek->useragent){
              $mail = $cek->user_mail;
              //Login OK
              $data = array();
              $g = 0;
              for($x=0; $x <= 11; $x++){
                $d = 0;
                $a = strtotime("-$x month");
                $b = date("Y-m", $a);
                $y = date("Y", $a);
                $m = date("m", $a);
                //$day = cal_days_in_month(CAL_GREGORIAN, $m, $y);
                $day = date('t', mktime(0, 0, 0, $m, 1, $y));
                $cout = $this->waterm->saved_mon(array('surel' => $mail), $b);

                if($cout != FALSE){
                  foreach($cout as $out){
                    $d += $out['harian'];
                  }

                }else{
                  $d += 0;
                }
                $data[] = array('bulan' => date("F", $a), 'botol_bln' => (string)round($d), 'max_botol' => (string)$day);
                $f = date("Y-m-d");
                $hit = $this->waterm->saved_day(array('surel' => $mail), array('tgl' => $f));

                if($hit != FALSE){
                  foreach($hit as $hi){
                    $g = $hi['harian'];
                  }

                }else{
                  $g += 0;
                }
                $tot = 0;
                $get = $this->waterm->saved_all(array('surel' => $mail));

                if($get != FALSE){
                  foreach($get as $got){
                    #'divflo' dalam liter, langsung
                    //$tot += $this->waterm->divflo($got['harian'], 1000);
                    $tot += $got['harian'];
                  }
                  
                }else{
                  $tot += 0;
                }
                $stat = array('status' => '200', 'penggunaan' => array_merge(array('monthly' => $data), array('daily' => (string)$g), array('total' => (string)$tot)), 'msg' => 'OK');
              }
            }else{
              //No Auth!
              $stat = array('status' => '400', 'msg' => 'You dont have permission!');
            }
          }else{
            //Requipment is min
            $stat = array('status' => '400','msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        case 'logout':  //Logout User
        //get is value is allowable
        $this->form_validation->set_rules('token', 'Data Login', 'required');

        //Check validation
        if ($this->form_validation->run() == TRUE){
          //get a POST method
          $tkn = stripslashes($this->input->post("token",TRUE));

          //Delete Token
          $cek = $this->waterm->dtok(array('token' => $tkn));
          if($cek != FALSE){
            $stat = array('status' => '200', 'msg' => "Youre Now Logout!" );
          }else{
            $stat = array('status' => '400', 'msg' => "Cant Logout! System Error!" );
          }
        }else{
          //Requipment is min
          $stat = array('status' => '400','msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
        }
        break;

        case 'forgot': //Forgot Password
          //get is value is allowable
          $this->form_validation->set_rules('mail', 'E-Mail', 'required');
          if ($this->form_validation->run() == TRUE){
            //get mail form post
            $mail = $this->input->post("mail",TRUE);

            //Cek Email
            $cek = $this->waterm->chkmail(array('surel' => $mail));
            if($cek != FALSE){
              $this->forgot->pass($mail);
              //$stat = array('status' => '200','msg' => "We send New password to $mail");
            }else{
              //$stat = array('status' => '400','msg' => 'Account Not Found!');
            }
            $stat = array('status' => '400', 'msg' => 'If exists, We send New password to $mail');
          }else{
            //Requipment is min
            $stat = array('status' => '400', 'msg' => preg_replace("/(\n)+/m", ' ', strip_tags(strip_tags(validation_errors()))));
          }
        break;

        default:
          //Wrong parameter if not identify
          $stat = array('status' => '400', 'msg' => 'Wrong Parameters');
        break;
      }
      //encode and print the $stat to json for android
      echo json_encode($stat);
    }
}
#}
//Credits: DzEN/DzEN[DENZVELOPER] (c)2018 For Codelabs [Codeigniter]
