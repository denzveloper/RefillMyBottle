<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct(){
        parent::__construct();
        //Must to Load!
        $this->load->library('session');
        //load model loginm
        $this->load->model('loginm');
    }

    public function index(){
        if($this->loginm->chksess()){
			//jika memang session sudah terdaftar, maka dialihkan ke halaman dahsboard
			redirect("dashboard");
		}else{
			//set form validation
            $this->form_validation->set_rules('mail', 'Email', 'required|valid_email|trim|min_length[6]|max_length[64]');
            $this->form_validation->set_rules('pass', 'Password', 'required|min_length[4]|max_length[64]');
            //set message form validation
            $this->form_validation->set_message('required', 'harus diisi!');
            //check Validation
			if ($this->form_validation->run() == TRUE){
                $mail = $this->input->post("mail", TRUE);
                $pass = $this->input->post("pass", TRUE);
                //check user mail and password
                $cek = $this->loginm->login(array('surel' => $mail), array('sandi' => $pass));
                if ($cek != FALSE){
                    foreach ($cek as $hit){
            	        $sesar = array(
                            'logged_in' => TRUE,
                            'mail' => $hit->surel,
                            'fnam' => $hit->namadepan,
                            'lnam' => $hit->namabelakang,
                            'lv' => $hit->level,
                            'foto' => base_url()."asset/uspic/".$hit->photo
                        );
                        $dis = $hit->surel;
                    }

                    if($dis == 0){
                        $this->load->model('variabel');
                        //set session userdata
                        $this->session->set_userdata($sesar);
                        $namaewa = $this->variabel->nama();
                        $errmsg[] = array('ico' => 'ti-check', 'txt' => "<b>Welcome to: </b><br><i>Refill My Bottle Admin Page '$namaewa'!</i>",
                                    'typ' => 'success');
                        $this->session->set_flashdata('info', $errmsg);
                        redirect('dashboard/');
                    }else{
                        $this->session->set_flashdata('login', FALSE);
                        $errmsg[] = array('ico' => 'ti-close', 'txt' => '<b>Sorry: </b><br><i>Your Account has been disabled!<br><sub>Contact the Support Center to Activating your Account.</sub></i>',
                        'typ' => 'danger');
                    }
                }else{
                    $this->session->set_flashdata('login', FALSE);
                    $errmsg[] = array('ico' => 'ti-close', 'txt' => '<b>Warning: </b><br><i>Mail or Password is Wrong!</i>',
                        'typ' => 'danger');
                }
            }else{
                if(validation_errors()){
                    $errmsg[] = array('ico' => 'ti-info', 'txt' => '<b>Warning: </b><br><i>'.preg_replace("/(\n)+/m", '<br>', strip_tags(strip_tags(validation_errors()))).'</i>',
                        'typ' => 'warning');
                }
            }
            if(!empty($errmsg)) $this->session->set_flashdata('info', $errmsg);
            $this->load->view('login');
        }
    }

    public function logout(){
		$this->loginm->logout(TRUE);
		redirect('login');
	}
}