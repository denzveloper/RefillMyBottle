<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Place extends CI_Controller {

    public function __construct(){
        parent::__construct();
        //Must to Load!
        //load model placem
        $this->load->model('placem');
    }

    public function index(){
        $id = $this->input->get("id", TRUE);
        $x = $this->input->get("x", TRUE);
        $y = $this->input->get("y", TRUE);
        $name = $this->input->get("name", TRUE);
            $data['pl'] = $this->placem->info($id, $x, $y, $name);
            if(!empty($data['pl'])) $this->load->view("place", $data);
            else echo "<h2>Not Found</h2><p style='color: #fff'>TWFkZSBCeTogREVOWlZFTE9QRVIgRHpFTi9EekVOIOOAjERFTkRZIE9DVEFWSUFO44CN</p>";
    }
}