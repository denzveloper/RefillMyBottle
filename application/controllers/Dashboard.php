<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct(){
        parent::__construct();
        //Must to Load!
        $this->load->library('session');
        //load model loginm
        $this->load->model('loginm');
        $this->load->model('variabel');
    }

    //Index
    public function index(){
		if($this->loginm->chksess()){
            if($_SESSION['lv'] == 1){
            //If Logged in here
            $data['user_list'] = $this->loginm->chklu();
            //Give a some to page

            $data['photpt'] = base_url()."asset/plpic/".$this->loginm->getailalt('tempatisi', array('id' => $this->loginm->placeid()),'foto');
            $data['namtpt'] = $this->loginm->singkat($this->loginm->getailalt('tempatisi', array('id' => $this->loginm->placeid()),'nama'));
            $data['alatpt'] = $this->loginm->singkat($this->loginm->getailalt('tempatisi', array('id' => $this->loginm->placeid()),'alamat'), 24);

            //Calling Page
            $this->load->view("owner/dashboard", $data);

            }elseif($_SESSION['lv'] == 0){
                $data['udat'] = $this->loginm->singkatusercount();
                $this->load->view("admin/dashboard", $data);

            }else{
                //If Doesnt Login go to login
			    redirect("login");
            }
		}else{
			//If Doesnt Login go to login
			redirect("login");
		}
    }

######################################################################

    //Edit Profil all User level
    public function profil(){
        if($this->loginm->chksess()){
            if($_SESSION['lv'] < 2){
                $data['user'] = $this->loginm->usrdata();

                if($_SESSION['lv'] == 1){
                    $data['photpt'] = base_url()."asset/plpic/".$this->loginm->getailalt('tempatisi', array('id' => $this->loginm->placeid()),'foto');
                    $data['namtpt'] = $this->loginm->singkat($this->loginm->getailalt('tempatisi', array('id' => $this->loginm->placeid()),'nama'));
                    $data['alatpt'] = $this->loginm->singkat($this->loginm->getailalt('tempatisi', array('id' => $this->loginm->placeid()),'alamat'), 24);
                    $this->load->view("owner/chgpro", $data);
                }else{
                    $this->load->view("admin/chgpro", $data);
                }

            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
			//If Doesnt Login go to login
			redirect("login");
		}
    }

    //Edit Profil all User level
    public function passwd(){
        if($this->loginm->chksess()){
            if($_SESSION['lv'] < 2){
                $data['user'] = $this->loginm->usrdata();

                if($_SESSION['lv'] == 1){
                    $data['photpt'] = base_url()."asset/plpic/".$this->loginm->getailalt('tempatisi', array('id' => $this->loginm->placeid()),'foto');
                    $data['namtpt'] = $this->loginm->singkat($this->loginm->getailalt('tempatisi', array('id' => $this->loginm->placeid()),'nama'));
                    $data['alatpt'] = $this->loginm->singkat($this->loginm->getailalt('tempatisi', array('id' => $this->loginm->placeid()),'alamat'), 24);
                    $this->load->view("owner/chgpas", $data);
                }else{
                    $this->load->view("admin/chgpas", $data);
                }

            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
			//If Doesnt Login go to login
			redirect("login");
		}
    }

    //Edit Place Owner
    public function place(){
        if($this->loginm->chksess()){
            if($_SESSION['lv'] == 1){
                $data['place'] = $this->loginm->getinfpla();
                $data['type'] = $this->loginm->gettype();
                $data['type0'] = $this->loginm->gettynh($ok=$data['type']);
                $data['images'] = $this->loginm->getplaimg();
                $this->load->view("owner/place", $data);
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
			redirect("login");
        }
    }

    //Voucer by Owner
    public function voucer(){
        if($this->loginm->chksess()){
            if($_SESSION['lv'] == 1){
                $data['lst'] = $this->loginm->getvoli();
                $this->load->view("owner/voucer", $data);
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
			redirect("login");
        }
    }

    //Event by Owner
    public function event(){
        if($this->loginm->chksess()){
            if($_SESSION['lv'] == 1){
                $data['evt'] =$this->loginm->getevli();
                $this->load->view("owner/evt", $data);
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
			redirect("login");
        }
    }

    //Edit Shop Owner
    public function shop(){
        if($this->loginm->chksess()){
            if($_SESSION['lv'] == 1){
                $data['shop'] = $this->loginm->getlssh();
                $this->load->view("owner/shop", $data);
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
			redirect("login");
        }
    }

    //Add Place Admin
    public function adpl(){
        if($this->loginm->chksess()){
            if($_SESSION['lv'] == 0){
                $this->load->view("admin/adpl");
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
            redirect("login");
        }
    }

    //New Place by User Admin
    public function npu(){
        if($this->loginm->chksess()){
            if($_SESSION['lv'] == 0){
                $data['lst'] = $this->loginm->getnpl();
                $this->load->view("admin/npu", $data);
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
            redirect("login");
        }
    }

    //Adduser Admin
    public function addusr(){
        if($this->loginm->chksess()){    
            if($_SESSION['lv'] == 0){
                $this->load->view("admin/addusr");
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
            redirect("login");
        }
    }

    //Manage place Admin
    public function mngplc(){
        if($this->loginm->chksess()){
            if($_SESSION['lv'] == 0){
                $data['place'] = $this->loginm->getplzrl();
                $this->load->view("admin/mnpl", $data);
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
            redirect("login");
        }
    }

    //Manage User Admin
    public function mngusr(){
        if($this->loginm->chksess()){
            if($_SESSION['lv'] == 0){
                $data['user'] = $this->loginm->getusrall();
                $this->load->view("admin/manuser", $data);
            }else{
                //If No acceptable level
                redirect("dashboard");
            }
        }else{
            //If Doesnt Login go to login
            redirect("login");
        }
    }

##############################################################
}