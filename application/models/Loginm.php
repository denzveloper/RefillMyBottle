<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loginm extends CI_Model{

    //You Know lah ini login
    function login($f1,$f2){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where($f1);
        $this->db->where($f2);
        $this->db->where('level <', '2');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result();
        }
    }

    //Function check access
    function chksess(){
        return $this->session->userdata('mail');
    }

    //function get a id place
    function placeid(){
        $mail = $_SESSION['mail'];
        $this->load->model('waterm');
        return $this->getailalt('kepemilikan', array('surel' => $mail), 'id_tempat');
    }

    //Blocking User
    function blockus($f1){
        $this->load->model("waterm");
        $dis = $this->getailalt('user', array('surel' => $f1), 'disable');

        $this->db->where('surel', $f1);
        if($dis) $this->db->set("disable", '0', false);
        else $this->db->set("disable", '1', false);
        $this->db->update('user');

        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    //Get Detailed On another table ALternative
    function getailalt($f1, $f2, $f3){
        if($this->infoex($f1, $f2)){
            $this->db->from($f1);
            $this->db->where($f2);
            $query = $this->db->get();
            return $query->row()->$f3;
        }else{
            return null;
        }
    }

    //Get Detailed On another table Order first
    function getshopid($f1, $f2){
        $table = "jual";
        if($this->infoex($table, $f1)){
            $this->db->from($table);
            $this->db->where($f1);
            $this->db->where($f2);
            $this->db->order_by('id', 'desc');
            $this->db->limit(1);
            $query = $this->db->get();
            return $query->row()->id;
        }else{
            return null;
        }
    }

    //have exists info checker
    function infoex($f1, $f2){
        $this->db->select('*');
        $this->db->from($f1);
        $this->db->where($f2);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        }else{
            return TRUE;
        }
    }

    //Get info
    function getinfpla(){
        $id = $this->placeid();

        $this->db->select('*');
        $this->db->from('tempatisi');
        $this->db->where('id',$id);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        }else{
            return $query->result_array();
        }
    }

    //get detail shop
    function getlssh(){
        $this->load->library("safe");
        $id = $_SESSION['mail'];

        $this->db->select('*');
        $this->db->from('jual');
        $this->db->where('by_user', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        }else{
            foreach($query->result_array() as $row){
                $row['ktg'] = $this->singkat($row['detail'], 20);
                $row['name'] = $this->singkat($row['nama'], 15);
                $row['link'] = base_url('index.php/update/sotodo?id=')."$row[id]&key=".$this->safe->encrypt($row['name'], $id);
                $data[] = $row;
            }
            return $data;
        }
    }

    //Get image place
    function getplaimg(){
        $this->load->library("safe");
        $id = $this->placeid();

        $this->db->select('*');
        $this->db->from('img_tempat');
        $this->db->where('id_tempat', $id);
        $query = $this->db->get();
        if($query->num_rows() != 0){
            foreach($query->result_array() as $row){
                $row['images'] = base_url('asset/papic/').$row['image'];
                $row['link'] = base_url('index.php/update/acph?id=')."$row[id]&key=".$this->safe->encrypt($row['image'], $id);
                $data[] = $row;
            }
            return $data;
        }else{
            return FALSE;
        }
    }

    //Get event list
    function getevli(){
        $this->load->library("safe");
        $id = $_SESSION['mail'];

        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('by_user', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        }else{
            foreach($query->result_array() as $row){
                $row['ktg'] = $this->singkat($row['keterangan'], 20);
                $row['name'] = $this->singkat($row['nama'], 15);
                $row['link'] = base_url('index.php/update/evtodo?id=')."$row[id]&key=".$this->safe->encrypt($row['name'], $id);
                $data[] = $row;
            }
            return $data;
        }
    }

    //Get Voucher list
    function getvoli(){
        $this->load->library("safe");
        $id = $_SESSION['mail'];

        $this->db->select('*');
        $this->db->from('voucer');
        $this->db->where('by_user', $id);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        }else{
            foreach($query->result_array() as $row){
                $row['name'] = $this->singkat($row['nama'], 15);
                $row['link'] = base_url('index.php/update/votodo?id=')."$row[id]&key=".$this->safe->encrypt($row['kode'], $id);
                $data[] = $row;
            }
            return $data;
        }
    }

    //Checking any? Empty True - Exist False
    function chkean($f1, $f2, $f3=FALSE){
        $this->db->select('*');
        $this->db->from($f1);
        $this->db->where($f2);
        if($f3 != FALSE) $this->db->where($f3);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return TRUE;
        }else{
            return FALSE;
        }
    }

    //Get any data?
    function getean($f1, $f2, $f3=FALSE, $f4=FALSE){
        $this->db->select('*');
        $this->db->from($f1);
        $this->db->where($f2);
        if($f3 != FALSE) $this->db->where($f3);
        if($f4 != FALSE) $this->db->where($f4);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        }else{
            return $query->result_array();
        }
    }

    //Add Data
    function addsm($f1, $f2){
        $this->db->insert($f1, $f2);
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    //Add Event
    function addpict($f1){
        $this->db->insert('img_tempat', $f1);
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    //Update di table event
    function upev($f1,$f2, $f3){
        $this->db->where($f1);
        $this->db->where($f2);
        $this->db->update("event", $f3);
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    //Update table voucher
    function upvo($f1, $f2){
        $this->db->where($f1);
        $this->db->update("voucer", $f2);
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    //Update
    function upsh($f1, $f2, $f3){
        $this->db->where($f2);
        $this->db->update($f1, $f3);
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    //Delete Somedata
    function delete($f1, $f2, $f3=FALSE){
        $this->db->where($f2);
        $this->db->delete($f1);
        if($f3 == TRUE) $this->db->limit(1);
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    function logdata($f1){
        $array = array('mail' => $f1, 'place' => $f2, 'place_user' => $f3, 'transaction' => $f4, 'detail' => $f5);
        $this->addsm('transaction', $array);
    }

    //... when > 16 char
    function singkat($an, $x = 16){
    if (strlen($an) > $x){
        $x = $x - 3;
        $an = substr($an, 0, $x) . '&#8230;';
    }
    return $an;
    }

    //Check User Location
    function chklu($f1 = FALSE){
        $pla = $this->placeid();
        $lon = $this->getailalt('tempatisi', array('id' => $pla), 'longitude');
        $lat = $this->getailalt('tempatisi', array('id' => $pla), 'latitude');

        $this->db->from('log_token');
        
        $a = $lon - 0.0035;
        $b = $lon + 0.0035;
        $this->db->where("longitude BETWEEN $a AND $b");

        $a = $lat - 0.0035;
        $b = $lat + 0.0035;
        $this->db->where("latitude BETWEEN $a AND $b");

        if($f1 != FALSE){
            $this->db->where("user_mail", $f1);
        }

        $query = $this->db->get();
        $data = array();
        if($query !== FALSE && $query->num_rows() > 0){
            foreach ($query->result_array() as $row) {
                $data[] = array('id' => $row['user_mail'],
                'name' => $this->waterm->getail('user', array('surel' => $row['user_mail']))->namadepan,
                'point' => $this->waterm->getail('user', array('surel' => $row['user_mail']))->poin);
            }
        }
        return $data;
    }

    //Add point User
    function addpnt($f1, $f2){
        $chk = $this->chklu($f1);
        if(!empty($chk)){
            $this->db->where('surel', $f1);
            $this->db->set("poin", "poin+$f2", false);
            $this->db->set("progress", "progress+$f2", false);
            $this->db->update("user");
            $query = $this->db->affected_rows();
            if ($query == 0) {
                return FALSE;
            }else{
                return $query;
            }
        }else{
            return FALSE;
        }
    }

    //Check any today point
    function chkpne($f1, $f2){
        $this->db->select('*');
        $this->db->from('konsumsi');
        $this->db->where('surel', $f1);
        $this->db->where('tgl', $f1);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() == 0){
            return FALSE;
        }else{
            return TRUE;
        }
    }

    //Add consumption
    function addcom($f1, $f2){
        $chk = $this->chklu($f1);
        if(!empty($chk)){
            $date = date('Y-m-d');
            $chk = $this->chkpne($f1, $date);

            if($chk != FALSE){
                $this->db->where('surel', $f1);
                $this->db->where('tgl', $date);
                $this->db->set("harian", "harian+$f2", false);
                $this->db->update("konsumsi");
                $query = $this->db->affected_rows();
                if ($query == 0) {
                    return FALSE;
                }else{
                    return $query;
                }
            }else{
                $array = array('surel' => $f1, 'tgl' => $date, 'harian' => $f2);
                $this->db->insert('konsumsi', $array);
                $query = $this->db->affected_rows();
                if ($query == 0) {
                    return FALSE;
                }else{
                    return $query;
                }
            }
        }else{
            return FALSE;
        }
    }

    //Get Selected Tag
    function gettype(){
        $id = $this->placeid();
        $this->db->select('*');
        $this->db->from('tmpt_tag');
        $this->db->where('id_tempat',$id);
        $data = array();
        $query = $this->db->get();
        if($query !== FALSE && $query->num_rows() > 0){
            foreach ($query->result_array() as $row) {
                $data[] = array('nama' => $row['tag']);
            }
        }else{
            return FALSE;
        }
        return $data;
    }

    //Get Unselected Tag
    function gettynh($f1){
        if($f1 != FALSE){
            $hit = array();
            foreach($f1 as $row){
                $hit[] = $row['nama'];
            }
            $this->db->select('*');
            $this->db->from('interest_list');

            $this->db->where_not_in('nama',$hit);
            $data = array();
            $query = $this->db->get();
            if($query !== FALSE && $query->num_rows() > 0){
                foreach ($query->result_array() as $row) {
                    $data[] = $row;
                }
            }
            return $data;
        }else{
            $this->db->select('*');
            $this->db->from('interest_list');

            $data = array();
            $query = $this->db->get();
            if($query !== FALSE && $query->num_rows() > 0){
                foreach ($query->result_array() as $row) {
                    $data[] = $row;
                }
            }
            return $data;
        }
    }
    
    //Update place
    function updplc($f1){
        $id = $this->placeid();
        
        $this->db->where('id', $id);
        $this->db->update("tempatisi", $f1);
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    //Update Type Place
    function updtyp($f1){
        $id = $this->placeid();
        $this->deltagpl(array('id_tempat' => $id));
        $x = 0;
        $y = 0;
        while(isset($f1[$x])){
            //code
            $hit = $f1[$x];
            $cek1 = $this->ctagpl(array('id_tempat' => $id), array('tag' => $hit));
            if($cek1 == FALSE){
                $cek2 = $this->addtagpl(array('id_tempat' => $id, 'tag' => $hit));
                if($cek2 != FALSE){
                    $y++;
                }
            }
            $x++;
        }
        if($x == $y){
            $hit = TRUE;
        }else{
            $hit = FALSE;
        }
        return $hit;
    }

    //Delete tag place
    function deltagpl($f1){
        $this->db->where($f1);
        $this->db->delete('tmpt_tag');
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    //Check tag place
    function ctagpl($f1, $f2){
        $this->db->from('tmpt_tag');
        $this->db->where($f1);
        $this->db->where($f2);
        $query = $this->db->get();
        if($query !== FALSE && $query->num_rows() > 0){
            $data = array();
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
        }else{
            $data = FALSE;
        }
        return $data;
    }

    function usrdata(){
        $mail = $_SESSION['mail'];
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('surel',$mail);
        $this->db->where('level <', '2');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result_array();
        }
    }

    //Adding tag place
    function addtagpl($f1){
        $this->db->insert('tmpt_tag', $f1);
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    function updpro($f1, $f2){
        //Library
        $this->load->library('session');
        $mail = $_SESSION['mail'];

        $this->db->where('surel', $mail);
        $this->db->where($f1);
        $this->db->update("user", $f2);
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            $this->logout();
            $cek = $this->login(array('surel' => $mail), $f1);
            if ($cek != FALSE){
                foreach ($cek as $hit){
            	    $sesar = array(
                        'logged_in' => TRUE,
                        'mail' => $hit->surel,
                        'fnam' => $hit->namadepan,
                        'lnam' => $hit->namabelakang,
                        'lv' => $hit->level,
                        'foto' => base_url()."asset/uspic/".$hit->photo
                    );
                }
                //set session userdata
                $this->session->set_userdata($sesar);
            }
            return $query;
        }
    }

    function logout($f1 = FALSE){
        //library
        $this->load->library('session');
        //logout
		$arraydata = array(
            'logged_in', 'mail', 'fnam', 'lnam', 'lv'
        );
        $this->session->unset_userdata($arraydata);

        //if logout session
        if($f1 == TRUE){
            $this->session->sess_destroy();
        }
    }

    function singkatusercount(){
        $q1 = $this->db->from('user')->where('level', '2')->get()->num_rows();
        $q2 = $this->db->from('log_token')->get()->num_rows();

        $a1 = $this->db->from('user')->where('level', '0')->get()->num_rows();
        $a2 = $this->db->from('user')->where('level', '1')->get()->num_rows();
        $a3 = $this->db->from('user')->where('level', '2')->get()->num_rows();
        return array('jumlah' => $q1-$q2, 'aktif' => $q2, 'lvla' => $a1, 'lvlb' => $a2, 'lvlc' => $a3);
    }

    function getusrall(){
        $this->load->library("safe");
        $q = $this->db->from('user')->where('level >', '0')->get();
        if($q->num_rows() != 0){
            foreach($q->result_array() as $row){
                $n = $row['namadepan']; 
                $n .= ' '.$row['namabelakang'];
                $row['nama'] = $this->singkat($n, 20);
                $row['stat'] = "Blokir";
                if($row['disable'] == 1)$row['stat'] = "Unblock";
                $row['link'] = base_url('index.php/update/usrtodo?usr=').$this->safe->encrypt($row['surel'], $_SESSION['mail']);
                $data[] = $row;
            }
            return $data;
        }else{
            return FALSE;
        }
    }

    function getnpl(){
        $this->load->library("safe");
        $q = $this->db->from('tempat_baru')->get();
        if($q->num_rows() != 0){
            foreach($q->result_array() as $row){
                $nadep = $this->getailalt('user', array('surel' => $row['pendaftar']), 'namadepan');
                $nakang = $this->getailalt('user', array('surel' => $row['pendaftar']), 'namabelakang');
                $row['users'] = $nadep." ".$nakang;
                $row['class'] = "text-warning";
                if($row['relasi'] == "owner") $row['class'] = "text-danger";
                $row['user'] =  $this->singkat($row['users'], 12);
                $row['name'] = $this->singkat($row['nama'], 12);
                $row['addr'] = $this->singkat($row['alamat'], 20);
                $row['link'] = base_url('index.php/update/npltd?plac=').$this->safe->encrypt($row['id_tempat'], $_SESSION['mail']);
                $query = $this->db->from('pic_tmpt_baru')->where('id_tempat', $row['id_tempat'])->get();
                if($query->num_rows() != 0){
                    foreach($query->result_array() as $img){
                        $row['img'][] = $img;
                    }
                }
                $data[] = $row;
            }
            return $data;
        }else{
            return FALSE;
        }
    }

    function getimgf($f1){
        $q = $this->db->from('pic_tmpt_baru')->where('id_tempat', $f1)->limit(1)->get();
        if($q->num_rows() != 0){
            return $q->row()->image;
        }else{
            return 'default.png';
        }
    }

    //Get event list
    function getimnli($f1, $f2){
        $this->load->library("safe");

        $this->db->select('*');
        $this->db->from('pic_tmpt_baru');
        $this->db->where_not_in('image', $f1);
        $this->db->where('id_tempat', $f2);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        }else{
            return $query->result_array();
        }
    }
    
    //Get List Place All
    function getplzrl(){
        $this->load->library("safe");
        $q = $this->db->from('tempatisi')->get();
        if($q->num_rows() != 0){
            foreach($q->result_array() as $row){
                $row['name'] = $this->singkat($row['nama'], 12);
                $row['pre'] = "ti-close";
                $row['prec'] = "text-danger";
                if($row['premium'] == "y"){
                    $row['pre'] = "ti-check";
                    $row['prec'] = "text-primary";
                }
                $row['addr'] = $this->singkat($row['alamat'], 20);
                $row['img'][] = base_url('asset/plpic/').$row['foto'];
                $row['link'] = base_url('index.php/update/mngplcz?plac=').$this->safe->encrypt($row['id'], $_SESSION['mail']);
                $query = $this->db->from('img_tempat')->where('id_tempat', $row['id'])->get();
                if($query->num_rows() > 0){
                    foreach($query->result_array() as $img){
                        $row['img'][] = base_url('asset/papic/').$img['image'];
                    }
                }else{
                    $row['img'][] = base_url('asset/papic/default.png');
                }
                $data[] = $row;
            }
            return $data;
        }else{
            return FALSE;
        }
    }
}