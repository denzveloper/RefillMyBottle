<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot extends CI_Model{

    //Code
    function mail($f1, $f2, $f3){
        $config = Array(
            //Configuration of sends mail
            'protocol' => 'smtp',
            'smtp_crypto' => 'tls',
            'smtp_host' => 'smtp-mail.outlook.com',
            'smtp_port' => 587,
            'smtp_user' => 'refillbottleina@outlook.co.id',
            'smtp_pass' => 'refillid123',
            'mailtype'  => 'html',
            'charset'   => 'utf-8'
        );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");

        $msg = $this->messages($f2, $f3);
        $this->email->from($config['smtp_user'], 'Refill My Bottle');
        $this->email->to($f1);

        $this->email->subject('Forgot Password Recovery');
        $this->email->message($msg);

        if(!$this->email->send()){
            echo $this->email->print_debugger();
        }
    }

        //Generate Token
        function pass($f1){
            //Load Library and model
            $this->load->library('safe');
            $this->load->model('waterm');

            $t = time();
            $rand = rand(100000, 999999);
            $x = $this->safe->encrypt($rand, $t);
            $fnam = $this->waterm->getail('user', array('surel' => $f1))->namadepan;
            $c = $this->db->select('*')->from('user')->where('surel', $f1)->where('level', '2')->limit(1)->get();
            if ($c->num_rows() > 0) {
                $this->mail($f1, $fnam, $x);
                $this->waterm->uptabusr(array('surel' => $f1),array('sandi' => $x));
                $this->waterm->dtok(array('surel' => $f1));
            }
        }

    function messages($f1, $f2){
        return "Hi $f1! Here is Your New password Code: \"<i><b><u>$f2</u></b></i>\".<br><i>You can change password after login in Your Profile Account</i>";
    }
}