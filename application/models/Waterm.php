<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Waterm extends CI_Model{
    
    //You Know lah ini login
    function login($f1, $f2){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where($f1);
        $this->db->where($f2);
        $this->db->where('level', '2');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        }else{
            return $query->result();
        }
    }

    //Generate Token
    function gtok($f1){
        //Code Generate token
        $this->load->library('safe');
        $this->load->library('user_agent');
        $t = time();
        $date = date('Y-m-d H:i:s', $t);
        $b = $this->agent->agent_string();
        $x = $this->safe->encrypt($f1, $t);
        $a = $this->db->select('*')->from('log_token')->where('user_mail', $f1)->limit(1)->get();
        $c = $this->db->select('*')->from('user')->where('surel', $f1)->where('level', '2')->limit(1)->get();
        if ($a->num_rows() == 0 && $c->num_rows() > 0) {
            //If Ok, Create a new token
            $ip = $this->safe->get_client_ip();
            $array = array('token' => $x, 'user_mail' => $f1, 'ip' => $ip, 'created' => $date, 'useragent' => $b);
            $this->db->insert('log_token', $array);
            $query = $this->db->affected_rows();
            if ($query == 0) {
            return FALSE;
            }else{
            $hit[] = array('data' => $x, 'date' => $date);
            return $hit;
            }
        }else{
            return FALSE;
        }
    }

    //Check Token
    function ctok($f1){
        $this->load->library('safe');
        $a = $this->db->select('*')->from('log_token')->where($f1)->limit(1)->get();
        if($a->num_rows() == 0){
            return FALSE;
        }else{
            return $a->row();
        }
    }

    //Delete Token
    function dtok($f1){
        $this->load->library('safe');
        $a = $this->ctok($f1);
        if($a != FALSE){
            $this->db->where($f1)->delete('log_token');
            $query = $this->db->affected_rows();
            if ($query == 0) {
                return FALSE;
            }else{
                return $query;
            }
        }else{
            return FALSE;
        }
    }

    //Get Detailed On another table
    function getail($f1, $f2){
        $this->db->from($f1);
        $this->db->where($f2);
        $query = $this->db->get();
        return $query->row();
    }

    //Checking Registered?
    function chkreg($f1, $f2){
        $this->db->select('*');
        $this->db->from($f1);
        $this->db->where($f2);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return TRUE;
        }else{
            return FALSE;
        }
    }

    //Add User
    function reg($f1){
        $this->db->insert('user', $f1);
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    //Update di table user
    function uptabusr($f1,$f2){
        $this->db->where($f1);
        $this->db->update("user", $f2);
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    //Fetching data Country
    function cofetch(){
        $this->db->from('country');
        $this->db->order_by('Name');
        $query = $this->db->get();
        if($query != FALSE && $query->num_rows() > 0){
            $data = array();
            foreach ($query->result() as $row) {
                $data[] = array('country' => $row->Name);
            }
        }else{
            $data = FALSE;
        }
        return $data;
    }

    //Fetching data State/City
    function scfetch($f1, $f2){
        $this->db->select('*');
        $this->db->from('city');
        $this->db->where($f1);
        $this->db->order_by($f2);
        $query = $this->db->get();
        if($query !== FALSE && $query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }

    function fetchfaf($f1){
        $this->db->from("kesukaan_pengguna");
        $this->db->where($f1);
        $query = $this->db->get();
        if($query !== FALSE && $query->num_rows() > 0){
            $data = array();
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
        }else{
            $data = FALSE;
        }
        return $data;
    }

    //Ambil seluruh data
    function fetchdata($f1){
        $this->db->from($f1);
        if($f1=="voucer"){
            $where = "vopic/";
            $this->db->where("expired >=", date("Y-m-d"));
            
        }else{
            $where = "";
        }
        $query = $this->db->get();
        $data = array();
        if($query !== FALSE && $query->num_rows() > 0){
            foreach ($query->result_array() as $row) {
                $row['foto'] = base_url()."asset/".$where.$row['foto'];
                $data[] = $row;
            }
        }
        return $data;
    }

    function shtop(){
        //shorting
        $this->db->from("poin_tra");
        $this->db->order_by('id_item asc');
        $query = $this->db->get();
        if($query !== FALSE && $query->num_rows() > 0){
            $ok = array();
            $ave = '';
            $x = 0;
            foreach($query->result_array() as $row){
                //Belum Jadi
                $hit=$row['id_item'];
                if($hit != $ave || $hit == null){
                    if($x > 0){
                        $ok += [$ave => $x];
                        $x = 1;
                        $ave = $hit;
                    }else{
                        $x++;
                        $ave = $hit;
                    }
                }else{
                    $x++;
                }
            }
            $ok += [$ave => $x];
        }else{
            $ok = FALSE;
        }
        return $ok;
    }

    //Ambil seluruh data
    function fetchshop($f1 = FALSE, $f2 = FALSE){
        $data = array();
        if($f1 == TRUE && $f2 != FALSE){
            //shorting
            foreach($f2 as $f2){
                $this->db->from("jual");
                $this->db->where('id', $f2);
                $this->db->where("exp >=", date("Y-m-d"));
                $query = $this->db->get();
                $got = array();
                if($query != FALSE && $query->num_rows() > 0){
                    foreach ($query->result_array() as $row) {
                        $row['foto'] = base_url()."asset/sopic/".$row['foto'];
                        $data[] = array('id' => $row['id'], 'nama' => $row['nama'], 'harga' => $row['harga'],
                            'harga_poin' => $row['harga_poin'], 'ambil_poin' => $row['ambil_poin'],
                            'foto' => $row['foto']);
                    }
                }
            }
        }else{
            $this->db->from("jual");
            $this->db->where("exp >=", date("Y-m-d"));
            $query = $this->db->get();
            if($query != FALSE && $query->num_rows() > 0){
                foreach ($query->result_array() as $row) {
                    $row['foto'] = base_url()."asset/sopic/".$row['foto'];
                    $data[] = array('id' => $row['id'], 'nama' => $row['nama'], 'harga' => $row['harga'],
                        'harga_poin' => $row['harga_poin'], 'ambil_poin' => $row['ambil_poin'],
                        'foto' => $row['foto']);
                }
            }
        }
        return $data;
    }

    function desho($f1, $f2){
        $this->db->from("jual");
        $this->db->where('id', $f1);
        $this->db->where("exp >=", date("Y-m-d"));
        $query = $this->db->get();
        $got = array();
        if($query != FALSE && $query->num_rows() > 0){
            $data = array();
            foreach ($query->result_array() as $row) {
                unset($got);
                $row['foto'] = base_url()."asset/sopic/".$row['foto'];
                $get = $this->db->from('jual_img')->where('id_jual', $row['id'])->get();
                if($get !== FALSE && $get->num_rows() > 0){
                    foreach($get->result() as $hit){
                        $got[] = array("img" => base_url()."asset/sdpic/".$hit->foto);
                    }
                }else{
                    $got = array(base_url()."asset/sdpic/default.png");
                }
                $x = array('image' => $got);
                $row['url'] = base_url()."index.php/shopping"."?tkn=".$f2."&id=".$f1;
                $data[] = array_merge($row, $x);
            }
        }else{
            $data = FALSE;
        }
        return $data;
    }

    //Filter Map
    function fmap($f1){
        $this->db->from("tmpt_tag");
        if($f1 != FALSE){
            $x = 0;
            foreach($f1 as $f1){
                if($x < 1){
                    $this->db->where("tag", "$f1->interest");
                }else{
                    $this->db->or_where("tag", "$f1->interest");
                }
                $x++;
            }
        }
        $query = $this->db->get();
        if($query !== FALSE && $query->num_rows() > 0){
            $data = array();
            foreach ($query->result_array() as $row) {
                $data[] = $row['id_tempat'];
            }
        }else{
            $data = FALSE;
        }
        $a = array_flip($data);
        $a = array_flip($a);
        $data= array_values($a);

        return $data;
    }

    //Ambil seluruh map
    function fetchmap($f1, $f2, $f3, $f4){
        $data = array();
        if($f2 != FALSE){
            foreach($f2 as $f2){
                $this->db->from($f1);
                //$this->db->like('longitude', $f3);
                //$this->db->like('latitude', $f4);
                
                $a = $f3 - 0.01;
                $b = $f3 + 0.01;
                $this->db->where("longitude BETWEEN $a AND $b");

                $a = $f4 - 0.01;
                $b = $f4 + 0.01;
                $this->db->where("latitude BETWEEN $a AND $b");
                
                $this->db->where("id", $f2, FALSE);

                $query = $this->db->get();
                $data = array();
                #$got = array();
                if($query !== FALSE && $query->num_rows() > 0){
                    foreach ($query->result_array() as $row) {
                        $got = $this->restar($row['id']);
                        if($row['premium']=='y'){
                            $row['mark']="premium";
                        }elseif($this->cek_bayar($f1) != TRUE){
                            $row['mark']="dollar";
                        }else{
                            $row['mark']="water";
                        }
                        $data[] = array('id' => $row['id'], 'nama' => $row['nama'],
                        'mark' => $row['mark'], 'harga' => $row['harga'],
                        'latitude' => $row['latitude'], 'longitude' => $row['longitude'],
                        'star' => $got);
                    }
                }
            }
        }else{
            $this->db->from($f1);
            //$this->db->like('longitude', $f3);
            //$this->db->like('latitude', $f4);
            
            $a = $f3 - 0.01;
            $b = $f3 + 0.01;
            $this->db->where("longitude BETWEEN $a AND $b");

            $a = $f4 - 0.01;
            $b = $f4 + 0.01;
            $this->db->where("latitude BETWEEN $a AND $b");
            $query = $this->db->get();
            $data = array();
            #$got = array();
            if($query !== FALSE && $query->num_rows() > 0){
                foreach ($query->result_array() as $row) {
                    $got = $this->restar($row['id']);
                    if($row['premium']=='y'){
                        $row['mark']="premium";
                    }elseif($this->cek_bayar($f1) != TRUE){
                        $row['mark']="dollar";
                    }else{
                        $row['mark']="water";
                    }
                    $data[] = array('id' => $row['id'], 'nama' => $row['nama'],
                    'mark' => $row['mark'], 'harga' => $row['harga'],
                    'latitude' => $row['latitude'], 'longitude' => $row['longitude'],
                    'star' => $got);
                }
            }
        }
        return $data;
    }

    //get image
    function imgmap($f1){
        $get = $this->db->from('img_tempat')->where('id_tempat', $f1)->get();
        //Added
        $name = "";
        $pnt = "";
        $get1 = "";
        //Added
        if($get != FALSE && $get->num_rows() > 0){
            $got  = array();
            //Added
            $get1 = $this->restar($f1);
            $name = $this->getail('tempatisi', array('id' => $f1))->nama;
            $lat = $this->getail('tempatisi', array('id' => $f1))->latitude;
            $lon = $this->getail('tempatisi', array('id' => $f1))->longitude;
            $pnt = array('latitude' => $lat, 'longitude' => $lon);
            //Added
            foreach($get->result() as $hit){
                $got[] = base_url()."asset/papic/".$hit->image;
            }

        }else{
            $got = array();
        }
        
        $got = array_merge(array("name" => $name), array("lonlat" => $pnt), array("star" => $get1), array("image" => $got));
        return $got;
    }

    //get image alternate
    function revimgmap($f1){
        $get = $this->db->from('img_tempat')->where('id_tempat', $f1)->get();
        //Added
        /*
        $name = "";
        $pnt = "";
        $get1 = "";
        */
        $get1 = $this->restar($f1);
        $name = $this->getail('tempatisi', array('id' => $f1))->nama;
        $lat = $this->getail('tempatisi', array('id' => $f1))->latitude;
        $lon = $this->getail('tempatisi', array('id' => $f1))->longitude;
        //Added
        if($get != FALSE && $get->num_rows() > 1){
            $got  = array();
            //Added
            /*
            $get1 = $this->restar($f1);
            $name = $this->getail('tempatisi', array('id' => $f1))->nama;
            $lat = $this->getail('tempatisi', array('id' => $f1))->latitude;
            $lon = $this->getail('tempatisi', array('id' => $f1))->longitude;
            */
            //Added
            foreach($get->result() as $hit){
                $row['name'] = $name;
                $row['star'] = $get1;
                $row['latitude'] = $lat;
                $row['longitude'] = $lon;
                $row['img'] = base_url()."asset/papic/".$hit->image;
                $got[] = $row;
            }
        }else{
            if($name != null){
                $img = $this->getail('tempatisi', array('id' => $f1))->foto;
                $plp = array(
                    'name' => $name,
                    'star' => $get1,
                    'latitude' => $lat,
                    'longitude' => $lon,
                    'img' => base_url()."asset/plpic/".$img
                );
                $opp = array(
                    'name' => $name,
                    'star' => $get1,
                    'latitude' => $lat,
                    'longitude' => $lon,
                    'img' => base_url()."asset/papic/default.png"
                );
                $got = array($plp, $opp);
                
            }else{
                $got = FALSE;
            }
        }
        return $got;
    }

    function revmap($f1){
        $get = $this->db->from('review_tempat')->where('id_tempat', $f1)->join('user', 'user.surel = review_tempat.surel')->get();
        if($get !== FALSE && $get->num_rows() > 0){
            $got  = array();
            foreach($get->result() as $hit){
                $got[] = array(
                    'id_review' => $hit->id,
                    'nama' => $hit->namadepan." ".$hit->namabelakang,
                    'star' => $hit->bintang,
                    'komentar' => $hit->komentar,
                    'datetime' => $hit->datetime
                );
            }
        }else{
            $got = array();
        }
        return $got;
    }

    function restar($f1){
        $get = $this->db->from('review_tempat')->where('id_tempat', $f1)->get();
        if($get !== FALSE && $get->num_rows() > 0){
            $x = 0;
            $star = 0;
            foreach($get->result() as $hit){
                $star += $hit->bintang;
                $x++;
            }
            $star = $star / $x; 
        }else{
            $star = 0;
        }
        $star = (string)$star;
        return $star;
    }

    function detamap($f1){
        $this->db->from("tempatisi");
        $this->db->where("id", $f1);
        $query = $this->db->get();
        if($query !== FALSE && $query->num_rows() > 0){
            foreach ($query->result_array() as $row) {
                $data = array();
                $got = array();
                $got1  = array();
                $row['foto'] = base_url()."asset/plpic/".$row['foto'];
                $got = $this->restar($f1);
                $get = $this->db->from('tmpt_tag')->where('id_tempat', $f1)->get();
                if($get !== FALSE && $get->num_rows() > 0){
                    unset($got1);
                    foreach($get->result() as $hit){
                        $got1[] = array('tags' => $hit->tag);
                    }
                }
                #$got2 = $this->imgmap($f1);

                //Time Only Show HH:MM
                $row['jam_buka'] = date('H:i', strtotime($row['jam_buka']));
                $row['jam_tutup'] = date('H:i', strtotime($row['jam_tutup']));

                //Selection Icon
                if($row['premium']=='y'){
                    $row['mark']="premium";
                }elseif($this->cek_bayar($f1) != TRUE){
                    $row['mark']="dollar";
                }else{
                    $row['mark']="water";
                }

                $this->load->library('safe');
                $x = $this->safe->encrypt($row['latitude'], $row['id'].$row['nama']);
                $y = $this->safe->encrypt($row['longitude'], $row['nama'].$row['id']);
                $add = base_url()."index.php/place?id=$row[id]&name=$row[nama]&x=$x&y=$y";
                $row['shaddr'] = $add; 

                $x = array('tag' => $got1);
                $y = array('star' => $got);
                $data[] = array_merge($row, $y, $x);
            }
        }else{
            $data = FALSE;
        }
        return $data;
    }

    //Cek Not other tag
    function cek_bayar($f1){
        $get = $this->db->from('tmpt_tag')->where('id_tempat', $f1)->where('tag', 'other')->get();
        if($get !== FALSE && $get->num_rows() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    //Ambil seluruh event
    function fetchev($f1){
        $this->db->from($f1);
        $query = $this->db->get();
        $data = array();
        if($query !== FALSE && $query->num_rows() > 0){
            foreach ($query->result_array() as $row) {
                $hit = $row['id'];
                $row['foto'] = base_url()."asset/evpic/".$row['foto'];
                $get0 = $this->db->from('suka_event')->where('id_event', $hit)->get()->num_rows();
                $x = array('like' => $get0);
                $get1 = $this->db->from('share_event')->where('id_event', $hit)->get()->num_rows();
                $y = array('share' => $get1);
                $data[] = array_merge($row,$x,$y);
            }
        }
        return $data;
    }

    //Ambil event id
    function etail($f1, $f2){
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where($f1, $f2);
        $query = $this->db->get();
        $data = array();
        if($query !== FALSE && $query->num_rows() > 0){
            foreach ($query->result_array() as $row) {
                $hit = $row['id'];
                $row['foto'] = base_url()."asset/evpic/".$row['foto'];
                $get0 = $this->db->from('suka_event')->where('id_event', $hit)->get()->num_rows();
                $x = array('like' => $get0);
                $get1 = $this->db->from('share_event')->where('id_event', $hit)->get()->num_rows();
                $y = array('share' => $get1);
                $data[] = array_merge($row,$x,$y);
            }
        }
        return $data;
    }

    //Ambil seluruh event by date
    function ddtail($f1){
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where('DATE(tgl)', $f1);
        $query = $this->db->get();
        $data = array();
        if($query !== FALSE && $query->num_rows() > 0){
            foreach ($query->result_array() as $row) {
                $hit = $row['id'];
                $row['foto'] = base_url()."asset/evpic/".$row['foto'];
                $get0 = $this->db->from('suka_event')->where('id_event', $hit)->get()->num_rows();
                $x = array('like' => $get0);
                $get1 = $this->db->from('share_event')->where('id_event', $hit)->get()->num_rows();
                $y = array('share' => $get1);
                $data[] = array_merge($row,$x,$y);
            }
        }else{
            $data = FALSE;
        }
        return $data;
    }

    //Check Event ada/tidak
    function chkev($f1){
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where($f1);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() == 0){
            return FALSE;
        }else{
            return TRUE;
        }
    }

    //Check share/like/other yet?
    function chsm($f1,$f2,$f3){
        $this->db->select('*');
        $this->db->from($f1);
        $this->db->where($f2);
        $this->db->where($f3);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() == 0){
            return FALSE;
        }else{
            return $query->result();
        }
    }

    //Add share/like/other
    function addsm($f1, $f2){
        $this->db->insert($f1, $f2);
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    //Update data
    function uprev($f1, $f2, $f3, $f4){
        $this->db->where($f2);
        $this->db->where($f3);
        $this->db->update($f1,$f4);
        $query = $this->db->affected_rows();
        if($query == 0){
            return FALSE;
        }else{
            return $query;
        }
    }

    //Delete like
    function delsm($f1, $f2){
        $this->db->where($f2);
        $this->db->delete($f1);
        $query = $this->db->affected_rows();
        if($query == 0){
            return FALSE;
        }else{
            return $query;
        }
    }

    //Point updating
    function pntud($f1, $f2){
        $this->db->set("poin", "poin$f1", false);
        $this->db->where($f2);
        $this->db->update("user");
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    //Save Monitor monthly
    function saved_mon($f1, $f2){
        $this->db->select('*');
        $this->db->from('konsumsi');
        $this->db->where($f1);
        $this->db->where("DATE_FORMAT(tgl,'%Y-%m')", $f2);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result_array();
        }
    }

    //Save Monitor Daily
    function saved_day($f1, $f2){
        $this->db->select('*');
        $this->db->from('konsumsi');
        $this->db->where($f1);
        $this->db->where($f2);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result_array();
        }
    }

    //Saved All Monitor
    function saved_all($f1){
        $this->db->select('*');
        $this->db->from('konsumsi');
        $this->db->where($f1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result_array();
        }
    }

    //Fetching data Interest
    function infetch($f1, $f2 = FALSE, $f3 = FALSE){
        $this->db->from($f1);
        if($f2 != FALSE){
            $this->db->where($f2);
            $this->db->where($f3);
        }
        $query = $this->db->get();
        if($query !== FALSE && $query->num_rows() > 0){
            $data = array();
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
        }else{
            $data = FALSE;
        }
        return $data;
    }

    //Add Interest
    function addinter($f1){
        $this->db->insert('kesukaan_pengguna', $f1);
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    //Delete Interest
    function delinter($f1){
        $this->db->where($f1);
        $this->db->delete('kesukaan_pengguna');
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    //Comma deleter
    function duocimal($number, $precision = 2) {
        // if Zero causes issues, and no need to truncate number
        if(0 == (int)$number) {
            return $number;
        }
        // is are negative number?
        $negative = $number / abs($number);
        // Cast the number to a positive to solve rounding
        $number = abs($number);
        // Calculate precision number for dividing / multiplying
        $precision = pow(10, $precision);
        // Run the math, re-applying the negative value to ensure returns correctly negative / positive
        return floor( $number * $precision ) / $precision * $negative;
    }

    //Divide float
    function divflo($a, $b, $pre = 3) {
        $a *= pow(10, $pre);
        $result = (int)($a / $b);
        if (strlen($result)==$pre) return '0.' . $result;
        else return preg_replace('/(\d{' . $pre . '})$/', '.\1', $result);
    }

    //splitting fullname into firstname and lastname
    function splitname($f1) {
        $name = trim($f1);
        $last_name = (strpos($f1, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $f1);
        $first_name = trim( preg_replace('#'.$last_name.'#', '', $f1 ) );
        return array($first_name, $last_name);
    }

    //Check email
    function chkmail($f1){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where($f1);
        $this->db->where('level', '2');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        }else{
            return $query->result();
        }
    }

    //Save Coordinates user
    function savcoor($f1, $f2, $f3){
        $hit = array('latitude' => $f2, 'longitude' => $f3);
        $this->db->where('token', $f1);
        $this->db->update('log_token', $hit);
    }

    function myvoucerlist($f1){
        $q = $this->db->from('user_voucer')->where($f1)->get();
        if ($q->num_rows() == 0) {
            return FALSE;
        }else{
            foreach($q->result() as $hit){
                $get = $this->db->from('voucer')->where('id', $hit->id_voucer)->where("expired >=", date("Y-m-d"))->get();
                if ($get->num_rows() > 0) {
                    foreach($get->result_array() as $lst){
                        $lst['foto'] = base_url()."asset/vopic/".$lst['foto'];
                        $got[] = $lst;
                    }
                }else{
                    return FALSE;
                }
            }
            return $got;
        }
    }

    function addvoc($f1, $f2){
        $q = $this->db->from('voucer')->where('kode', $f1)->where("expired >=", date("Y-m-d"))->get();
        if ($q->num_rows() == 0) {
            return FALSE;
        }else{
            foreach($q->result() as $hit){
                $idvoc = $hit->id;
                $get = $this->db->from('user_voucer')->where('id_voucer', $idvoc)->where('mail_user', $f2)->get();
                if ($get->num_rows() == 0) {
                    $this->db->insert('user_voucer', array('id_voucer' => $idvoc, 'mail_user' => $f2));
                    $query = $this->db->affected_rows();
                    if ($query == 0) {
                        return FALSE;
                    }else{
                        return $query;
                    }
                }else{
                    return FALSE;
                }
            }
        }
    }

    //add voucer alternative multi voucer get
    function addvocalt($f1, $f2){
        $q = $this->db->from('voucer')->where('kode', $f1)->where("expired >=", date("Y-m-d"))->get();
        if ($q->num_rows() == 0) {
            return FALSE;
        }else{
            foreach($q->result() as $hit){
                $query = $this->db->insert('user_voucer', array('id_voucer' => $hit->id_voucer, 'mail_user' => $f2))->affected_rows();
                if ($query == 0) {
                    return FALSE;
                }else{
                    return $query;
                }
            }
        }
    }

    //Add point User
    function addpnt($f1, $f2){
        $this->db->where('surel', $f1);
        $this->db->set("poin", "poin+$f2", false);
        $this->db->set("progress", "progress+$f2", false);
        $this->db->update("user");
        $query = $this->db->affected_rows();
        if ($query == 0) {
            return FALSE;
        }else{
            return $query;
        }
    }

    //Add consumption
    function addcom($f1, $f2){
        $date = date('Y-m-d');
        $this->load->model("loginm");
        $chk = $this->loginm->chkpne($f1, $date);

        if($chk != FALSE){
            $this->db->where('surel', $f1);
            $this->db->where('tgl', $date);
            $this->db->set("harian", "harian+$f2", false);
            $this->db->update("konsumsi");
            $query = $this->db->affected_rows();
            if ($query == 0) {
                return FALSE;
            }else{
                return $query;
            }
        }else{
            $array = array('surel' => $f1, 'tgl' => $date, 'harian' => $f2);
            $this->db->insert('konsumsi', $array);
            $query = $this->db->affected_rows();
            if ($query == 0) {
                return FALSE;
            }else{
                return $query;
            }
        }
    }

}
//End Of File -> Made By: DzEN/DzEN <-