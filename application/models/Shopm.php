<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shopm extends CI_Model{

    function info($f1, $f2){
        $this->load->model('waterm');
        $this->db->from("jual");
        $this->db->where("id", $f1);
        $query = $this->db->get();
        if($query !== FALSE && $query->num_rows() != 0){
            $data = array();
            $got = array();
            foreach ($query->result_array() as $row) {
                $get = $this->db->from('jual_img')->where('id_jual', $f1)->get();
                if($get != FALSE && $get->num_rows() != 0){
                    foreach($get->result() as $hit){
                        $got[] = base_url()."asset/sdpic/".$hit->foto;
                    }
                }
                $data[] = array_merge($row, array('galeri' => $got));
            }
        }else{
            $data = NULL;
        }
        return $data;
    }
}