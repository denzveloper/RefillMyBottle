<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/root.png');?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('assets/img/root.png');?>">
    <?php if(!empty($sh)){foreach($sh as $hit){  ?>
	<title><?php echo $hit['nama'];?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<!--Custom fonts. To improve load times, remove this and update the h1 styling in style.css--> 
	<link href='http://fonts.googleapis.com/css?family=Lato:300,700' rel='stylesheet' type='text/css'>
	<!-- stylesheets -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>" type="text/css" />
	
	<!-- just in case viewer is using Internet Explorer -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    <style>
        header{
            background-image: url(<?php echo base_url()."asset/sopic/".$hit['foto']; ?>);
        }
    </style>


</head>

<body>
	
	<!-- Header-->
	<header>
		<h1 style="background-color: rgba(0,0,0,0.3)"><?php echo $hit['nama'];?></h1>
		<p>Rp. <?php echo $hit['harga'];?> / Rp. <?php echo $hit['harga_poin'];?></p>
	</header>
	
	<div class="container">
		
		<!-- Introduction-->
		<section class="center">
			<h1><?php echo $hit['nama'];?></h1>
			<p><?php echo $hit['detail'];?></p>
		</section>
		
		<hr>
		
		<!-- Gallery Block-->
		<section class="gallery">
			<h1 class="center">Gambar Produk</h1>
			<div class="row center">
                <?php if(!empty($hit['galeri'])){foreach($hit['galeri'] as $got){ ?>
                    <a href="<?php echo $got; ?>"><img src="<?php echo $got; ?>" class="grid-3"></a>
                <?php }}else{ echo "<i><sub>NO IMAGE</sub></i>"; } ?>
			</div>
		</section>

        <hr>

        <section class="center">
            <h1 class="center">Information</h1>
            <p>Harga Normal: <?php echo $hit['harga'];?></p>
			<p>Poin diskon: <?php echo $hit['ambil_poin'];?> / Rp. <?php echo $hit['harga_poin'];?></p>
            <p>Tersedia hingga: <?php echo date("D, d M Y", strtotime($hit['exp'])); ?>!</p>
		</section>

        <hr>

        <section class="center">
            <h1 class="center">Cara Penggunaan</h1>
            <p><?php echo $hit['cara_guna'];?></p>
        </section>

        <hr>

        <section class="center">
            <h1 class="center">Syarat dan ketentuan</h1>
            <p><?php echo $hit['tnc'];?></p>
        </section>

        <hr>

        <section class="center">
            <h1 class="center"><a href="<?php echo $hit['address'];?>">Beli Disini!</a></h1>
        </section>
		
		<!--Footer-->
		<footer class="center">
			<hr>
			<span>&copy; RefillMyBottle&nbsp;&middot;&nbsp;DENZVELOPER DzEN/DzEN</span>
		</footer>
		
	</div><!-- End Container -->

</body>

    <?php }}else{ "<h2>Not Found</h2><p style='color: #fff'>TWFkZSBCeTogREVOWlZFTE9QRVIgRHpFTi9EekVOIOOAjERFTkRZIE9DVEFWSUFO44CN</p>"; } ?>
</html>