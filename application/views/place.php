<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/root.png');?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('assets/img/root.png');?>">
    <?php if(!empty($pl)){foreach($pl as $hit){  ?>
	<title><?php echo $hit['nama'];?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<!--Custom fonts. To improve load times, remove this and update the h1 styling in style.css--> 
	<link href='http://fonts.googleapis.com/css?family=Lato:300,700' rel='stylesheet' type='text/css'>
	<!-- stylesheets -->
	<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>" type="text/css" />
	
	<!-- just in case viewer is using Internet Explorer -->
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    <style>
        header{
            background-image: url(<?php echo base_url()."asset/plpic/".$hit['foto']; ?>);
        }
    </style>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>


</head>

<body>
	
	<!-- Header-->
	<header>
		<h1 style="background-color: rgba(0,0,0,0.3)"><?php echo $hit['nama'];?></h1>
		<p><?php echo $hit['alamat'];?></p>
	</header>
	
	<div class="container">
		
		<!-- Introduction-->
		<section class="center">
			<h1><?php echo $hit['nama'];?></h1>
			<p><?php echo $hit['detail'];?></p>
		</section>
		
		<hr>
		
		<!-- Gallery Block-->
		<section class="gallery">
			<h1 class="center">Gallery</h1>
			<div class="row center">
                <?php if(!empty($hit['galeri'])){foreach($hit['galeri'] as $got){ ?>
                    <a href="<?php echo $got; ?>"><img src="<?php echo $got; ?>" class="grid-3"></a>
                <?php }}else{ echo "<i><sub>NO IMAGE</sub></i>"; } ?>
			</div>
		</section>

        <hr>

        <section class="center">
            <h1 class="center">The Way to Arrive</h1>
            <div id="map_canvas" style="height: 300px;margin: 0.6em;" class="form-control border-input" ></div>
            <h3><a href="https://www.google.com/maps/search/<?php echo $hit['latitude'].','.$hit['longitude']?>" target="blank"><?php echo $hit['latitude'].', '.$hit['longitude']; ?></a></h2>
        </section>
        <hr>

        <section class="center">
            <h1 class="center">Information</h1>
			<p>Open: <?php echo $hit['jam_buka'];?> / Close: <?php echo $hit['jam_tutup'];?></p>
            <p>Now Is: <?php echo $hit['opcl']; ?>!</p>
			<p>Lat: <?php echo $hit['latitude'];?>, Lon: <?php echo $hit['longitude'];?></p>
		</section>
		
		<!--Footer-->
		<footer class="center">
			<hr>
			<span>&copy; RefillMyBottle&nbsp;&middot;&nbsp;DENZVELOPER DzEN/DzEN</span>
		</footer>
		
	</div><!-- End Container -->

</body>
    <script>
    $(function (){
        var lat = <?php echo $hit['latitude']; ?>,
            lng = <?php echo $hit['longitude']; ?>,
            latlng = new google.maps.LatLng(lat, lng),
            image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';
        //zoomControl: true,
        //zoomControlOptions: google.maps.ZoomControlStyle.LARGE,
        var mapOptions={
            center: new google.maps.LatLng(lat, lng),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            panControl: true,
            panControlOptions:{
                position: google.maps.ControlPosition.TOP_RIGHT
            },
            zoomControl: true,
            zoomControlOptions:{
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.TOP_left
            }
        },
        map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions),
            marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: image
            });
    });
    </script>

    <?php }}else{"<h2>Not Found</h2><p style='color: #fff'>TWFkZSBCeTogREVOWlZFTE9QRVIgRHpFTi9EekVOIOOAjERFTkRZIE9DVEFWSUFO44CN</p>";} ?>
</html>