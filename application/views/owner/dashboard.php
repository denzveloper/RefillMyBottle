<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/root.png');?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('assets/img/root.png');?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Dashboard Of <?php echo $this->session->userdata("fnam");?></title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url('assets/css/animate.min.css'); ?>" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="<?php echo base_url('assets/css/paper-dashboard.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url('assets/css/demo.css" rel="stylesheet'); ?>" />

    <!--  Fonts and icons     -->
    <link href='<?php echo base_url('assets/fa/css/all.min.css');?>' rel='stylesheet' type='text/css'> 
    <link href='<?php echo base_url('assets/fonts/muli/font.css');?>' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url('assets/css/themify-icons.css'); ?>" rel="stylesheet">

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="info">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <img class="img-responsive img-circle center-block" src="<?php echo base_url('assets/img/logo.png'); ?>" alt="..."/>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="<?php echo base_url('index.php/dashboard');?>">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/place');?>">
                        <i class="ti-map-alt"></i>
                        <p>Atur Tempat</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/event');?>">
                        <i class="ti-calendar"></i>
                        <p>Event</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/voucer');?>">
                        <i class="ti-ticket"></i>
                        <p>Voucer</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/shop');?>">
                        <i class="ti-bag"></i>
                        <p>Shop</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/profil');?>">
                        <i class="ti-user"></i>
                        <p>Profil</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/passwd');?>">
                        <i class="ti-lock"></i>
                        <p>Sandi</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">Dashboard</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                        <a href="<?php echo base_url('index.php/dashboard/profil');?>" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-user"></i>
								<p><?php echo $this->variabel->nama();?></p>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-header">Pengaturan Akun</li>
                                <li><a href="<?php echo base_url('index.php/dashboard/profil');?>">Pengguna</a></li>
                                <li><a href="<?php echo base_url('index.php/dashboard/passwd');?>">Sandi</a></li>
                                <li role="separator" class="divider"></li>
                                <li class="dropdown-header">Log Keluar</li>
                                <li><a href="<?php echo base_url('index.php/login/logout');?>">Keluar Sesi</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="text-center">
                                            <img src="<?php echo $_SESSION['foto']; ?>" class="avatar img-responsive" alt="...">
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Nama Anda</p>
                                            <?php echo $this->variabel->nama();
                                            ?>
                                            <p class="category"><?php echo $this->variabel->mail(); ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <a href="<?php echo base_url('index.php/dashboard/profil');?>"><i class="ti-user"></i> Edit Profil</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="text-center">
                                            <img src="<?php echo $photpt; ?>"  class="avatar img-responsive img-rounded" alt="...">
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Nama Tempat</p>
                                            <?php echo $namtpt; ?>
                                            <p class="category"><?php echo $alatpt; ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <a href="<?php echo base_url('index.php/dashboard/place');?>"><i class="ti-map"></i> Edit Tempat</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Tambah poin</h4>
                                <p class="category">Tambah Poin Pengguna</p>
                            </div>
                            <div class="content">
                                <?php if($this->loginm->placeid()!=null){ ?>
                                <form action="<?php echo base_url('index.php/update/addpoin'); ?>" method="POST">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Pos-el</label>
                                            <select name="user"  class="form-control border-input">
                                                <?php if(!empty($user_list)){ ?>
                                                    <option selected="selected" disabled>Select User</option>
                                                <?php foreach($user_list as $hit){ ?>
                                                    <option value="<?php echo $hit['id']; ?>"><?php echo $hit['id'].' - '.$hit['name'].' ('.$hit['point'].')'; ?></option>
                                                <?php }}else{ ?> <option selected="selected" value="Empty" disabled>No Nearlby User</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Transaksi</label>
                                            <input name="poin" type='number' min="1" max="99" value='1' class="form-control border-input" onkeypress="return hanyaAngka(event)" required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10">
                                    &nbsp;
                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Tambah</button>
                                    </div>
                                </div>
                                </form>
                                <?php }else{ echo "<b>No Available!</b>"; } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Tukar Voucher</h4>
                                <p class="category">Tukar Voucher Pengguna</p>
                            </div>
                            <div class="content">
                                <?php if($this->loginm->placeid()!=null){ ?>
                                <form action="<?php echo base_url('index.php/update/redmvoc'); ?>" method="POST">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Pos-el</label>
                                            <select name="uservoc" class="form-control border-input uservoc">
                                                <?php if(!empty($user_list)){ ?>
                                                    <option selected="selected" disabled>Select User</option>
                                                <?php foreach($user_list as $hit){ ?>
                                                    <option value="<?php echo $hit['id']; ?>"><?php echo $hit['id'].' - '.$hit['name'].' ('.$hit['point'].')'; ?></option>
                                                <?php }}else{ ?> <option selected="selected" value="Empty" disabled>No Nearlby User</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Voucer</label>
                                            <select name="voucer" class="form-control border-input voucer">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10">
                                    &nbsp;
                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Ambil</button>
                                    </div>
                                </div>
                                </form>
                                <?php }else{ echo "<b>No Available!</b>"; } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="#">DENZVELOPER DzEN/DzEN</a>
                </div>
            </div>
        </footer>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="<?php echo base_url('assets/js/jquery-1.10.2.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="<?php echo base_url('assets/js/paper-dashboard.js'); ?>"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url('assets/js/bootstrap-notify.js'); ?>"></script>

    <?php if($this->session->flashdata('info')){ foreach($this->session->flashdata('info') as $row) {?>
    <script type="text/javascript">
    	$(document).ready(function(){

        	$.notify({
            	icon: '<?php echo $row['ico']; ?>',
            	message: "<?php echo $row['txt']; ?>"

            },{
                type: '<?php echo $row['typ']; ?>',
                timer: 3000
            });

    	});
	</script>
    <?php } } ?>
    <script>
    $('.uservoc').change(function() {
        var user = $(this).val(); //get the current value's option
        //alert(user);
                
        $.ajax({
            type:'POST',
            url:'<?php echo base_url("index.php/update/volis");?>',
            data:{'id':user},
            success:function(data){
                //in here, for simplicity, you can substitue the HTML for a brand new select box for countries
                //1.
                $(".voucer").html(data);

                //2.
                // iterate through objects and build HTML here
                //alert(user);
            }
        });
    });
    </script>
    <script>
        function hanyaAngka(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57)){
                    return false;
                }
            return true;
        }
    </script>

</html>