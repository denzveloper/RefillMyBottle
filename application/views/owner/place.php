<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/root.png');?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('assets/img/root.png');?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Edit Place Info</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url('assets/css/animate.min.css'); ?>" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="<?php echo base_url('assets/css/paper-dashboard.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url('assets/css/demo.css" rel="stylesheet'); ?>" />

    <!--  Fonts and icons     -->
    <link href='<?php echo base_url('assets/fa/css/all.min.css');?>' rel='stylesheet' type='text/css'>    
    <link href='<?php echo base_url('assets/fonts/muli/font.css');?>' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url('assets/css/themify-icons.css'); ?>" rel="stylesheet">

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="info">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <img class="img-responsive center-block" src="<?php echo base_url('assets/img/logo.png'); ?>" alt="..."/>
            </div>

            <ul class="nav">
                <li>
                    <a href="<?php echo base_url('index.php/dashboard'); ?>">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="active">
                    <a href="<?php echo base_url('index.php/dashboard/place');?>">
                        <i class="ti-map-alt"></i>
                        <p>Atur Tempat</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/event');?>">
                        <i class="ti-calendar"></i>
                        <p>Event</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/voucer');?>">
                        <i class="ti-ticket"></i>
                        <p>Voucer</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/shop');?>">
                        <i class="ti-bag"></i>
                        <p>Shop</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/profil');?>">
                        <i class="ti-user"></i>
                        <p>Profil</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/passwd');?>">
                        <i class="ti-lock"></i>
                        <p>Sandi</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">Manajer Tempat</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?php echo base_url('index.php/dashboard/profil');?>" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-user"></i>
								<p><?php echo $this->variabel->nama();?></p>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-header">Pengaturan Akun</li>
                                <li><a href="<?php echo base_url('index.php/dashboard/profil');?>">Pengguna</a></li>
                                <li><a href="<?php echo base_url('index.php/dashboard/passwd');?>">Sandi</a></li>
                                <li role="separator" class="divider"></li>
                                <li class="dropdown-header">Log Keluar</li>
                                <li><a href="<?php echo base_url('index.php/login/logout');?>">Keluar Sesi</a></li>
                            </ul>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>


        <div class="content">
        <?php if($this->loginm->placeid()!=null){if(!empty($place)){foreach($place as $hit){  ?>
        <form action="<?php echo base_url('index.php/update/updplc');?>" method="POST" enctype="multipart/form-data">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4 col-md-5">
                        <div class="card card-user">
                            <img src="<?php echo base_url()."asset/plpic/".$hit['foto'];?>" alt="..." id="foto" class="img-responsive img-rounded center-block"/>
                            <div class="content text-center">
                                    <h2 class="title"><?php if($hit['premium']=='y') echo "<i class='ti-medall'></i>";else echo "<i class='ti-location-pin'></i>"; ?></h2>
                                    <h4 class="title"><a href="https://www.google.com/maps/search/<?php echo $hit['nama']." ".$hit['alamat']; ?>" target="_blank"><?php echo $hit['nama']; ?></a><br />
                                        <small><?php echo $hit['detail']; ?></small>
                                    </h4>
                                    <p class="description text-center">
                                    <?php echo $hit['alamat']; ?><br>
                                    <a href="https://www.google.com/maps/search/<?php echo $hit['latitude'].','.$hit['longitude']?>" target="_blank"><small><?php echo "<i class='ti-map-alt'></i> ".$hit['latitude'].','.$hit['longitude']; ?></small></a><br/>
                                    </p>
                            </div>
                            <div class="footer">
                                <hr />
                                <p>Ubah Foto Profil</p>
                                <input type="file" name="foto" class="form-control border-input" accept="image/gif,image/jpeg,image/png" onchange="readURL(this);">
                            </div>
                        </div>
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Other Images</h4>
                            </div>
                            <div class="content">
                                <ul class="list-unstyled team-members">
                                    <?php if(!empty($images)){ $x = 1; foreach($images as $img){ ?>
                                    <li>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <img src="<?php echo $img['images'];?>" alt="..." class="img-responsive img-rounded center-block">
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <a href="<?php echo $img['link']."&todo=primary";?>"><button class="btn btn-sm btn-success btn-icon"><i class="fa fa-check"></i></button></a>
                                                <a href="<?php echo $img['link']."&todo=delete";?>"><button class="btn btn-sm btn-danger btn-icon"><i class="fa fa-trash"></i></button></a>
                                            </div>
                                        </div>
                                    </li>
                                    <?php }}else{ echo "<li><div class='row'><div class='col-xs-12'><h4>EMPTY</h4></div></div></li>"; } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Tempat</h4>
                            </div>
                            <div class="content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label><i class="ti-home"></i> Nama Tempat</label>
                                                <input type="text" name="nama" value="<?php echo $hit['nama'];?>" class="form-control border-input" placeholder="Last Name" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label><i class="ti-direction-alt"></i> Alamat</label>
                                                <textarea rows="4" class="form-control border-input" placeholder="Ketik Alamat Lengkap Disini" name="alamat" required><?php echo $hit['alamat'];?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label><i class="ti-comment-alt"></i> Deskripsi</label>
                                                <textarea rows="4" class="form-control border-input" placeholder="Isi Deskripsi tentang Tempat Anda" name="deskripsi" required><?php echo $hit['detail'];?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><i class="ti-cup"></i> Poin</label>
                                                <input type="text" name="poin" value="<?php echo $hit['poin'];?>" class="form-control border-input" placeholder="Poin" onkeypress="return hanyaAngka(event)" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><i class="ti-wallet"></i> Harga Rata-rata</label>
                                                <input type="text" name="harga" value="<?php echo $hit['harga'];?>" class="form-control border-input" placeholder="Rupiah" onkeypress="return hanyaAngka(event)" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><i class="ti-time"></i> jam Buka</label>
                                                <input type='time' name="jam_b" value="<?php echo date("H:i", strtotime($hit['jam_buka']));?>" class="form-control border-input" placeholder="JJ:MM" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><i class="ti-time"></i> Jam Tutup</label>
                                                <input type='time' name="jam_t" value="<?php echo date("H:i", strtotime($hit['jam_tutup']));?>" class="form-control border-input" placeholder="JJ:MM" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label><i class="ti-medall-alt"></i> Toko Premium</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="radio" name="premium" value="y" <?php if($hit['premium']=='y')echo "checked";?>><i class="ti-medall"></i> Premium
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="radio" name="premium" value="n" <?php if($hit['premium']=='n')echo "checked";?>><i class="ti-location-pin"></i> Non-Premium
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label><i class="ti-map"></i> Lokasi</label>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="hidden" name="lat" class="MapLat form-control border-input" value="<?php echo $lat = $hit['latitude'];?>" readonly style="cursor: not-allowed;"> 
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="hidden" name="lon" class="MapLon form-control border-input" value="<?php echo $lon = $hit['longitude'];?>" readonly style="cursor: not-allowed;">
                                                </div>
                                            </div>
                                            <div id="map_canvas" style="height: 300px;margin: 0.6em;" class="form-control border-input" ></div>
                                        </div>
                                    </div>
                                    <?php }} ?>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label><i class="ti-layout-grid2-alt"></i> Tipe Tempat</label>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <?php if(!empty($type)){foreach($type as $hit){ ?>
                                                        <input type="checkbox" name="type[]" value="<?php echo $hit['nama']; ?>" checked><?php echo ucfirst($hit['nama']); ?>
                                                        &nbsp;
                                                    <?php }} ?>
                                                    <?php if(!empty($type0)){foreach($type0 as $hit){ ?>
                                                        <input type="checkbox" name="type[]" value="<?php echo $hit['nama']; ?>"><?php echo $hit['nama']; ?> 
                                                        &nbsp;
                                                    <?php }} ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Sandi</label>
                                                <input type="password" name="pass" class="form-control border-input" placeholder="Sandi Sekarang" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Perbarui</button>
                                        &nbsp;&middot;&nbsp;
                                        <button type="reset" class="btn btn-info btn-fill btn-wd">Atur Ulang</button>
                                    </div>
                                    <?php }else{echo "<h1>You Dont have Place</h1><p>Please Contact Administrator!</p>";}?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="#">DENZVELOPER DzEN/DzEN</a>
                </div>
            </div>
        </footer>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="<?php echo base_url('assets/js/jquery-1.10.2.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="<?php echo base_url('assets/js/paper-dashboard.js'); ?>"></script>
    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url('assets/js/bootstrap-notify.js'); ?>"></script>


    <?php if($this->session->flashdata('info')){ foreach($this->session->flashdata('info') as $row) {?>
    <script type="text/javascript">
    	$(document).ready(function(){

        	$.notify({
            	icon: '<?php echo $row['ico']; ?>',
            	message: "<?php echo $row['txt']; ?>"

            },{
                type: '<?php echo $row['typ']; ?>',
                timer: 3000
            });

    	});
	</script>
    <?php } } ?>

    <script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#foto')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);

        }
        $(document).ready(function(){

        $.notify({
            icon: 'ti-info',
            message: "<strong>To save image:</strong><br>Click Update button."

        },{
            type: 'info',
            timer: 3000
        });

        });
    }
    </script>

    <script>
    $(function (){
        var lat = <?php echo $lat; ?>,
            lng = <?php echo $lon; ?>,
            latlng = new google.maps.LatLng(lat, lng),
            image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png';
        //zoomControl: true,
        //zoomControlOptions: google.maps.ZoomControlStyle.LARGE,
        var mapOptions={
            center: new google.maps.LatLng(lat, lng),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            panControl: true,
            panControlOptions:{
                position: google.maps.ControlPosition.TOP_RIGHT
            },
            zoomControl: true,
            zoomControlOptions:{
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.TOP_left
            }
        },
        map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions),
            marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: image
            });

        var input = document.getElementById('searchTextField');
        var autocomplete = new google.maps.places.Autocomplete(input, {
            types: ["geocode"]
        });
        autocomplete.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(autocomplete, 'place_changed', function (event) {
            infowindow.close();
            var place = autocomplete.getPlace();
            if(place.geometry.viewport){
                map.fitBounds(place.geometry.viewport);
            }else{
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            moveMarker(place.name, place.geometry.location);
            $('.MapLat').val(place.geometry.location.lat());
            $('.MapLon').val(place.geometry.location.lng());
        });
        google.maps.event.addListener(map, 'click', function (event){
            $('.MapLat').val(event.latLng.lat());
            $('.MapLon').val(event.latLng.lng());
            placeMarker(event.latLng);

            infowindow.close();
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                "latLng":event.latLng
            }, function (results, status){
                console.log(results, status);
                if(status == google.maps.GeocoderStatus.OK){
                    console.log(results);
                    document.write(results[0].formatted_address);
                    var lat = results[0].geometry.location.lat(),
                        lng = results[0].geometry.location.lng(),
                        adr = results[0].address_components,
                        placeName = results[0].address_components[0].long_name,
                        latlng = new google.maps.LatLng(lat, lng);
                    moveMarker(placeName, latlng);
                }
            });
        });

        function placeMarker(location){
            marker.setIcon(image);
            marker.setPosition(location);
        }

        function moveMarker(placeName, latlng){
            marker.setIcon(image);
            marker.setPosition(latlng);
            infowindow.setContent(placeName);
            //infowindow.open(map, marker);
        }
    });

    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)){
                return false;
            }
        return true;
    }
    </script>

</html>