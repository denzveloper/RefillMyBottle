<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/root.png');?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('assets/img/root.png');?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Profile Of <?php echo $this->session->userdata("fnam");?></title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url('assets/css/animate.min.css'); ?>" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="<?php echo base_url('assets/css/paper-dashboard.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url('assets/css/demo.css" rel="stylesheet'); ?>" />

    <!--  Fonts and icons     -->
    <link href='<?php echo base_url('assets/fa/css/all.min.css');?>' rel='stylesheet' type='text/css'>    
    <link href='<?php echo base_url('assets/fonts/muli/font.css');?>' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url('assets/css/themify-icons.css'); ?>" rel="stylesheet">

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="info">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <img class="img-responsive img-circle center-block" src="<?php echo base_url('assets/img/logo.png'); ?>" alt="..."/>
            </div>

            <ul class="nav">
                <li>
                    <a href="<?php echo base_url('index.php/dashboard');?>">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/adpl');?>">
                        <i class="ti-map-alt"></i>
                        <p>Tempat Baru</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/npu');?>">
                        <i class="ti-map"></i>
                        <p>Saran Tempat</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/addusr');?>">
                        <i class="fa fa-users"></i>
                        <p>Tambah User</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/mngplc');?>">
                        <i class="fa fa-map"></i>
                        <p>Manajer Tempat</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/mngusr');?>">
                        <i class="fa fa-user-circle"></i>
                        <p>Manajer User</p>
                    </a>
                </li>
                <li class="active">
                    <a href="<?php echo base_url('index.php/dashboard/profil');?>">
                        <i class="ti-user"></i>
                        <p>Profil</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/passwd');?>">
                        <i class="ti-lock"></i>
                        <p>Sandi</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">Profil Pengguna</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                        <a href="<?php echo base_url('index.php/dashboard/profil');?>" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-user"></i>
								<p><?php echo $this->variabel->nama();?></p>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-header">Pengaturan Akun</li>
                                <li><a href="<?php echo base_url('index.php/dashboard/profil');?>">Pengguna</a></li>
                                <li><a href="<?php echo base_url('index.php/dashboard/passwd');?>">Sandi</a></li>
                                <li role="separator" class="divider"></li>
                                <li class="dropdown-header">Log Keluar</li>
                                <li><a href="<?php echo base_url('index.php/login/logout');?>">Keluar Sesi</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="content">
        <?php if(!empty($user)){foreach($user as $hit){ ?>
        <form action="<?php echo base_url('index.php/update/updpro');?>" method="POST" enctype="multipart/form-data">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4 col-md-5">
                        <div class="card card-user">
                            <img src="<?php echo $_SESSION['foto']; ?>" alt="..." id="foto" class="img-responsive img-rounded center-block"/>
                            <div class="content text-center">
                                    <h4 class="title"><?php echo $this->variabel->nama(); ?><br />
                                        <a href="#"><small><?php echo $_SESSION['mail']; ?></small></a>
                                    </h4>
                                    <p class="description text-center">
                                    <?php if(isset($namtpt)) echo "Owner of ".$namtpt; ?><br>
                                    <?php if(isset($alatpt)) echo $alatpt; ?></p>
                                    </p>
                            </div>
                            <div class="footer">
                                <hr />
                                <p>Ubah Foto Profil</p>
                                <input type="file" name="foto" class="form-control border-input" accept="image/gif,image/jpeg,image/png" onchange="readURL(this);">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Profil</h4>
                            </div>
                            <div class="content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Alamat Surel</label>
                                                <input name="email" value="<?php echo $hit['surel'];?>" type="email" class="form-control border-input" placeholder="Email" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nama Awal</label>
                                                <input name="nd" value="<?php echo $hit['namadepan'];?>" type="text" class="form-control border-input" placeholder="First Name" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nama Akhir</label>
                                                <input name="nb" type="text" value="<?php echo $hit['namabelakang'];?>" class="form-control border-input" placeholder="Last Name" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Sandi</label>
                                                <input type="password" name="pass" class="form-control border-input" placeholder="Sandi Sekarang" required>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Perbarui</button>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php }} ?>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="#">DENZVELOPER DzEN/DzEN</a>
                </div>
            </div>
        </footer>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="<?php echo base_url('assets/js/jquery-1.10.2.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="<?php echo base_url('assets/js/paper-dashboard.js'); ?>"></script>
    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url('assets/js/bootstrap-notify.js'); ?>"></script>


    <?php if($this->session->flashdata('info')){ foreach($this->session->flashdata('info') as $row) {?>
    <script type="text/javascript">
    	$(document).ready(function(){

        	$.notify({
            	icon: '<?php echo $row['ico']; ?>',
            	message: "<?php echo $row['txt']; ?>"

            },{
                type: '<?php echo $row['typ']; ?>',
                timer: 3000
            });

    	});
	</script>
    <?php } } ?>

    <script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#foto')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);

        }
        $(document).ready(function(){

        $.notify({
            icon: 'ti-info',
            message: "<strong>To save image:</strong><br>type Password and click Update."

        },{
            type: 'info',
            timer: 3000
        });

        });
    }
    </script>

</html>