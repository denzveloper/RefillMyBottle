<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/root.png');?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('assets/img/root.png');?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Add New Place</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url('assets/css/animate.min.css'); ?>" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="<?php echo base_url('assets/css/paper-dashboard.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url('assets/css/demo.css" rel="stylesheet'); ?>" />

    <!--  Fonts and icons     -->
    <link href='<?php echo base_url('assets/fa/css/all.min.css');?>' rel='stylesheet' type='text/css'>    
    <link href='<?php echo base_url('assets/fonts/muli/font.css');?>' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url('assets/css/themify-icons.css'); ?>" rel="stylesheet">

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="info">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <img class="img-responsive center-block" src="<?php echo base_url('assets/img/logo.png'); ?>" alt="..."/>
            </div>

            <ul class="nav">
                <li>
                    <a href="<?php echo base_url('index.php/dashboard');?>">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="active">
                    <a href="<?php echo base_url('index.php/dashboard/adpl');?>">
                        <i class="ti-map-alt"></i>
                        <p>Tempat Baru</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/npu');?>">
                        <i class="ti-map"></i>
                        <p>Saran Tempat</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/addusr');?>">
                        <i class="fa fa-users"></i>
                        <p>Tambah User</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/mngplc');?>">
                        <i class="fa fa-map"></i>
                        <p>Manajer Tempat</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/mngusr');?>">
                        <i class="fa fa-user-circle"></i>
                        <p>Manajer User</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/profil');?>">
                        <i class="ti-user"></i>
                        <p>Profil</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/passwd');?>">
                        <i class="ti-lock"></i>
                        <p>Sandi</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">Tempat Baru</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                        <a href="<?php echo base_url('index.php/dashboard/profil');?>" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-user"></i>
								<p><?php echo $this->variabel->nama();?></p>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-header">Pengaturan Akun</li>
                                <li><a href="<?php echo base_url('index.php/dashboard/profil');?>">Pengguna</a></li>
                                <li><a href="<?php echo base_url('index.php/dashboard/passwd');?>">Sandi</a></li>
                                <li role="separator" class="divider"></li>
                                <li class="dropdown-header">Log Keluar</li>
                                <li><a href="<?php echo base_url('index.php/login/logout');?>">Keluar Sesi</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
        <form action="<?php echo base_url('index.php/update/addpln');?>" method="POST" enctype="multipart/form-data">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-7">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Tempat</h4>
                            </div>
                            <div class="content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label><i class="ti-home"></i> Nama Tempat</label>
                                                <input type="text" name="nama" class="form-control border-input" placeholder="Place Name" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label><i class="ti-direction-alt"></i> Alamat</label>
                                                <textarea rows="4" class="form-control border-input" placeholder="Ketik Alamat Lengkap Disini" name="alamat" required></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <label><span title="Zum peta untuk Lokasi Tepat" style="text-decoration: underline;"><i class="ti-map"></i> Lokasi</span></label>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="hidden" name="lat" class="MapLat form-control border-input" readonly style="cursor: not-allowed;" require> 
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="hidden" name="lon" class="MapLon form-control border-input" readonly style="cursor: not-allowed;" require>
                                                </div>
                                            </div>
                                            <div id="map_canvas" style="height: 300px;margin: 0.6em;" class="form-control border-input" ></div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Tambah Tempat</button>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="#">DENZVELOPER DzEN/DzEN</a>
                </div>
            </div>
        </footer>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="<?php echo base_url('assets/js/jquery-1.10.2.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="<?php echo base_url('assets/js/paper-dashboard.js'); ?>"></script>
    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url('assets/js/bootstrap-notify.js'); ?>"></script>

    <script type="text/javascript">
    	$(document).ready(function(){

        	$.notify({
            	icon: 'ti-info',
            	message: "<b>Info: </b><br><i>Zoom map for a more precise location!</i>"

            },{
                type: 'info',
                timer: 2000
            });

    	});
	</script>
    <?php if($this->session->flashdata('info')){ foreach($this->session->flashdata('info') as $row) {?>
    <script type="text/javascript">
    	$(document).ready(function(){

        	$.notify({
            	icon: '<?php echo $row['ico']; ?>',
            	message: "<?php echo $row['txt']; ?>"

            },{
                type: '<?php echo $row['typ']; ?>',
                timer: 3000
            });

    	});
	</script>
    <?php } } ?>

    <script>
    $(function (){
        var lat = -6.176130,
            lng =  106.826594,
            latlng = new google.maps.LatLng(lat, lng),
            image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png';
        //zoomControl: true,
        //zoomControlOptions: google.maps.ZoomControlStyle.LARGE,
        var mapOptions={
            center: new google.maps.LatLng(lat, lng),
            zoom: 5,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            panControl: true,
            panControlOptions:{
                position: google.maps.ControlPosition.TOP_RIGHT
            },
            zoomControl: true,
            zoomControlOptions:{
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.TOP_left
            }
        },
        map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions),
            marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: image
            });

        var input = document.getElementById('searchTextField');
        var autocomplete = new google.maps.places.Autocomplete(input, {
            types: ["geocode"]
        });
        autocomplete.bindTo('bounds', map);
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(autocomplete, 'place_changed', function (event) {
            infowindow.close();
            var place = autocomplete.getPlace();
            if(place.geometry.viewport){
                map.fitBounds(place.geometry.viewport);
            }else{
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            moveMarker(place.name, place.geometry.location);
            $('.MapLat').val(place.geometry.location.lat());
            $('.MapLon').val(place.geometry.location.lng());
        });
        google.maps.event.addListener(map, 'click', function (event){
            $('.MapLat').val(event.latLng.lat());
            $('.MapLon').val(event.latLng.lng());
            placeMarker(event.latLng);

            infowindow.close();
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                "latLng":event.latLng
            }, function (results, status){
                console.log(results, status);
                if(status == google.maps.GeocoderStatus.OK){
                    console.log(results);
                    document.write(results[0].formatted_address);
                    var lat = results[0].geometry.location.lat(),
                        lng = results[0].geometry.location.lng(),
                        adr = results[0].address_components,
                        placeName = results[0].address_components[0].long_name,
                        latlng = new google.maps.LatLng(lat, lng);
                    moveMarker(placeName, latlng);
                }
            });
        });

        function placeMarker(location){
            marker.setIcon(image);
            marker.setPosition(location);
        }

        function moveMarker(placeName, latlng){
            marker.setIcon(image);
            marker.setPosition(latlng);
            infowindow.setContent(placeName);
            //infowindow.open(map, marker);
        }
    });

    function hanyaAngka(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)){
                return false;
            }
        return true;
    }
    </script>

</html>