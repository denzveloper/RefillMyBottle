<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/img/root.png');?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('assets/img/root.png');?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Place Manager Of <?php echo $this->session->userdata("fnam");?></title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url('assets/css/animate.min.css'); ?>" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="<?php echo base_url('assets/css/paper-dashboard.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url('assets/css/demo.css" rel="stylesheet'); ?>" />

    <!--  Fonts and icons     -->
    <link href='<?php echo base_url('assets/fa/css/all.min.css');?>' rel='stylesheet' type='text/css'>    
    <link href='<?php echo base_url('assets/fonts/muli/font.css');?>' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url('assets/css/themify-icons.css'); ?>" rel="stylesheet">    
	
    <!--  Table     -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/datatables.min.css'); ?>"/>

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="info">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <img class="img-responsive img-circle center-block" src="<?php echo base_url('assets/img/logo.png'); ?>" alt="..."/>
            </div>

            <ul class="nav">
                <li>
                    <a href="<?php echo base_url('index.php/dashboard');?>">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/adpl');?>">
                        <i class="ti-map-alt"></i>
                        <p>Tempat Baru</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/npu');?>">
                        <i class="ti-map"></i>
                        <p>Saran Tempat</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/addusr');?>">
                        <i class="fa fa-users"></i>
                        <p>Tambah User</p>
                    </a>
                </li>
                <li class="active">
                    <a href="<?php echo base_url('index.php/dashboard/mngplc');?>">
                        <i class="fa fa-map"></i>
                        <p>Manajer Tempat</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/mngusr');?>">
                        <i class="fa fa-user-circle"></i>
                        <p>Manajer User</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/profil');?>">
                        <i class="ti-user"></i>
                        <p>Profil</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('index.php/dashboard/passwd');?>">
                        <i class="ti-lock"></i>
                        <p>Sandi</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">Manajer Tempat</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                        <a href="<?php echo base_url('index.php/dashboard/profil');?>" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="ti-user"></i>
								<p><?php echo $this->variabel->nama();?></p>
                                <b class="caret"></b>
                            </a>
                                <ul class="dropdown-menu">
                                <li class="dropdown-header">Pengaturan Akun</li>
                                <li><a href="<?php echo base_url('index.php/dashboard/profil');?>">Pengguna</a></li>
                                <li><a href="<?php echo base_url('index.php/dashboard/passwd');?>">Sandi</a></li>
                                <li role="separator" class="divider"></li>
                                <li class="dropdown-header">Log Keluar</li>
                                <li><a href="<?php echo base_url('index.php/login/logout');?>">Keluar Sesi</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Manager Tempat</h4>
                                <p class="category">Mengatur Tempat yang ada</p>
                            </div>
                            <div class="content">
                                <?php echo var_dump($place); ?>
                                <table id="tableusr" class="table display table-hover">
                                <thead>
                                    <tr class="bg-primary">
                                        <th scope="col">#</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Alamat(Coordinates)</th>
                                        <th scope="col">Foto</th>
                                        <th scope="col">Premium</th>
                                        <th scope="col">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($place)){ $x = 1; foreach($place as $hit){
                                        echo "<tr>";
                                        echo "<th scope='row'>$x</th>";
                                        echo "<td><span title='$hit[nama]'><a href='https://www.google.com/maps/search/$hit[nama] $hit[alamat]' target='_blank'>$hit[name]</span></td>";
                                        echo "<td><span title='$hit[alamat]'>$hit[addr]</span></br><small><a href='https://www.google.com/maps/search/$hit[latitude],$hit[longitude]' target='_blank'><i class='ti-map-alt'></i> $hit[latitude],$hit[longitude]</small></td>";
                                        echo "<td>";
                                        if(!empty($hit['img'])){ foreach($hit['img'] as $img){
                                            echo "<a href=\"$img\" target='_blank'><img width=\"24\" class=\"img-responsive img-rounded center-block\" src=\"$img\"></a>";
                                        }}else echo "*No IMG*";
                                        echo "</td>";
                                        echo "<td><i class='$hit[pre] $hit[prec]'></i> </td>";
                                        echo "<td>
                                                <a href='$hit[link]&todo=delete' onclick=\"return confirm('Yahkin ingin menghapus $hit[nama]?')\"><button class=\"btn btn-sm btn-danger btn-icon\" title=\"Delete Place and User\"><i class=\"fa fa-trash\"></i></button></a>
                                            </td>";
                                        echo "</tr>";
                                        $x++;
                                    }}else{ echo "<tr><td colspan='6'><h4 class='text-center'>EMPTY</h4></td></tr>"; } ?>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by <a href="#">DENZVELOPER DzEN/DzEN</a>
                </div>
            </div>
        </footer>

    </div>
</div>

</body>

    <!--   Core JS Files   -->
    <script src="<?php echo base_url('assets/js/jquery-1.10.2.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="<?php echo base_url('assets/js/paper-dashboard.js'); ?>"></script>
    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url('assets/js/bootstrap-notify.js'); ?>"></script>
    
    <!--  Table Plugin    -->
    <script type="text/javascript" src="<?php echo base_url('assets/datatables/datatables.min.js'); ?>"></script>

    <script>
        $(document).ready(function() {
            $('#tableusr').DataTable({ "aLengthMenu": [[5, 10, 20, 30, -1], [5, 10, 20, 30, "Semua"]],
        "iDisplayLength": 5, "language": {"url": "<?php echo base_url('assets/datatables/Indonesian.json');?>"}
        });
        } );
    </script>
    <?php if($this->session->flashdata('info')){ foreach($this->session->flashdata('info') as $row) {?>
    <script type="text/javascript">
    	$(document).ready(function(){

        	$.notify({
            	icon: '<?php echo $row['ico']; ?>',
            	message: "<?php echo $row['txt']; ?>"

            },{
                type: '<?php echo $row['typ']; ?>',
                timer: 3000
            });

    	});
	</script>
    <?php } } ?>

</html>